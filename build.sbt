name := "AsApp"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaJpa,
  cache,
  "com.google.inject" % "guice" % "3.0"
)     

play.Project.playJavaSettings
