/**
 * 
 * File created at 20 okt. 2013 21:31:44 by Johan Mulder <johan@mulder.net>
 */
package asapp.di;

import com.google.inject.AbstractModule;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class InjectorModule extends AbstractModule
{
	@Override
	public void configure()
	{
		// bind the DAO interfaces.
		bindDaoInterfaces();
		// Bind the zipcode interfaces.
		bindZipcodeInterfaces();
		// Bind mail interfaces.
		bindMailInterfaces();
	}

	/**
	 * Bind all DAO interfaces to concrete implementations.
	 */
	private void bindDaoInterfaces()
	{
		bind(asapp.data.dao.ParticipantDao.class).to(asapp.data.dao.impl.ParticipantDaoImpl.class);
		bind(asapp.data.dao.ParticipantGroupDao.class).to(
				asapp.data.dao.impl.ParticipantGroupDaoImpl.class);
		bind(asapp.data.dao.ParticipantGroupMemberDao.class).to(
				asapp.data.dao.impl.ParticipantGroupMemberDaoImpl.class);
		bind(asapp.data.dao.SupplierDao.class).to(asapp.data.dao.impl.SupplierDaoImpl.class);
		bind(asapp.data.dao.RoleDao.class).to(asapp.data.dao.impl.RoleDaoImpl.class);
		bind(asapp.data.dao.UserDao.class).to(asapp.data.dao.impl.UserDaoImpl.class);
		bind(asapp.data.dao.ActivityDao.class).to(asapp.data.dao.impl.ActivityDaoImpl.class);
		bind(asapp.data.dao.ActivityImageDao.class).to(
				asapp.data.dao.impl.ActivityImageDaoImpl.class);
		bind(asapp.data.dao.ActivityCategoryDao.class).to(
				asapp.data.dao.impl.ActivityCategoryDaoImpl.class);
		bind(asapp.data.dao.ActivityAvailabilityDao.class).to(
				asapp.data.dao.impl.ActivityAvailabilityDaoImpl.class);
		bind(asapp.data.dao.ActivityRatingDao.class).to(
				asapp.data.dao.impl.ActivityRatingDaoImpl.class);
		bind(asapp.data.dao.ActivityBookingDao.class).to(
				asapp.data.dao.impl.ActivityBookingDaoImpl.class);
		bind(asapp.data.dao.FacebookDataDao.class).to(
				asapp.data.dao.impl.FacebookAuthTokenDaoImpl.class);
	}

	/**
	 * Bind the zip code interfaces to concrete implementations.
	 */
	private void bindZipcodeInterfaces()
	{
		bind(asapp.services.zipcode.ZipcodeService.class).to(
				asapp.services.zipcode.ZipcodeServiceImpl.class);
	}

	/**
	 * Bind the mail interfaces to concrete implementations.
	 */
	private void bindMailInterfaces()
	{
		bind(asapp.mail.MailTemplateRenderer.class).to(asapp.mail.SimpleTemplateRenderer.class);
	}
}
