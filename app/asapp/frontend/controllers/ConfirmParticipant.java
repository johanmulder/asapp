package asapp.frontend.controllers;

import java.util.Date;

import javax.inject.Inject;

import org.joda.time.DateTime;

import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.frontend.views.html.participantconfirmed;
import asapp.models.ParticipantGroupMemberModel;
import asapp.models.types.ParticipantGroupMember;

public class ConfirmParticipant extends Controller
{
	@Inject
	private ParticipantGroupMemberModel participantGroupMemberModel;

	/**
	 * Confirm participant.
	 * 
	 * @param confirmhash, ends with: 
	 * @param 0 = won't participate, 
	 * @param 1 = will  participate
	 */
	@Transactional
	public Result doConfirmed(String confirmHash)
	{
		// Split confirm hash into hash and confirm value
		String[] hashParts = confirmHash.split("=");
		if (hashParts.length > 1)
		{
			if (hashParts[1].equals("0") || hashParts[1].equals("1"))
			{
				ParticipantGroupMember participantGroupMember = participantGroupMemberModel
						.getByHash(hashParts[0]);
				if (participantGroupMember != null)
				{
					// Check if the activity timeout ends after today
					int timeout = participantGroupMember.getParticipantGroup()
							.getNotificationTimeout();
					Date activityDate = participantGroupMember.getParticipantGroup()
							.getPreferredTime();
					Date outerConfirmDate = new DateTime(activityDate).minusDays(timeout).toDate();
					if (outerConfirmDate.after(new Date()))
					{
						participantGroupMember.setParticipantConfirmed(Integer
								.valueOf(hashParts[1]));
						participantGroupMemberModel.update(participantGroupMember);
						return ok(participantconfirmed.render(participantGroupMember));
					}
				}
			}
		}
		return redirect(routes.Main.index());
	}
}