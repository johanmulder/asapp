package asapp.frontend.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.mvc.Controller;
import play.mvc.Result;
import asapp.frontend.views.html.index;

/**
 * Controller for the main page.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class Main extends Controller
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(Main.class);

	public Main()
	{
		super();
		logger.debug("Main controller initialized");
	}

	/**
	 * The main entrypoint.
	 * 
	 * @return
	 */
	public Result index()
	{
		logger.debug("Index from " + request().remoteAddress());
		return ok(index.render());
	}
}
