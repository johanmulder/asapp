package asapp.frontend.controllers;

import java.util.ArrayList;
import java.util.List;
import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.mail.MessagingException;

import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.views.html.editbooking;
import asapp.services.ParticipantMailer;
import asapp.services.SupplierMailer;

import asapp.models.ParticipantModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.ParticipantGroupMemberModel;
import asapp.models.BookingModel;

import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;
import asapp.models.types.Supplier;
import asapp.models.types.ActivityBooking;

/**
 * Controller handling the viewing & editing of booked activities.
 * 
 * @author Vincent Vogelesang
 */
@Security.Authenticated(SecuredUser.class)
public class EditBooking extends Controller
{
	// Form handling the activity input.
	private Form<ParticipantGroupMember> participantGroupMemberForm = Form
			.form(ParticipantGroupMember.class);
	@Inject
	private ParticipantModel participantModel;
	@Inject
	private ParticipantGroupModel participantGroupModel;
	@Inject
	private ParticipantGroupMemberModel participantGroupMemberModel;
	@Inject
	private BookingModel bookingModel;
	@Inject
	private ParticipantMailer participantMailer;
	@Inject
	private SupplierMailer supplierMailer;

	/**
	 * Method handling the main activity booking page.
	 * 
	 * @return
	 */
	@Transactional
	public Result editBooking()
	{
		String email = session().get("email");
		Participant participant = getParticipant(email);
		List<ParticipantGroupMember> bookingList = participantGroupMemberModel
				.getBookedListByParticipant(participant);

		return ok(editbooking
				.render(participantGroupMemberForm, bookingList, getParticipant(email)));
	}

	/**
	 * Method handling the cancelling of an activity.
	 * 
	 * @param the participant group id
	 * @return
	 */
	@Transactional
	public Result doEditBooking(int participantGroupId)
	{
		String email = session().get("email");
		// Get the participantgroup from the given id
		ParticipantGroup participantGroup = participantGroupModel.getById(participantGroupId);

		// Check if current participant is initiator
		Participant participant = participantGroupMemberModel
				.getInitiatorByParticipantGroup(participantGroup);

		if (participant.getEmail().equals(email))
		{
			// Send cancel mail to all participants & supplier
			try
			{
				// Mail participants
				participantMailer.mailBookingCancellation(participantGroup);

				// Mail supplier if already booked
				if (participantGroup.getActivityBooking() != null)
				{
					Supplier supplier = participantGroup.getActivityBooking()
							.getActivityAvailability().getActivity().getSupplier();
					ActivityBooking booking = participantGroup.getActivityBooking();
					supplierMailer.mailBookingCancellation(supplier, booking);
					// Delete booking for given participantGroup
					bookingModel.deleteBooking(participantGroup);
				}
			}
			catch (UnsupportedEncodingException | MessagingException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Delete all ParticipantGroupMembers for the given participantGroup
			participantGroupMemberModel.deleteByParticipantGroup(participantGroup);
			// Delete the particpantGroup
			participantGroupModel.delete(participantGroup);
		}
		return redirect(routes.EditBooking.editBooking());
	}

	/**
	 * When a new booking has been created by doBookActivity(), a redirect will
	 * be sent to this method.
	 * 
	 * @return
	 */
	public Result bookingEdited()
	{
		String email = session().get("email");
		List<ParticipantGroupMember> bookingList = new ArrayList<>();
		return ok(editbooking
				.render(participantGroupMemberForm, bookingList, getParticipant(email)));
	}

	/**
	 * Get the current participant.
	 * 
	 * @param email adress
	 * @return Participant object
	 */
	private Participant getParticipant(String email)
	{
		return participantModel.getByEmail(email);
	}
}
