package asapp.frontend.controllers;

import static play.data.Form.form;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.data.dao.UserDao;
import asapp.frontend.views.html.login;
import asapp.models.types.User;

/**
 * Controller handling the login.
 * 
 * @author JH
 */
public class Login extends Controller
{
	// Logging object.
	private static Logger logger = LoggerFactory.getLogger(Login.class);
	@Inject
	private UserDao userDao;

	/**
	 * Method handling the login page for unauthenticated users.
	 * 
	 * @return
	 */
	public Result login()
	{
		return ok(login.render(form(Logins.class)));
	}

	/**
	 * Class containing the email and password of a user.
	 * 
	 * @author JH
	 */
	public static class Logins
	{
		public String email;
		public String password;

	}

	/**
	 * Authenticate the user. If successful sets session variable and returns to
	 * index.
	 * 
	 * @return
	 */
	@Transactional
	public Result authenticate()
	{
		Form<Logins> loginForm = form(Logins.class).bindFromRequest();
		Logins loginData = loginForm.get();
		User user = userDao.getByUsername(loginData.email);

		// Check if the given email address exists
		if (user == null)
		{
			logger.info("Auth failed: username " + loginData.email + " does not exist");
			return badRequest(login.render(loginForm));
		}
		else
		{
			if (user.getPassword().equals(loginData.password))
			{
				session().clear();
				session("email", loginData.email);
				session("role", user.getRole().getName());
				session("userid", Integer.toString(user.getUserId()));

				// Log it.
				logger.info("Logged in as " + loginData.email + " from "
						+ request().remoteAddress());
				// Redirect location after logged in base on Role
				if (user.getRole().getName().equals("supplier"))
					return redirect(asapp.supplierportal.controllers.routes.Main.index());
				if (user.getRole().getName().equals("admin"))
					return redirect(asapp.adminportal.controllers.routes.Main.index());
				else
					return redirect(routes.Main.index());
			}
		}
		// Return loginform if not authenticated.
		return badRequest(login.render(loginForm));
	}

	/**
	 * Controller function handling the log out.
	 * 
	 * @return
	 */
	public Result logout()
	{
		session().clear();
		flash("success", "You've been logged out");
		return redirect(routes.Main.index());
	}
}
