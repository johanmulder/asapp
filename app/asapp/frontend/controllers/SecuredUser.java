package asapp.frontend.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Security controller handling the user authorization. Annotation can be used
 * to check if there's a user session and role type. Unauthorized users will be
 * redirected
 * 
 * @author JH
 */
public class SecuredUser extends Security.Authenticator
{
	// Logger class.
	private static Logger logger = LoggerFactory.getLogger(SecuredUser.class);

	/**
	 * Get the username of the currently logged in user. If it's not available,
	 * play will assume no user is logged in and onUnauthorized will be called.
	 * It also checks if user has a user role.
	 * 
	 * @param Context ctx (Retrieves the username from the HTTP context; the
	 *            default is to read from the session cookie.)
	 * @return String ctx (null if the user is not authenticated.)
	 */
	@Override
	public String getUsername(Context ctx)
	{
		try
		{
			if (ctx.session().get("role").toString().equals("user"))
			{
				return ctx.session().get("email");
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			return null;
		}
	}

	/**
	 * Action handler which will be executed when a controller or controller
	 * method has been annotated with Security.Authenticated, but if no user has
	 * been logged in or wrong user role.
	 * 
	 * @param Context ctx
	 * @return Result
	 */
	@Override
	public Result onUnauthorized(Context ctx)
	{
		logger.debug("Not logged in. Redirecting to login");
		return redirect(routes.Login.login());
	}

}