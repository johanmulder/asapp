package asapp.frontend.controllers;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.data.validation.ValidationError;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import asapp.frontend.views.html.activitybooked;
import asapp.frontend.views.html.booking;
import asapp.models.ActivityModel;
import asapp.models.ParticipantGroupMemberModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.ParticipantModel;
import asapp.models.exceptions.ParticipantAlreadyExistsException;
import asapp.models.types.ActivityCategory;
import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;
import asapp.services.ParticipantMailer;
import asapp.util.CryptUtils;
import asapp.util.DateFormatter;

/**
 * Controller handling the registration of activities.
 * 
 * @author Vincent Vogelesang
 */
@Security.Authenticated(SecuredUser.class)
public class BookActivity extends Controller
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(BookActivity.class);
	// Form handling the activity input.
	private Form<ParticipantGroup> bookActivityForm = Form.form(ParticipantGroup.class);
	@Inject
	private ActivityModel activityModel;
	@Inject
	private ParticipantModel participantModel;
	@Inject
	private ParticipantGroupModel participantGroupModel;
	@Inject
	private ParticipantGroupMemberModel participantGroupMemberModel;
	@Inject
	private ParticipantMailer participantMailer;

	/**
	 * Method handling the main activity booking page.
	 * 
	 * @return
	 */
	@Transactional
	public Result bookActivity()
	{
		String email = session().get("email");
		return ok(booking.render(bookActivityForm, getCategories(), getParticipant(email)));
	}

	/**
	 * Method handling the booking of an activity.
	 * 
	 * @return
	 */
	@Transactional
	public Result doBookActivity()
	{
		String email = session().get("email");

		// Get form data and parse into participant list
		Form<ParticipantGroup> form = bookActivityForm.bindFromRequest();
		
		// Validate the input
		Form<ParticipantGroup> invalidForm = validateInput(form);		
		if (invalidForm != null) 
		{
			return badRequest(booking.render(invalidForm, getCategories(), getParticipant(email)));
		}		

		Map<String, String> formMap = form.data();
		List<Participant> participants = getParticipantsFromMap(formMap);

		// Create participant group
		ParticipantGroup participantGroup = getGroupFromMap(formMap);
		participantGroupModel.create(participantGroup);

		// Create participants in db & add to group
		for (Participant participant : participants)
		{
			ParticipantGroupMember participantGroupMember = new ParticipantGroupMember();
			try
			{
				participantModel.create(participant);
			}
			catch (ParticipantAlreadyExistsException ex)
			{
				participant = ex.getParticipant();
				logger.warn("Participant with email address " + participant.getEmail()
						+ " already exists. Using entry " + participant.getParticipantId());
			}
			
			Participant dbParticipant = participantModel.getByEmail(participant.getEmail());
			
			participantGroupMember.setParticipant(dbParticipant);
			participantGroupMember.setParticipantGroup(participantGroup);

			participantGroupMember.setParticipantConfirmed(0);
			participantGroupMember.setInitiator(participant.getEmail().equals(email) ? 1 : 0);

			participantGroupMember.setHash(getHash(dbParticipant.getEmail()
					+ participantGroup.getParticipantGroupId()));

			participantGroupMemberModel.create(participantGroupMember);
			participantGroup.getGroupMembers().add(participantGroupMember);
		}
		
		// Send mail with activity to all participants
		try
		{
			participantMailer.mailBookingInvitation(participantGroup);
		}
		catch (UnsupportedEncodingException | MessagingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return redirect(routes.BookActivity.activityBooked());
	}

	/**
	 * When a new booking has been created by doBookActivity(), a redirect will
	 * be sent to this method.
	 * 
	 * @return
	 */
	public Result activityBooked()
	{
		return ok(activitybooked.render());
	}

	/**
	 * Get all possible categories.
	 * 
	 * @return A list of categories.
	 */
	private List<ActivityCategory> getCategories()
	{
		List<ActivityCategory> categories = activityModel.getActivityCategories();
		return categories;
	}

	/**
	 * Get the current participant.
	 * 
	 * @param email adress
	 * @return Participant object
	 */
	private Participant getParticipant(String email)
	{
		return participantModel.getByEmail(email);
	}

	/**
	 * Create a list of participants from the form data
	 * 
	 * @param form map
	 * @return List with participant objects
	 */
	private List<Participant> getParticipantsFromMap(Map<String, String> formMap)
	{
		// Get all map entries starting with emailx and namex
		List<String> emailList = new ArrayList<>();
		List<String> firstNameList = new ArrayList<>();
		List<String> lastNameList = new ArrayList<>();
		for (String key : formMap.keySet())
		{
			// Check for a (somewhat) valid email address
			if (key.contains("email") && formMap.get(key).length() > 4
					&& formMap.get(key).contains("@"))
			{
				emailList.add(formMap.get(key));
				firstNameList.add(formMap.get(key.replace("email", "firstname")));
				lastNameList.add(formMap.get(key.replace("email", "lastname")));
			}
		}

		// Create participants from both lists
		List<Participant> participants = new ArrayList<>();
		for (int i = 0; i < emailList.size(); i++)
		{
			Participant participant = new Participant();
			participant.setEmail(emailList.get(i));
			logger.debug("Email:" + emailList.get(i));
			participant.setFirstName(firstNameList.get(i));
			participant.setLastName(lastNameList.get(i));
			participants.add(participant);
		}
		return participants;
	}

	/**
	 * Gets the participant group and info from formMap
	 * 
	 * @param Map with form data
	 * @return ParticipantGroup from data
	 */
	private ParticipantGroup getGroupFromMap(Map<String, String> formMap)
	{
		ParticipantGroup participantGroup = new ParticipantGroup();
		
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			Date date = formatter.parse(formMap.get("date"));
			participantGroup.setPreferredTime(date);
		}
		catch (ParseException e)
		{
			logger.debug("Error1:" + e);
		}		
		
		ActivityCategory activityCategory = new ActivityCategory();
		activityCategory.setCategory(formMap.get("category"));
		participantGroup.setActivityCategory(activityCategory);

		participantGroup.setPostcodeArea(Integer.parseInt(formMap.get("zipcode").replaceAll(
				"[^0-9]", "")));
		participantGroup.setMaxDistance(Integer.parseInt(formMap.get("distance")));
		participantGroup.setTariffParticipant(Integer.parseInt(formMap.get("price")));
		participantGroup.setNotificationTimeout(Integer.parseInt(formMap.get("timeout")));

		return participantGroup;
	}

	/**
	 * Get the hash from a string
	 * 
	 * @param String to hash
	 * @return Hash string result
	 */
	public String getHash(String hash)
	{
		try
		{
			return CryptUtils.getSha1Hash(hash);
		}
		catch (NoSuchAlgorithmException e)
		{
			return null;
		}
	}
	
	/**
	 * Validate non participant input fields
	 * 
	 * @param Form 
	 * @return form with validation error messages
	 */
	private Form<ParticipantGroup> validateInput(Form<ParticipantGroup> form)
	{
		// Validate the postcode
		String postcode = form.data().get("zipcode");
		if (postcode == null || !postcode.matches("[0-9]{4}[A-Za-z]{2}")) 
		{
			form.reject(new ValidationError("zipcode",
					"error.zipcode.mismatch"));
			return form;
		}
		
		// Validate the date
		try
		{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			Date date = formatter.parse(form.data().get("date"));
			int timeout = Integer.parseInt(form.data().get("timeout"));
			DateTime dateTimeOut = new DateTime(date).minusDays(timeout);
			if (date.before(new Date()) || dateTimeOut.toDate().before(new Date()))
			{
				form.reject(new ValidationError("date",
						"error.date.mismatch"));
				return form;
			}		
		}
		catch (ParseException e)
		{
			logger.debug("Error2:" + e);
			form.reject(new ValidationError("date",
					"error.date.mismatch"));
			return form;
		}
				
		// Validate the distance
		String distance = form.data().get("distance");
		if (!distance.matches("^[0-9]+$")) 
		{
			form.reject(new ValidationError("distance",
					"error.distance.mismatch"));
			return form;
		}	
		return null;			
	}
}
