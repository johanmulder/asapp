package asapp.frontend.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.frontend.views.html.categories;
import asapp.models.ActivityImageModel;
import asapp.models.ActivityModel;
import asapp.models.types.Activity;
import asapp.models.types.ActivityImage;
import asapp.util.AppConfig;

public class Activities extends Controller
{
	// Logger object.
	private static final Logger logger = LoggerFactory.getLogger(Activities.class);
	@Inject
	private ActivityModel activityModel;
	@Inject
	private ActivityImageModel activityImageModel;

	/**
	 * Display all activity categories
	 * 
	 * @return
	 */
	@Transactional
	public Result allCategories()
	{
		return getCategories("%");
	}

	/**
	 * Display a specific activity category.
	 * 
	 * @param category
	 * @return
	 */
	@Transactional
	public Result category(String category)
	{
		return getCategories(category);
	}

	/**
	 * Get a specific activity category.
	 * 
	 * @param category
	 * @return
	 */
	private Result getCategories(String category)
	{
		// Get list of activities
		List<Activity> activityList = activityModel.getActivityByCategory(category);

		// Get list of corresponding pictures
		List<ActivityImage> imageList = new ArrayList<>();
		for (Activity activity : activityList)
		{
			List<ActivityImage> activityImageList = activityImageModel
					.getActivityImageByActivity(activity);
			if (activityImageList.size() > 0)
			{
				imageList.add(activityImageList.get(0));
			}
			else
				imageList.add(new ActivityImage());
		}
		return ok(categories.render(category, activityList, imageList));
	}

	/**
	 * Get an image.
	 * 
	 * @param activityImageId
	 * @return
	 */
	@Transactional
	public Result image(int activityImageId)
	{
		ActivityImage image = activityImageModel.getById(activityImageId);
		if (image == null)
			return notFound();

		String separator = System.getProperty("file.separator");
		String imagePath = AppConfig.getActivityImagePath() + separator
				+ image.getActivity().getActivityId() + separator + image.getHash();
		File imageFile = new File(imagePath);
		if (!imageFile.exists())
		{
			logger.debug("Image path " + imagePath + " does not exist");
			return notFound();
		}

		return ok(imageFile, "").as(image.getContentType());
	}
}
