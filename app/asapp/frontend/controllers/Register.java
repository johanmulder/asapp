package asapp.frontend.controllers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.data.validation.ValidationError;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.data.dao.FacebookDataDao;
import asapp.frontend.views.html.register;
import asapp.frontend.views.html.registered;
import asapp.models.ParticipantModel;
import asapp.models.exceptions.ParticipantAlreadyExistsException;
import asapp.models.exceptions.UserAlreadyExistsException;
import asapp.models.types.FacebookData;
import asapp.models.types.Participant;
import asapp.services.ParticipantMailer;
import asapp.util.Password;

/**
 * Controller handling the registration of regular participants.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class Register extends Controller
{
	// Logging object.
	private static Logger logger = LoggerFactory.getLogger(Register.class);
	// The form handling the registration.
	private static Form<Participant> registerForm = Form.form(Participant.class);
	// Automatically injected participantDao.
	@Inject
	private ParticipantModel participantModel;
	@Inject
	private ParticipantMailer participantMailer;
	@Inject
	private FacebookDataDao facebookDataDao;

	/**
	 * Method handling the main registration page for regular users.
	 * 
	 * @return
	 */
	@Transactional
	public Result register()
	{
		// Check if we've got an oauth provider
		String oauthProvider = session("oauthProvider");
		if (oauthProvider != null)
			// Switch, because multiple oauth providers will be added.
			switch (oauthProvider)
			{
				case "facebook":
					return getFormWithFacebookData(new BigDecimal(session("facebookUid")));
			}

		return ok(register.render(registerForm));
	}

	/**
	 * Get the registration page filled with data from facebook.
	 * 
	 * @param fbUid
	 * @return
	 */
	private Result getFormWithFacebookData(BigDecimal fbUid)
	{
		// Retrieve the facebook data.
		FacebookData data = facebookDataDao.getByUserId(fbUid);
		if (data == null)
		{
			String logmsg = "Facebook user entry with uid " + fbUid.toPlainString() + " not found";
			logger.error(logmsg);
			return internalServerError(logmsg);
		}

		// Fill in the form details.
		Participant participant = new Participant();
		participant.setEmail(data.getEmail());
		participant.setFirstName(data.getFirstName());
		participant.setMiddleName(data.getMiddleName());
		participant.setLastName(data.getLastName());

		return ok(register.render(registerForm.fill(participant)));
	}

	/**
	 * Method handling the posted data for regular users. This method validates
	 * the form and will create the newly created user. When created, a
	 * confirmation link will be sent to the user.
	 * 
	 * @return
	 * @throws MessagingException
	 * @throws AddressException
	 */
	@Transactional
	public Result doRegister()
	{
		// Bind the form to the http request.
		Form<Participant> participantForm = registerForm.bindFromRequest();

		// Check if the given passwords match, only if there's no oauth provider
		// set.
		String oauthProvider = session("oauthProvider");
		String password = null;
		if (oauthProvider == null)
		{
			password = participantForm.data().get("password");
			String passwordValidate = participantForm.data().get("passwordValidate");
			if (Password.isPasswordMismatch(passwordValidate, password))
				participantForm.reject(new ValidationError("passwordValidate",
						"error.password.mismatch"));
		}

		if (participantForm.hasErrors())
		{
			logger.debug("Form has errors: " + participantForm.errorsAsJson());
			return badRequest(register.render(participantForm));
		}
		else
		{
			try
			{
				// Get the participant object from the posted form.
				Participant newParticipant = participantForm.get();
				// Create the new participant.
				participantModel.create(newParticipant, password);
				// Check if an oauth provider needs to be linked.
				if (oauthProvider != null)
				{
					switch (oauthProvider)
					{
						case "facebook":
							// Get the user entry.
							FacebookData data = facebookDataDao.getByUserId(new BigDecimal(
									session("facebookUid")));
							data.setParticipant(newParticipant);
							data.setEmail(newParticipant.getEmail());
							facebookDataDao.update(data);
							break;
					}
				}
				// Mailing of confirmation.
				participantMailer.mailRegistrationConfirmation(newParticipant);
				return redirect(routes.Register.registered());
			}
			catch (UserAlreadyExistsException | ParticipantAlreadyExistsException
					| UnsupportedEncodingException | MessagingException e)
			{
				logger.error("Exception caught", e);
				participantForm.reject("Exception caught: " + e.getMessage());
				return badRequest(register.render(participantForm));
			}
		}
	}

	/**
	 * When a new user has been created by doRegister(), a redirect will be sent
	 * to this method.
	 * 
	 * @return
	 */
	public Result registered()
	{
		return ok(registered.render());
	}
}