/**
 * 
 * File created at 6 nov. 2013 20:39:52 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.net.URL;

/**
 * Utility methods for the service clasess. This shouldn't actually be a public
 * class, but due to a bug in mockito, it is impossible to inject a package
 * private object into an object under test.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ServiceUtils
{
	/**
	 * Generate a bounce address with the given prefix.
	 * 
	 * @param prefix
	 * @param identifier
	 * @return
	 */
	public String getBounceAddress(String prefix, int identifier)
	{
		return prefix + "-bounce-" + identifier + "@asapp.localhost.nl";
	}

	/**
	 * Retrieve the mail template url for the given template name.
	 * 
	 * @param templateName The name of the html template file.
	 * @return
	 */
	public URL getMailTemplateUrl(String templateName)
	{
		String resourceName = "mailtemplates/" + templateName;
		URL url = play.Play.application().classloader().getResource(resourceName);
		if (url == null)
			throw new RuntimeException("Template " + resourceName + " not found");
		return url;
	}

}
