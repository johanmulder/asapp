/**
 * 
 * File created at 6 nov. 2013 20:35:13 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.*;

import asapp.mail.MailTemplateRenderer;
import asapp.models.types.Activity;
import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroupMember;
import asapp.models.types.Supplier;
import asapp.util.Mailer;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class SupplierMailer
{
	// Logger object.
	// private static Logger logger =
	// LoggerFactory.getLogger(ParticipantMailer.class);
	// Mail template renderer.
	private MailTemplateRenderer mailTemplateRenderer;
	// Mailer for sending the actual mail to the participant.
	private Mailer mailer;
	@Inject
	private ServiceUtils serviceUtils;

	/**
	 * Class constructor.
	 * 
	 * @param mailer
	 * @param mailTemplateRenderer
	 */
	@Inject
	public SupplierMailer(Mailer mailer, MailTemplateRenderer mailTemplateRenderer)
	{
		this.mailTemplateRenderer = mailTemplateRenderer;
		this.mailer = mailer;
	}

	/**
	 * Mail the supplier with the information.
	 * 
	 * @param supplier
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void mailSupplierRegistrationConfirmation(Supplier supplier) throws MessagingException,
			UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(supplier.getEmail()));
		message.setSubject("Bevestiging aanbieder-registratie bij AsApp");
		message.setFrom(new InternetAddress("asappnl@gmail.com", "AsApp Registratie"));
		message.setSender(new InternetAddress(getBounceAddressSupplier(supplier)));
		message.setContent(getConfirmationContent(supplier));

		// Send the mail.
		mailer.sendMail(message);
	}

	public void mailBookingInformation(Supplier supplier, asapp.models.types.ActivityBooking booking)
			throws AddressException, MessagingException, UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(supplier.getEmail()));
		message.setSubject("Boeking via AsApp");
		message.setFrom(new InternetAddress("asapp@asapp.localhost.nl", "AsApp Boekingen"));
		message.setSender(new InternetAddress(getBounceAddressSupplier(supplier)));
		message.setContent(getBookingInformationContent(supplier, booking, "supplier_activity_booked.html"));

		// Send the mail.
		mailer.sendMail(message);

	}
	
	public void mailBookingCancellation(Supplier supplier, asapp.models.types.ActivityBooking booking)
			throws AddressException, MessagingException, UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(supplier.getEmail()));
		message.setSubject("Annulering van boeking via AsApp");
		message.setFrom(new InternetAddress("asapp@asapp.localhost.nl", "AsApp Boekingen"));
		message.setSender(new InternetAddress(getBounceAddressSupplier(supplier)));
		message.setContent(getBookingInformationContent(supplier, booking, "supplier_activity_cancelled.html"));

		// Send the mail.
		mailer.sendMail(message);

	}


	/**
	 * Generate the bounce address for the given supplier.
	 * 
	 * @param supplier
	 * @return
	 */
	private String getBounceAddressSupplier(Supplier supplier)
	{
		return serviceUtils.getBounceAddress("supplier", supplier.getSupplierId());
	}

	/**
	 * Get the mail content for a booking.
	 * 
	 * @param participant
	 * @param booking
	 * @return
	 * @throws MessagingException
	 */
	private MimeMultipart getConfirmationContent(Supplier supplier) throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getSupplierHtmlContent(supplier), "text/html");
		content.addBodyPart(html);

		return content;
	}

	/**
	 * Get the html content for supplier confirmation.
	 * 
	 * @param supplier
	 * @return
	 */
	private String getSupplierHtmlContent(Supplier supplier)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Participant values.
		templateValues.put("name", supplier.getName());

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl("supplier_confirmation.html"), templateValues);
	}

	private MimeMultipart getBookingInformationContent(Supplier supplier,
			asapp.models.types.ActivityBooking booking, String templateName) throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getBookingHtmlContent(supplier, booking, templateName), "text/html");
		content.addBodyPart(html);

		return content;
	}

	private String getBookingHtmlContent(Supplier supplier,
			asapp.models.types.ActivityBooking booking, String templateName)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Supplier values.
		templateValues.put("name", supplier.getName());
		// Activity values.
		Activity activity = booking.getActivityAvailability().getActivity();
		// TODO: Make this a localized date.
		templateValues.put("activity_time",
				DateFormat.getDateInstance().format(booking.getDatetime()));
		templateValues.put("activity_name", activity.getName());
		// Get all participants.
		StringBuilder sb = new StringBuilder();
		for (ParticipantGroupMember groupMember : booking.getParticipantGroup().getGroupMembers())
		{
			// Only mention confirmed participants.
			if (groupMember.getParticipantConfirmed() != 1)
				continue;
			Participant participant = groupMember.getParticipant();
			// XXX: This is bad. No formatting should be done here, let alone
			// marking the initiator.
			sb.append(participant.getFirstName() + " "
					+ (participant.getMiddleName() == null ? "" : participant.getMiddleName())
					+ " " + participant.getLastName());
			if (groupMember.getInitiator() == 1)
				sb.append("<a href=\"" + participant.getEmail() + "\">e-mail</a> (initiator)");
			sb.append("<br>");
		}
		templateValues.put("members", sb.toString());

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl(templateName), templateValues);
	}
}
