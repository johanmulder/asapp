/**
 * 
 * File created at 2 nov. 2013 22:50:05 by Johan Mulder <johan@mulder.net>
 */
package asapp.services.zipcode;

/**
 * Service class for zipcode actions.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ZipcodeServiceImpl implements ZipcodeService
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.services.zipcode.ZipcodeInterface#calculateDistance(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	public double calculateDistance(String zipcode1, String zipcode2)
	{
		// TODO: This should be implemented in an actual real life situation.
		// For now, just leave it 0.
		return 0.0;
	}
}
