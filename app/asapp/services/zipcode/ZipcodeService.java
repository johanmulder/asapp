/**
 * 
 * File created at 2 nov. 2013 23:14:48 by Johan Mulder <johan@mulder.net>
 */
package asapp.services.zipcode;

/**
 * Zipcode service interface.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface ZipcodeService
{

	/**
	 * Calculate the distance between 2 zipcodes.
	 * 
	 * @param zipcode1
	 * @param zipcode2
	 * @return The distance in kilometers.
	 */
	public double calculateDistance(String zipcode1, String zipcode2);

}