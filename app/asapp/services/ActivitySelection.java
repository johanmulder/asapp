/**
 * 
 * File created at 28 okt. 2013 21:36:01 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asapp.data.dao.ActivityAvailabilityDao;
import asapp.models.BookingModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.*;
import asapp.services.zipcode.ZipcodeService;

/**
 * Service class handling the automatic booking of unbooked selections.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ActivitySelection
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ActivitySelection.class);
	// Required models.
	private ParticipantGroupModel participantGroupModel;
	private ActivityAvailabilityDao activityAvailabilityDao;
	private ZipcodeService zipcodeService;
	// The maximum number of choice to make for a participant group.
	private static final int MAX_CHOICES = 3;

	/**
	 * Class constructor.
	 * 
	 * @param bookingModel
	 */
	@Inject
	public ActivitySelection(BookingModel bookingModel,
			ParticipantGroupModel participantGroupModel,
			ActivityAvailabilityDao activityAvailabilityDao, ZipcodeService zipcodeService)
	{
		this.participantGroupModel = participantGroupModel;
		this.activityAvailabilityDao = activityAvailabilityDao;
		this.zipcodeService = zipcodeService;
	}

	/**
	 * Select activities for all groups in status PENDING_SELECTION.
	 */
	public void selectActivitiesForPendingGroups()
	{
		// 1. Select all groups with status PENDING_SELECTION
		// 2. Number of entries to add = 3 - number of entries attached
		// 3. for i = 1; i <= number of entries to add; i++
		// 4. Select an available activity with criteria:
		// - same category as the preferred category
		// - in the surroundings of the selected area
		// - available around the preferred time
		// - activities with at least and at most the amount of participants in
		// the group
		// - activities costing no more than the preferred amount of the group
		// 5. Add the activity to the participant group
		// 6. Change the status to PENDING_BOOKING
		// 7. done.

		List<ParticipantGroup> pending = participantGroupModel.getGroupsPendingSelection();
		if (pending == null || pending.size() == 0)
		{
			logger.info("No participant groups currently need selection");
			return;
		}

		for (ParticipantGroup group : pending)
		{
			Set<ActivityAvailability> currentSelections = group.getActivityAvailabilities();
			// Get feasible activities for this group.
			List<ActivityAvailability> feasible = getFeasibleActivitiesForGroup(group);
			int maxSelect = MAX_CHOICES - currentSelections.size();
			for (int i = 0; i < maxSelect; i++)
			{
				int numFeasible = feasible.size();
				if (numFeasible == 0)
					break;

				// If we've got feasible entries, pick a random one.
				// XXX: This could use some more intelligence.
				// e.g. selection based on rating or least selected.
				int pick = new Random().nextInt(numFeasible);
				ActivityAvailability newActivity = feasible.get(pick);
				feasible.remove(pick);

				// Log it.
				logger.info("Picked activity availability "
						+ newActivity.getActivityAvailabilityId() + " for group "
						+ group.getParticipantGroupId());
				// Add it to the list in the group pojo.
				group.addActivityAvailability(newActivity);
			}

			// Check if the group has sufficient choice entries set now.
			if (group.getActivityAvailabilities().size() >= 1)
			{
				// The new status is "pending booking". The group will now wait
				// until the actual booking will be made. Only set this status
				// when all selections have been made.
				logger.debug("Found sufficient choices for participant group "
						+ group.getParticipantGroupId());
				group.setStatus(ParticipantGroupStatus.PENDING_BOOKING);
			}
			else
				logger.debug("Not enough choices found for participant group "
						+ group.getParticipantGroupId() + ", found "
						+ group.getActivityAvailabilities().size() + "entries");

			// Update the group entry. This will also create the entries in the
			// link table.
			participantGroupModel.update(group);
		}
	}

	/**
	 * Select feasible activities for a participant group.
	 * 
	 * @param group
	 * @return A list if feasible activities (may be an empty list).
	 */
	private List<ActivityAvailability> getFeasibleActivitiesForGroup(ParticipantGroup group)
	{
		// Get all available activities.
		List<ActivityAvailability> available = getAvailableActivitiesByCategory(group
				.getActivityCategory());
		if (available == null)
		{
			logger.warn("No available activities for category " + group.getActivityCategory());
			return null;
		}

		List<ActivityAvailability> feasible = new ArrayList<>();
		for (ActivityAvailability availableEntry : available)
		{
			// When the selection criteria match, add it to the list of feasible
			// entries.
			if (selectionCriteriaMatch(availableEntry, group))
				feasible.add(availableEntry);
		}

		return feasible;
	}

	/**
	 * Check if all selection criteria match for the given ActivityAvailability.
	 * 
	 * @param availableEntry
	 * @param group
	 * @return
	 */
	private boolean selectionCriteriaMatch(ActivityAvailability availableEntry,
			ParticipantGroup group)
	{
		Activity activity = availableEntry.getActivity();
		// Check if the activity is near the group.
		if (!isNear(activity, group))
			return false;
		// Check if the activity is on the preferred time.
		if (!isOnTime(availableEntry, group))
			return false;
		// Check if there are sufficient participants for the activity.
		if (!hasSufficientParticipants(availableEntry, group))
			return false;
		// Check if the tariff matches the group preference.
		if (!tariffMatches(availableEntry, group))
			return false;
		// Everything matches, so return true.
		return true;
	}

	/**
	 * Check if the tariff of the activity or ActivityAvailability matches the
	 * preferred tariff of the group.
	 * 
	 * @param availableEntry
	 * @param group
	 * @return
	 */
	private boolean tariffMatches(ActivityAvailability availableEntry, ParticipantGroup group)
	{
		BigDecimal tariff = availableEntry.getTariffParticipant();
		if (tariff == null)
			tariff = availableEntry.getActivity().getBaseTariffParticipant();
		if (tariff == null)
		{
			logger.error("No tariff attached to ActivityAvailability entry "
					+ availableEntry.getActivityAvailabilityId() + " and Activity "
					+ availableEntry.getActivity());
			return false;
		}
		BigDecimal preferredTariff = new BigDecimal(group.getTariffParticipant());
		// Return true if the preferred tariff is less than or equal to the
		// preferred tariff.
		return preferredTariff.compareTo(tariff) >= 0;
	}

	/**
	 * Determine if a group has sufficient participants
	 * 
	 * @param activity
	 * @param group
	 * @return
	 */
	private boolean hasSufficientParticipants(ActivityAvailability activity, ParticipantGroup group)
	{
		int numGroupMembers = group.getParticipants().size();
		return numGroupMembers >= activity.getMinParticipants()
				&& numGroupMembers <= activity.getMaxParticipants();
	}

	/**
	 * Check if the chosen activity matches the preferred time of the
	 * participant group.
	 * 
	 * @param activity
	 * @param group
	 * @return
	 */
	private boolean isOnTime(ActivityAvailability activity, ParticipantGroup group)
	{
		// When the difference between the preferred time and the time of the
		// activity is less than half an hour (aka 1800000 ms), we can assume
		// the activity matches.
		return Math.abs(activity.getDateTime().getTime() - group.getPreferredTime().getTime()) <= 1800000;
	}

	/**
	 * Determine if an activity is near a group.
	 * 
	 * @param activity
	 * @param group
	 * @return
	 */
	private boolean isNear(Activity activity, ParticipantGroup group)
	{
		// When the preferred maximum distance is less than the distance between
		// the zipcode of the activity and the zipcode area of the group, there
		// is a match.
		return zipcodeService.calculateDistance(activity.getZipcode(),
				Integer.toString(group.getPostcodeArea())) <= group.getMaxDistance();
	}

	/**
	 * Get available activities by category.
	 * 
	 * @param category
	 * @return
	 */
	private List<ActivityAvailability> getAvailableActivitiesByCategory(ActivityCategory category)
	{
		List<ActivityAvailability> available = activityAvailabilityDao.getByCategory(category);
		if (available == null || available.size() == 0)
			return null;
		return available;
	}
}
