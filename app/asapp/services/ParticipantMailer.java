/**
 * 
 * File created at 3 nov. 2013 21:05:24 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asapp.mail.MailTemplateRenderer;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.*;
import asapp.util.Mailer;

/**
 * Service class handling the mailing of various types of mails to participants.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ParticipantMailer
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ParticipantMailer.class);
	// Necessary model.
	private ParticipantGroupModel participantGroupModel;
	// Mail template renderer.
	private MailTemplateRenderer mailTemplateRenderer;
	// Mailer for sending the actual mail to the participant.
	private Mailer mailer;
	// Service utility methods.
	private ServiceUtils serviceUtils;

	/**
	 * Class constructor.
	 * 
	 * @param participantGroupModel
	 * @param mailer
	 * @param mailTemplateRenderer
	 * @param serviceUtils
	 */
	@Inject
	public ParticipantMailer(ParticipantGroupModel participantGroupModel, Mailer mailer,
			MailTemplateRenderer mailTemplateRenderer, ServiceUtils serviceUtils)
	{
		this.participantGroupModel = participantGroupModel;
		this.mailTemplateRenderer = mailTemplateRenderer;
		this.mailer = mailer;
		this.serviceUtils = serviceUtils;
	}

	/**
	 * Mail all participants in groups for which a booking has been made.
	 */
	public void mailBookedParticipants()
	{
		List<ParticipantGroup> groups = participantGroupModel.getGroupsPendingMailing();
		if (groups == null || groups.size() == 0)
		{
			logger.info("No participant groups currently need to be informed");
			return;
		}

		// Walk through the list of participant groups.
		for (ParticipantGroup group : groups)
		{
			try
			{
				// Mail the participant group.
				mailParticipantGroup(group);
				group.setStatus(ParticipantGroupStatus.PARTICIPANTS_INFORMED);
			}
			catch (Exception e)
			{
				logger.error(
						"An error occured while mailing participant group "
								+ group.getParticipantGroupId() + ": " + e.getMessage(), e);
				group.setStatus(ParticipantGroupStatus.INVALID);
			}

			// Update the group.
			participantGroupModel.update(group);
		}
	}

	/**
	 * Mail the booking invitation to all members in a participant group.
	 * 
	 * @param group
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	public void mailBookingInvitation(ParticipantGroup group) throws UnsupportedEncodingException,
			MessagingException
	{
		for (ParticipantGroupMember groupMember : group.getGroupMembers())
			mailBookingInvitation(groupMember);
	}
	
	/**
	 * Mail the booking cancellation to all members in a participant group.
	 * 
	 * @param group
	 * @throws UnsupportedEncodingException
	 * @throws MessagingException
	 */
	public void mailBookingCancellation(ParticipantGroup group) throws UnsupportedEncodingException,
			MessagingException
	{
		for (ParticipantGroupMember groupMember : group.getGroupMembers())
			mailBookingCancellation(groupMember);
	}

	/**
	 * Mail confirmed members of a participant group.
	 * 
	 * @param participantGroup
	 * @throws Exception
	 */
	private void mailParticipantGroup(ParticipantGroup participantGroup) throws Exception
	{
		for (ParticipantGroupMember groupMember : participantGroup.getGroupMembers())
			// Only mail confirmed members.
			if (groupMember.getParticipantConfirmed() >= 1)
			{
				asapp.models.types.ActivityBooking booking = participantGroup.getActivityBooking();
				// Sanity check.
				if (booking == null)
					// TODO: Make this a non-generic exception.
					throw new Exception("No booking attached to participant group "
							+ participantGroup.getParticipantGroupId());

				mailParticipant(groupMember.getParticipant(), booking);
			}
	}

	/**
	 * Mail the participant with the information wrt. the booking.
	 * 
	 * @param participant
	 * @param booking
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	private void mailParticipant(Participant participant, asapp.models.types.ActivityBooking booking)
			throws MessagingException, UnsupportedEncodingException
	{
		// Convenience var.
		Activity activity = booking.getActivityAvailability().getActivity();

		// Create a new message object.
		Session session = mailer.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(participant.getEmail()));
		message.setSubject("Reservering geplaatst voor activiteit " + activity.getName());
		message.setFrom(new InternetAddress("boeking@asapp.localhost.nl", "AsApp Boeking"));
		message.setSender(new InternetAddress(getBounceAddress(participant)));
		message.setContent(getBookingContent(participant, booking));

		// Send the mail.
		mailer.sendMail(message);
	}

	/**
	 * Mail the participant with the information wrt. the booking.
	 * 
	 * @param participant
	 * @param booking
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void mailRegistrationConfirmation(Participant participant) throws MessagingException,
			UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(participant.getEmail()));
		message.setSubject("Bevestiging AsApp-registratie");
		message.setFrom(new InternetAddress("asappnl@gmail.com", "AsApp Registratie"));
		message.setSender(new InternetAddress(getBounceAddress(participant)));
		message.setContent(getConfirmationContent(participant));

		// Send the mail.
		mailer.sendMail(message);
	}

	/**
	 * Mail the participant with the information wrt. the booking.
	 * 
	 * @param participant
	 * @param booking
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void mailBookingInvitation(ParticipantGroupMember groupMember)
			throws MessagingException, UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		// Convenience vars.
		Participant participant = groupMember.getParticipant();
		// Create the mime message.
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(participant.getEmail()));
		message.setSubject("Uitnodiging AsApp activiteit");
		message.setFrom(new InternetAddress("asappnl@gmail.com", "AsApp Uitnodiging"));
		message.setSender(new InternetAddress(getBounceAddress(participant)));
		message.setContent(getBookingInvitationContent(groupMember));

		// Send the mail.
		mailer.sendMail(message);
	}
	
	/**
	 * Mail the participant with cancellation of the booking.
	 * 
	 * @param participant
	 * @param booking
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	public void mailBookingCancellation(ParticipantGroupMember groupMember)
			throws MessagingException, UnsupportedEncodingException
	{
		// Create a new message object.
		Session session = mailer.getSession();
		// Convenience vars.
		Participant participant = groupMember.getParticipant();
		// Create the mime message.
		MimeMessage message = new MimeMessage(session);
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(participant.getEmail()));
		message.setSubject("Annulering AsApp activiteit");
		message.setFrom(new InternetAddress("asappnl@gmail.com", "AsApp Annulering"));
		message.setSender(new InternetAddress(getBounceAddress(participant)));
		message.setContent(getBookingCancellationContent(groupMember));

		// Send the mail.
		mailer.sendMail(message);
	}

	/**
	 * Generate the bounce address for the given participant.
	 * 
	 * @param participant
	 * @return
	 */
	private String getBounceAddress(Participant participant)
	{
		return serviceUtils.getBounceAddress("participant", participant.getParticipantId());
	}

	/**
	 * Get the booking invitation content
	 * 
	 * @param participantGroupMember
	 * @return MimeMultiPart content
	 */
	private MimeMultipart getBookingInvitationContent(ParticipantGroupMember groupMember)
			throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getBookingInvitationHtmlContent(groupMember), "text/html");
		content.addBodyPart(html);

		return content;
	}
	
	/**
	 * Get the booking cancellation content
	 * 
	 * @param participantGroupMember
	 * @return MimeMultiPart content
	 */	
	private MimeMultipart getBookingCancellationContent(ParticipantGroupMember groupMember)
			throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getBookingCancellationHtmlContent(groupMember), "text/html");
		content.addBodyPart(html);

		return content;
	}

	/**
	 * Get the mail content for a booking.
	 * 
	 * @param participant
	 * @param booking
	 * @return
	 * @throws MessagingException
	 */
	private MimeMultipart getBookingContent(Participant participant,
			asapp.models.types.ActivityBooking booking) throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getHtmlContent(participant, booking), "text/html");
		content.addBodyPart(html);

		return content;
	}

	/**
	 * Get the mail content for confirmation of joining a group.
	 * 
	 * @param participant
	 * @param booking
	 * @return
	 * @throws MessagingException
	 */
	private MimeMultipart getConfirmationContent(Participant participant) throws MessagingException
	{
		MimeMultipart content = new MimeMultipart("alternative");
		// Text part.
		// TODO: Actually implement the text body.
		MimeBodyPart text = new MimeBodyPart();
		text.setText("Als u deze tekst kunt lezen, ondersteunt uw mail-applicatie geen HTML-mail.");
		content.addBodyPart(text);
		// HTML part.
		MimeBodyPart html = new MimeBodyPart();
		html.setContent(getConfirmationHtmlContent(participant), "text/html");
		content.addBodyPart(html);

		return content;
	}

	/**
	 * Get html content for the participant confirmation mail.
	 * 
	 * @param participant
	 * @return
	 */
	private String getConfirmationHtmlContent(Participant participant)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Participant values.
		templateValues.put("first_name", participant.getFirstName());
		templateValues.put("middle_name",
				participant.getMiddleName() != null ? participant.getMiddleName() : "");
		templateValues.put("last_name", participant.getLastName());

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl("participant_confirmation.html"), templateValues);

	}

	/**
	 * Render the HTML content for the participant and the corresponding
	 * booking.
	 * 
	 * @param participant
	 * @param booking
	 * @return
	 */
	private String getHtmlContent(Participant participant,
			asapp.models.types.ActivityBooking booking)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Participant values.
		templateValues.put("first_name", participant.getFirstName());
		templateValues.put("middle_name",
				participant.getMiddleName() != null ? participant.getMiddleName() : "");
		templateValues.put("last_name", participant.getLastName());
		// Activity values.
		Activity activity = booking.getActivityAvailability().getActivity();
		// TODO: Make this a localized date.
		templateValues.put("activity_time",
				DateFormat.getDateInstance().format(booking.getDatetime()));
		templateValues.put("activity_name", activity.getName());
		templateValues.put("activity_description", activity.getDescription());
		templateValues.put("activity_street", activity.getStreet());
		templateValues.put("activity_housenum", Integer.toString(activity.getHouseNumber()));
		templateValues.put("activity_housenum_suffix", activity.getHouseNumberSuffix());
		templateValues.put("activity_zipcode", activity.getZipcode());
		templateValues.put("activity_city", activity.getCity());
		templateValues.put("supplier_name", activity.getSupplier().getName());

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl("participant_booked.html"), templateValues);
	}

	/**
	 * Get the html content for a booking invitation mail.
	 * 
	 * @param groupMember
	 * @return
	 */
	private String getBookingInvitationHtmlContent(ParticipantGroupMember groupMember)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Participant values.
		templateValues.put("first_name", groupMember.getParticipant().getFirstName());
		templateValues.put("middle_name",
				groupMember.getParticipant().getMiddleName() != null ? groupMember.getParticipant()
						.getMiddleName() : "");
		templateValues.put("last_name", groupMember.getParticipant().getLastName());
		templateValues.put("hash", groupMember.getHash());
		templateValues.put("category", groupMember.getParticipantGroup().getActivityCategory().getCategory());
		templateValues.put("activity_time",
				DateFormat.getDateInstance().format(groupMember.getParticipantGroup().getPreferredTime()));

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl("booking_invitation.html"), templateValues);
	}
	
	/**
	 * Get the html content for a booking invitation mail.
	 * 
	 * @param groupMember
	 * @return
	 */
	private String getBookingCancellationHtmlContent(ParticipantGroupMember groupMember)
	{
		// Generate template values.
		Map<String, String> templateValues = new HashMap<>();
		// Participant values.
		templateValues.put("first_name", groupMember.getParticipant().getFirstName());
		templateValues.put("middle_name",
				groupMember.getParticipant().getMiddleName() != null ? groupMember.getParticipant()
						.getMiddleName() : "");
		templateValues.put("last_name", groupMember.getParticipant().getLastName());
		templateValues.put("hash", groupMember.getHash());
		templateValues.put("category", groupMember.getParticipantGroup().getActivityCategory().getCategory());
		templateValues.put("activity_time",
				DateFormat.getDateInstance().format(groupMember.getParticipantGroup().getPreferredTime()));

		return mailTemplateRenderer.render(
				serviceUtils.getMailTemplateUrl("booking_cancellation.html"), templateValues);
	}

}
