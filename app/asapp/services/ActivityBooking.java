/**
 * 
 * File created at 3 nov. 2013 20:52:57 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asapp.models.BookingModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;
import asapp.models.types.ParticipantGroupStatus;

/**
 * Activity booking service class.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ActivityBooking
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ActivitySelection.class);
	// Required models.
	private BookingModel bookingModel;
	private ParticipantGroupModel participantGroupModel;
	// Supplier mailer.
	private SupplierMailer supplierMailer;

	@Inject
	public ActivityBooking(BookingModel bookingModel, ParticipantGroupModel participantGroupModel,
			SupplierMailer supplierMailer)
	{
		this.bookingModel = bookingModel;
		this.participantGroupModel = participantGroupModel;
		this.supplierMailer = supplierMailer;
	}

	/**
	 * Book selection for participant groups which should get a booking.
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws AddressException
	 */
	public void bookPendingSelections() throws AddressException, UnsupportedEncodingException,
			MessagingException
	{
		List<ParticipantGroup> pending = participantGroupModel.getGroupsPendingBooking();
		if (pending == null || pending.size() == 0)
		{
			logger.info("No participant groups currently need to be booked");
			return;
		}

		// Walk through the list of pending groups and book the selection.
		for (ParticipantGroup group : pending)
			try
			{
				bookSelectionForGroup(group);
			}
			catch (Exception e)
			{
				logger.error("Caught exception while trying to booking a selection for group "
						+ group.getParticipantGroupId() + ": " + e.getMessage(), e);
			}
	}

	/**
	 * Book an activity selection for a group.
	 * 
	 * @param group
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 * @throws AddressException
	 */
	private void bookSelectionForGroup(ParticipantGroup group) throws AddressException,
			UnsupportedEncodingException, MessagingException
	{
		// Check if confirmation of this group has been closed.
		if (!confirmationClosed(group))
		{
			logger.debug("Group " + group.getParticipantGroupId()
					+ " not closed for confirmation yet");
			return;
		}

		// Pick an availability.
		ActivityAvailability choice = pickAvailabilityForGroup(group);
		if (choice == null)
		{
			// No choice could be made.
			group.setStatus(ParticipantGroupStatus.INVALID);
			participantGroupModel.update(group);
		}
		else
		{
			// Book it.
			asapp.models.types.ActivityBooking booking = bookingModel.book(group, choice);
			logger.info("Booked availability " + choice.getActivityAvailabilityId() + " on "
					+ choice.getDateTime() + " for participant group "
					+ group.getParticipantGroupId());

			supplierMailer.mailBookingInformation(booking.getActivityAvailability().getActivity()
					.getSupplier(), booking);
		}
	}

	/**
	 * Check if the confirmation closing time has been reached for the given
	 * group.
	 * 
	 * @param group
	 * @return
	 */
	private boolean confirmationClosed(ParticipantGroup group)
	{
		// The closing data is the preferred time minus the notification
		// timeout. The notification timeout is in days and getTime() returns
		// the time in ms, so it needs to be multiplied by 86400 (1 day) and
		// 1000 (ms).
		Date closingDate = new Date(group.getPreferredTime().getTime()
				- (group.getNotificationTimeout() * 86400 * 1000));
		// If the current time is after the closing date, return true.
		return new Date().after(closingDate);
	}

	/**
	 * Pick an availability from the availabilities attached to the group.
	 * 
	 * @param group
	 * @return
	 */
	private ActivityAvailability pickAvailabilityForGroup(ParticipantGroup group)
	{
		// There should be multiple ActivityAvailable entries attached to the
		// participant group. From those entries, one needs to be selected and
		// booked.
		Set<ActivityAvailability> availabilities = group.getActivityAvailabilities();
		if (availabilities.size() == 0)
		{
			logger.error("Participant group " + group.getParticipantGroupId()
					+ " has no availabilities attached. Marking as invalid.");
			return null;
		}

		// Check the number of participants.
		int numParticipants = 0;
		for (ParticipantGroupMember groupMember : group.getGroupMembers())
			if (groupMember.getParticipantConfirmed() > 0)
				numParticipants++;
		if (numParticipants == 0)
		{
			logger.error("No confirmed participants attached to group "
					+ group.getParticipantGroupId());
			return null;
		}

		// Return the first activity availability matching the number of
		// participants.
		for (ActivityAvailability availability : group.getActivityAvailabilities())
			if (numParticipants >= availability.getMinParticipants()
					&& numParticipants <= availability.getMaxParticipants())
				return availability;

		// If we get here, no availability matches.
		logger.error("No availability entries found matching " + numParticipants
				+ " participants in group " + group.getParticipantGroupId());
		return null;
	}
}
