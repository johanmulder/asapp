/**
 * 
 * File created at 9 nov. 2013 23:11:20 by Johan Mulder <johan@mulder.net>
 */
package asapp;

import static play.Play.application;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class AsAppConfig
{

	/**
	 * Get a config item by key.
	 * 
	 * @param key
	 * @return
	 */
	public String getString(String key)
	{
		return application().configuration().getString(key);
	}

	/**
	 * Get a config item by key. Returns an int.
	 * 
	 * @param key
	 * @return
	 */
	public int getInt(String key)
	{
		return application().configuration().getInt(key);
	}

	/**
	 * Get a config item by key. Returns a boolean.
	 * 
	 * @param key
	 * @return
	 */
	public boolean getBoolean(String key)
	{
		return application().configuration().getBoolean(key);
	}
}
