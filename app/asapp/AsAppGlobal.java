/**
 * 
 * File created at 20 okt. 2013 20:04:17 by Johan Mulder <johan@mulder.net>
 */
package asapp;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.Configuration;
import play.GlobalSettings;
import play.libs.F.Callback0;
import asapp.di.InjectorModule;
import asapp.services.ActivityBooking;
import asapp.services.ActivitySelection;
import asapp.services.ParticipantMailer;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Global settings class for the AsApp application.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class AsAppGlobal extends GlobalSettings
{
	// The guice dependency injector.
	private static final Injector INJECTOR = createInjector();
	// Logger class.
	private static Logger logger = LoggerFactory.getLogger(AsAppGlobal.class);
	// Service threads.
	private List<Thread> runningServices = new ArrayList<>();

	/**
	 * Create the guice dependency injector.
	 * 
	 * @return
	 */
	private static Injector createInjector()
	{
		return Guice.createInjector(new InjectorModule());
	}

	/**
	 * Do global initialization of the application.
	 */
	@Override
	public void onStart(play.Application app)
	{
		// Call the parent onStart method.
		super.onStart(app);

		Configuration config = app.configuration();

		// Initialize service classes.
		if (config.getBoolean("asapp.service.booking.enable"))
			initializeActivityBookingService(config.getInt("asapp.service.booking.sleeptime") * 1000);
		if (config.getBoolean("asapp.service.activity_selection.enable"))
			initializeActivitySelectionService(config
					.getInt("asapp.service.activity_selection.sleeptime") * 1000);
		if (config.getBoolean("asapp.service.participant_mailer.enable"))
			initializeParticipantMailerService(config
					.getInt("asapp.service.participant_mailer.sleeptime") * 1000);

		logger.info("AsApp application started");
	}

	/**
	 * Create a controller instance for the given class name. This will only be
	 * executed when a controller is marked as non static in the route
	 * definitions.
	 */
	@Override
	public <T> T getControllerInstance(Class<T> clazz)
	{
		// Return a new instance for the given controller class.
		return INJECTOR.getInstance(clazz);
	}

	/**
	 * Handle the stop actions in case the application gets stopped.
	 */
	@Override
	public void onStop(play.Application app)
	{
		// If the activity service exists and is running, stop it.
		for (Thread service : runningServices)
		{
			service.interrupt();
			runningServices.remove(service);
		}
	}

	private void initializeParticipantMailerService(int sleepTime)
	{
		// The service class. Run the activity selection every 5 minutes
		// (300k ms).
		ServiceClass service = new ServiceClass("participant mailer", sleepTime, new Callback0()
		{

			@Override
			public void invoke() throws Throwable
			{
				// Instantiate the activity selection service class.
				final ParticipantMailer selector = INJECTOR.getInstance(ParticipantMailer.class);
				// Run the mail service.
				selector.mailBookedParticipants();
			}
		});

		// Start the service in the background.
		startService(service);

		logger.info("Particiant mailer service started");
	}

	private void initializeActivitySelectionService(int sleepTime)
	{
		// The service class. Run the activity selection every 5 minutes
		// (300k ms).
		ServiceClass service = new ServiceClass("activity selection", sleepTime, new Callback0()
		{

			@Override
			public void invoke() throws Throwable
			{
				// Instantiate the activity selection service class.
				final ActivitySelection selector = INJECTOR.getInstance(ActivitySelection.class);
				// Run the selection.
				selector.selectActivitiesForPendingGroups();
			}
		});

		// Start the service in the background.
		startService(service);

		logger.info("Activity selection service started");
	}

	private void startService(ServiceClass service)
	{
		Thread thread = new Thread(service);
		thread.start();
		runningServices.add(thread);
	}

	/**
	 * Initialize the activity booking service and start it in the background.
	 */
	private void initializeActivityBookingService(int sleepTime)
	{
		// The service class. Run the activity booking every 5 minutes
		// (300k ms).
		ServiceClass service = new ServiceClass("activity booking selection", sleepTime,
				new Callback0()
				{

					@Override
					public void invoke() throws Throwable
					{
						// Instantiate the activity selection service class.
						final ActivityBooking booker = INJECTOR.getInstance(ActivityBooking.class);
						// Run the selection.
						booker.bookPendingSelections();
					}
				});

		// Start the service in the background.
		startService(service);
		logger.info("Activity booking service started");
	}

	/**
	 * Generic service class which should be used to start services with. As it
	 * implements Runnable, it can be started with a new Thread. Interrupting
	 * the thread will exit the run loop.
	 * 
	 * @author Johan Mulder <johan@mulder.net>
	 */
	private static class ServiceClass implements Runnable
	{
		private final Callback0 callback;
		private final String name;
		private final long sleepTime;

		/**
		 * ServiceClass constructor.
		 * 
		 * @param name The name of the service.
		 * @param sleepTime The amount of milliseconds to sleep in between
		 *            loops.
		 * @param callback A play callback object.
		 */
		public ServiceClass(String name, long sleepTime, Callback0 callback)
		{
			this.callback = callback;
			this.name = name;
			this.sleepTime = sleepTime;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			// Flag indicating whether or not the thread should keep on
			// going.
			boolean keepRunning = true;
			while (keepRunning)
			{
				try
				{
					// As there is no entity manager bound to this thread,
					// it needs to be wrapped inside a play/JPA callback
					// class.
					play.db.jpa.JPA.withTransaction(callback);
				}
				catch (Exception e)
				{
					logger.error("Exception caught while running the " + this.name + " service", e);
				}

				// Sleep until the next execution.
				try
				{
					// Sleep for this amount of milliseconds.
					Thread.sleep(sleepTime);
				}
				catch (InterruptedException e)
				{
					logger.info("Stopping " + this.name + "service");
					keepRunning = false;
				}
			}
			logger.info("Service " + this.name + " stopped");
		}
	}
}
