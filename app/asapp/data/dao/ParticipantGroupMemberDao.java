/**
 * 
 * File created at 27 okt. 2013 14:16:56 by Vincent Vogelesang
 */
package asapp.data.dao;

import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;

import java.util.List;

/**
 * 
 * @author Vincent Vogelesang
 */
public interface ParticipantGroupMemberDao
{
	/**
	 * Create a new participantGroupMember.
	 * 
	 * @param participantGroupMember
	 */
	public void create(ParticipantGroupMember participantGroupMember);

	/**
	 * Update an existing participantGroupMember.
	 * 
	 * @param participantGroupMember
	 */
	public void update(ParticipantGroupMember participantGroupMember);

	/**
	 * Delete a given participantGroupMember.
	 * 
	 * @param participantGroupMember
	 */
	public void delete(ParticipantGroupMember participantGroupMember);

	/**
	 * Get a participantGroupMember by Participant
	 * 
	 * @param Participant
	 * @return ParticipantGroupMember object
	 */	
	public ParticipantGroupMember getByParticipant(Participant participant);
	
	/**
	 * Get a participantGroupMember by ParticipantGroup
	 * 
	 * @param ParticipantGroup
	 * @return ParticipantGroupMember object
	 */	
	public ParticipantGroupMember getByParticipantGroup(ParticipantGroup participantGroup);	
	
	/**
	 * Deleta a participantGroupMember by ParticipantGroup.
	 * 
	 * @param participantGroup
	 */
	public void deleteByParticipantGroup(ParticipantGroup participantGroup);	
	
	/**
	 * Get the initiator of a ParticipantGroupMember by ParticipantGroup
	 * 
	 * @param participantGroup
	 */
	public Participant getInitiatorByParticipantGroup(ParticipantGroup participantGroup);
	
	/**
	 * Get all participantGroupMembers created by Participant
	 * 
	 * @param Participant
	 * @return List with ParticipantGroupMember objects
	 */	
	public List<ParticipantGroupMember> getBookedListByParticipant(Participant participant);
	
	/**
	 * Get the ParticipantGroupMember by hash
	 * 
	 * @param sha-1 40 digit hash String
	 * @return ParticipantGroupMember
	 */
	public ParticipantGroupMember getByHash(String hash);

	/**
	 * Set the ParticipantConfirmed by hash
	 * 
	 * @param sha-1 40 digit hash String
	 * @param int value 0 or 1
	 * 
	 */
	public void setParticipantConfirmedByHash(String hash, int participantConfirmed);
}
