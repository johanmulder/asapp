/**
 * 
 * File created at 3 nov. 2013 21:50:55 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import asapp.models.types.ActivityBooking;

/**
 * Activity booking DAO interface.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface ActivityBookingDao
{
	/**
	 * Create a new activity booking.
	 * 
	 * @param booking
	 */
	public void create(ActivityBooking booking);
	
	/**
	 * Delete an activity booking
	 * 
	 * @param booking
	 */
	public void delete(ActivityBooking booking);

}
