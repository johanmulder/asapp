/**
 * 
 * File created at 23 okt. 2013 20:46:12 by Vincent Vogelesang
 */
package asapp.data.dao;

import java.util.List;
import java.util.Date;

import asapp.models.types.Activity;
import asapp.models.types.ActivityCategory;
import asapp.models.types.Supplier;

/**
 * 
 * @author Vincent Vogelesang
 */
public interface ActivityDao
{
	/**
	 * Create a new activity entry.
	 * 
	 * @param activity
	 */
	public void create(Activity activity);

	/**
	 * Update an existing activity entry.
	 * 
	 * @param activity
	 */
	public void update(Activity activity);

	/**
	 * Delete an activity entry.
	 * 
	 * @param activity
	 */
	public void delete(Activity activity);

	/**
	 * Get an activity entry by id.
	 * 
	 * @param id
	 * @return
	 */
	public Activity getById(int id);

	/**
	 * Get an activity entry by SupplierId.
	 * 
	 * @param SupplierId
	 * @return
	 */
	public List<Activity> getBySupplierId(int supplierId);
	
	/**
	 * Get an activity entry by Supplier
	 * 
	 * @param Supplier
	 * @return
	 */
	public List<Activity> getBySupplier(Supplier supplier);

	/**
	 * Get an activity list by ActivityCategory.
	 * 
	 * @param activityCategory
	 * @return
	 */
	public List<Activity> getByCategory(ActivityCategory activityCategory);

	/**
	 * Get an activity list by activity category
	 * 
	 * @param activityCategory The category name.
	 * @return
	 */
	public List<Activity> getByCategory(String category);
	
	/**
	 * Get a booking count for Activity by given period
	 * 
	 * @param supplierId
	 * @return integer booking count
	 */	
	public int getBookingCountByPeriod(Activity activity, Date startDate, Date endDate);
	
	/**
	 * Get a tarrif total for Activity by given period
	 * 
	 * @param activity object
	 * @param date start
	 * @param date end
	 * @return integer tariff total
	 */	
	public int getTariffTotalByPeriod(Activity activity, Date startDate, Date endDate);
	
}
