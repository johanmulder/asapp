/**
 * 
 * File created at  25 okt. 2013 16:19:12 by Chakir Bouziane
 */
package asapp.data.dao;

import java.util.List;

import asapp.models.types.ActivityCategory;

/**
 * 
 * @author Chakir Bouziane
 */
public interface ActivityCategoryDao
{
	/**
	 * Create a new activityCategory entry.
	 * 
	 * @param activityCategory
	 */
	public void create(ActivityCategory activityCategory);

	/**
	 * Update an existing activityCategory entry.
	 * 
	 * @param activityCategory
	 */
	public void update(ActivityCategory activityCategory);

	/**
	 * Delete an activityCategory entry.
	 * 
	 * @param activityCategory
	 */
	public void delete(ActivityCategory activityCategory);

	/**
	 * Get an activityCategory entry by id.
	 * 
	 * @param id
	 * @return
	 */
	public ActivityCategory getById(int id);

	/**
	 * Get a list of all ActivityCategoryCategories.
	 * 
	 * @return list of ActivityCategoryCategories
	 */
	public List<ActivityCategory> getAll();

	/**
	 * Get a ctivityCategory entry by category name.
	 * 
	 * @param category
	 * @return
	 */
	public ActivityCategory getByCategory(String category);
}