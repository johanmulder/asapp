/**
 * 
 * File created at 22 okt. 2013 23:16:09 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import asapp.models.types.Role;
import asapp.models.types.User;

import java.util.List;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface UserDao
{
	/**
	 * Create a new user entry.
	 * 
	 * @param user
	 */
	public void create(User user);

	/**
	 * Update an existing user entry.
	 * 
	 * @param user
	 */
	public void update(User user);

	/**
	 * Delete a user entry.
	 * 
	 * @param user
	 */
	public void delete(User user);

	/**
	 * Get a user entry by email address.
	 * 
	 * @param username
	 * @return
	 */
	public User getByUsername(String username);

	/**
	 * Get a user by name and role.
	 * 
	 * @param username
	 * @param role
	 * @return
	 */
	public User getUserByNameAndRole(String username, Role role);

	/**
	 * Get a user by name and role name.
	 * 
	 * @param username
	 * @param roleName The name of the role.
	 * @return
	 */
	public User getUserByNameAndRole(String username, String roleName);

	/**
	 * Get a user by id
	 * 
	 * @param id
	 * @return
	 */
	public User getById(int Id);

	/**
	 * Get a list of users
	 * 
	 * @return
	 */
	public List<User> getAllUsers();
}
