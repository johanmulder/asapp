/**
 * 
 * File created at 20 okt. 2013 22:39:14 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import asapp.models.types.Supplier;
import asapp.models.types.User;

/**
 * Interfacing defining methods for the SupplierDao interface.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface SupplierDao
{
	/**
	 * Create a new supplier.
	 * 
	 * @param supplier
	 */
	public void create(Supplier supplier);

	/**
	 * Update an existing supplier.
	 * 
	 * @param supplier
	 */
	public void update(Supplier supplier);

	/**
	 * Delete a given supplier.
	 * 
	 * @param supplier
	 */
	public void delete(Supplier supplier);

	/**
	 * Get a supplier by id.
	 * 
	 * @param id
	 */
	public Supplier getById(int id);

	/**
	 * Get a supplier by email. As the email is a unique key, only one result
	 * can be returned, or null if not found.
	 * 
	 * @param email
	 * @return
	 */
	public Supplier getByEmail(String email);

	/**
	 * Get a supplier by user.
	 * 
	 * @param User user
	 */
	public Supplier getByUser(User user);
}
