/**
 * 
 * File created at 27 okt. 2013 11:36:56 by Vincent Vogelesang
 */
package asapp.data.dao;

import java.util.List;

import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;

/**
 * 
 * @author Vincent Vogelesang
 */
public interface ParticipantGroupDao
{
	/**
	 * Create a new participantGroup.
	 * 
	 * @param participantGroup
	 */
	public void create(ParticipantGroup participantGroup);

	/**
	 * Update an existing participantGroup.
	 * 
	 * @param participantGroup
	 */
	public void update(ParticipantGroup participantGroup);

	/**
	 * Delete a given participantGroup.
	 * 
	 * @param participantGroup
	 */
	public void delete(ParticipantGroup participantGroup);

	/**
	 * Get a participantGroup by id.
	 * 
	 * @param id
	 * @return
	 */
	public ParticipantGroup getById(int id);

	/**
	 * Get a list of participant groups by status.
	 * 
	 * @param status
	 * @return
	 */
	public List<ParticipantGroup> getByStatus(ParticipantGroupStatus status);
}
