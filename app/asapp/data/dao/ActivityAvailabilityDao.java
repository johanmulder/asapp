/**
 * 
 * File created at 30 okt. 2013 20:46:00 by Chakir Bouziane
 */
package asapp.data.dao;

import java.util.List;

import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityCategory;

/**
 * Interface defining the methods for CRUD operations for the activity
 * availability entries.
 * 
 * @author Chakir Bouziane
 * @author Johan Mulder <johan@mulder.net>
 */
public interface ActivityAvailabilityDao
{
	/**
	 * Create a new activity availability entry.
	 * 
	 * @param activityAvailability
	 */
	public void create(ActivityAvailability activityAvailability);

	/**
	 * Update an existing activity entry.
	 * 
	 * @param activityAvailability
	 */
	public void update(ActivityAvailability activityAvailability);

	/**
	 * Delete an activity entry.
	 * 
	 * @param activityAvailability
	 */
	public void delete(ActivityAvailability activityAvailability);

	/**
	 * Get an ActivityAvailability entry by id.
	 * 
	 * @param id
	 * @return
	 */
	public ActivityAvailability getById(int id);

	/**
	 * Get an ActivityAvailability entry by activityId.
	 * 
	 * @param activityId
	 * @return
	 */
	public List<ActivityAvailability> getByActivityId(int activityId);

	/**
	 * Get an ActivityAvailability entry by activityId.
	 * 
	 * @param activity
	 * @return
	 */
	public List<ActivityAvailability> getByActivity(Activity activity);

	/**
	 * Get ActivityAvailability entries by category.
	 * 
	 * @param category
	 * @return
	 */
	public List<ActivityAvailability> getByCategory(ActivityCategory category);

}