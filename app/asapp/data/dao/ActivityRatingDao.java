/**
 * 
 * File created at 3 nov. 2013 14:06:42 by Vincent Vogelesang
 */
package asapp.data.dao;

import java.util.List;

import asapp.models.types.Activity;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityRating;

/**
 * 
 * @author Vincent Vogelesang
 */
public interface ActivityRatingDao
{
	/**
	 * Create a new activityRating entry.
	 * 
	 * @param activityRating
	 */
	public void create(ActivityRating activityRating);

	/**
	 * Update an existing activityRating entry.
	 * 
	 * @param activityRating
	 */
	public void update(ActivityRating activityRating);

	/**
	 * Delete an activityRating entry.
	 * 
	 * @param activityRating
	 */
	public void delete(ActivityRating activityRating);

	/**
	 * Get an activityRating entry by id.
	 * 
	 * @param id
	 * @return ActivityRating
	 */
	public ActivityRating getById(int id);

	/**
	 * Get an average activityRating by Activity.
	 * 
	 * @param Activity
	 * @return int rating (1-10)
	 */
	public int getAvgRatingByActivity(Activity activity);

	/**
	 * Get an activityRating list by Activity
	 * 
	 * @param Activity
	 * @return ActivityRating list
	 */
	public List<ActivityRating> getRatingsByActivity(Activity activity);

	/**
	 * Get top10 for ActivityCategory
	 * 
	 * @param ActivityCategory
	 * @return rating list
	 */
	public List<String> getTop10ByCategory(ActivityCategory activityCategory);

}
