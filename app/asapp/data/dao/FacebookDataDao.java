/**
 * 
 * File created at 9 nov. 2013 21:20:19 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import java.math.BigDecimal;

import asapp.models.types.FacebookData;

/**
 * DAO handling CRUD actions for the facebook data.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface FacebookDataDao
{
	/**
	 * Create a new FacebookData.
	 * 
	 * @param data
	 */
	public void create(FacebookData data);

	/**
	 * Update an existing FacebookData.
	 * 
	 * @param data
	 */
	public void update(FacebookData data);

	/**
	 * Delete a FacebookData.
	 * 
	 * @param data
	 */
	public void delete(FacebookData data);

	/**
	 * Get a FacebookData by user id.
	 * 
	 * @param uid
	 * @return
	 */
	public FacebookData getByUserId(BigDecimal uid);
}
