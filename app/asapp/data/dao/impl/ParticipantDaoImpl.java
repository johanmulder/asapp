/**
 * 
 * File created at 20 okt. 2013 21:29:35 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ParticipantDao;
import asapp.models.types.Participant;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ParticipantDaoImpl extends DaoBase implements ParticipantDao
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ParticipantDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantDao#create(asapp.models.Participant)
	 */
	@Override
	@Transactional
	public void create(Participant participant)
	{
		// TODO Auto-generated method stub
		logger.debug("Creating participant with email address " + participant.getEmail());
		getEntityManager().persist(participant);
		logger.debug("Created participant with id " + participant.getParticipantId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantDao#update(asapp.models.Participant)
	 */
	@Override
	@Transactional
	public void update(Participant participant)
	{
		getEntityManager().persist(participant);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantDao#delete(asapp.models.Participant)
	 */
	@Override
	@Transactional
	public void delete(Participant participant)
	{
		getEntityManager().remove(participant);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantDao#getById(int)
	 */
	@Override
	@Transactional
	public Participant getById(int id)
	{
		return getEntityManager().find(Participant.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantDao#getByEmail(java.lang.String)
	 */
	@Override
	@Transactional
	public Participant getByEmail(String email)
	{
		// TODO Auto-generated method stub
		logger.debug("Searching for participant with email address " + email);

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from Participant p where email = :email",
							Participant.class).setParameter("email", email).getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}

	}
}
