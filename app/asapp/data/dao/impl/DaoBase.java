/**
 * 
 * File created at 20 okt. 2013 22:41:45 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import javax.persistence.EntityManager;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
abstract class DaoBase
{
	/**
	 * Get the entity manager.
	 * 
	 * @return
	 */
	protected EntityManager getEntityManager()
	{
		return play.db.jpa.JPA.em();
	}

}
