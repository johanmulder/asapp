/**
 * 
 * File created at 30 okt. 2013 21:06:35 by Chakir Bouziane
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ActivityAvailabilityDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityCategory;

/**
 * Implementation of ActivityAvailabilityDao.
 * 
 * @author Chakir Bouziane
 * @author Johan Mulder <johan@mulder.net>
 */
public class ActivityAvailabilityDaoImpl extends DaoBase implements ActivityAvailabilityDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(ActivityAvailabilityDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityAvailabilityDao#create(asapp.models.
	 * ActivityAvailability)
	 */
	@Override
	@Transactional
	public void create(ActivityAvailability activityAvailability)
	{
		getEntityManager().persist(activityAvailability);
		logger.debug("Created activityAvailability with id "
				+ activityAvailability.getActivityAvailabilityId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityAvailabilityDao#update(asapp.models.
	 * ActivityAvailability)
	 */
	@Override
	@Transactional
	public void update(ActivityAvailability activityAvailability)
	{
		getEntityManager().persist(activityAvailability);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityAvailabilityDao#delete(asapp.models.
	 * ActivityAvailability)
	 */
	@Override
	@Transactional
	public void delete(ActivityAvailability activityAvailability)
	{
		getEntityManager().remove(activityAvailability);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityActivityAvailabilityDao#getById(int)
	 */
	@Override
	@Transactional
	public ActivityAvailability getById(int id)
	{
		return getEntityManager().find(ActivityAvailability.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getBySupplierId(int)
	 */
	@Override
	@Transactional
	public List<ActivityAvailability> getByActivityId(int id)
	{
		logger.debug("Searching for activity with ActivityId " + id);

		return getEntityManager()
				.createQuery("SELECT p from ActivityAvailability p where activity = :id",
						ActivityAvailability.class).setParameter("activity", id).getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getByActivityCategory(asapp.models.types.
	 * ActivityCategory)
	 */
	@Override
	@Transactional
	public List<ActivityAvailability> getByActivity(Activity activity)
	{
		logger.debug("Searching for activity with category " + activity);

		return getEntityManager()
				.createQuery(
						"SELECT p from ActivityAvailability p where activity like :activity order by activity",
						ActivityAvailability.class).setParameter("activity", activity)
				.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityAvailabilityDao#getByCategory(asapp.models.types
	 * .ActivityCategory)
	 */
	@Override
	@Transactional
	public List<ActivityAvailability> getByCategory(ActivityCategory category)
	{
		logger.debug("Searching for ActivityAvailability entries with category "
				+ category.getCategory());

		String query = "SELECT aa from ActivityAvailability aa JOIN aa.activity a JOIN a.activityCategory "
				+ "WHERE a.activityCategory = :category ORDER BY aa.activity";
		return getEntityManager().createQuery(query, ActivityAvailability.class)
				.setParameter("category", category).getResultList();
	}

}
