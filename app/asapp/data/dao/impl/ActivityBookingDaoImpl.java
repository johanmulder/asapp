/**
 * 
 * File created at 3 nov. 2013 21:51:53 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import asapp.data.dao.ActivityBookingDao;
import asapp.models.types.ActivityBooking;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ActivityBookingDaoImpl extends DaoBase implements ActivityBookingDao
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityBookingDao#create(asapp.models.types.ActivityBooking
	 * )
	 */
	@Override
	public void create(ActivityBooking booking)
	{
		getEntityManager().persist(booking);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityBookingDao#create(asapp.models.types.ActivityBooking
	 * )
	 */
	@Override
	public void delete(ActivityBooking booking)
	{
		getEntityManager().remove(booking);
	}

}
