/**
 * 
 * File created at 25 okt. 2013 16:27:35 by Chakir Bouziane
 */
package asapp.data.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ActivityCategoryDao;
import asapp.models.types.ActivityCategory;

/**
 * 
 * @author Chakir bouziane
 */
public class ActivityCategoryDaoImpl extends DaoBase implements ActivityCategoryDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(ActivityCategoryDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityCategoryDao#create(asapp.models.ActivityCategory)
	 */
	@Override
	@Transactional
	public void create(ActivityCategory activityCategory)
	{
		getEntityManager().persist(activityCategory);
		logger.debug("Created activityCategory with id " + activityCategory.getCategory());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityCategoryDao#update(asapp.models.ActivityCategory)
	 */
	@Override
	@Transactional
	public void update(ActivityCategory activityCategory)
	{
		getEntityManager().persist(activityCategory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ActivityCategoryDao#delete(asapp.models.ActivityCategory)
	 */
	@Override
	@Transactional
	public void delete(ActivityCategory activityCategory)
	{
		getEntityManager().remove(activityCategory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityCategoryDao#getById(int)
	 */
	@Override
	@Transactional
	public ActivityCategory getById(int id)
	{
		return getEntityManager().find(ActivityCategory.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityCategory#getAll(asapp.models.types.
	 * ActivityCategory)
	 */
	@Override
	@Transactional
	public List<ActivityCategory> getAll()
	{
		logger.debug("Fetching all the activityCategory categories ");

		try
		{
			return getEntityManager().createQuery("from ActivityCategory", ActivityCategory.class)
					.getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#getByCategory(java.lang.String)
	 */
	@Override
	@Transactional
	public ActivityCategory getByCategory(String category)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT c from ActivityCategory c where category = :category",
							ActivityCategory.class).setParameter("category", category)
					.getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}
}
