/**
 * 
 * File created at 22 okt. 2013 23:20:46 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;

import play.db.jpa.Transactional;
import asapp.data.dao.UserDao;
import asapp.models.types.Role;
import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class UserDaoImpl extends DaoBase implements UserDao
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#create(asapp.models.User)
	 */
	@Override
	@Transactional
	public void create(User user)
	{
		getEntityManager().persist(user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#update(asapp.models.User)
	 */
	@Override
	@Transactional
	public void update(User user)
	{
		getEntityManager().persist(user);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#delete(asapp.models.User)
	 */
	@Override
	@Transactional
	public void delete(User user)
	{
		getEntityManager().remove(user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#getByUsername(java.lang.String)
	 */
	@Override
	@Transactional
	public User getByUsername(String username)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT u from User u where username = :username", User.class)
					.setParameter("username", username).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#getUserByNameAndRole(java.lang.String,
	 * asapp.auth.Role)
	 */
	@Override
	@Transactional
	public User getUserByNameAndRole(String username, Role role)
	{
		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT u from User u where username = :username AND role = :role",
							User.class).setParameter("username", username)
					.setParameter("role", role).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#getUserByNameAndRole(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	@Transactional
	public User getUserByNameAndRole(String username, String roleName)
	{
		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT u from User u JOIN u.role r WHERE username = :username AND r.name = :role",
							User.class).setParameter("username", username)
					.setParameter("role", roleName).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}

	}

	@Override
	public User getById(int Id)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT u from User u where id = :id", User.class)
					.setParameter("id", Id).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.UserDao#getAllUsers()
	 */
	@Override
	@Transactional
	public List<User> getAllUsers()
	{
		logger.debug("Getting all users accounts");

		try
		{
			return getEntityManager().createQuery("SELECT u from User u", User.class)
					.getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

}
