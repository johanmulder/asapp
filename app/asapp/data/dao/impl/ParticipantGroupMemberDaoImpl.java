/**
 * 
 * File created at 27 okt. 2013 11:38:56 by Vincent Vogelesang
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ParticipantGroupMemberDao;
import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ParticipantGroupMemberDaoImpl extends DaoBase implements ParticipantGroupMemberDao
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ParticipantGroupMemberDaoImpl.class);

	/**
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupMemberDao#create(asapp.models.ParticipantGroupMember)
	 */
	@Override
	@Transactional
	public void create(ParticipantGroupMember participantGroupMember)
	{
		logger.debug("Creating participantGroupMember");
		getEntityManager().persist(participantGroupMember);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupMemberDao#update(asapp.models.ParticipantGroupMember)
	 */
	@Override
	@Transactional
	public void update(ParticipantGroupMember participantGroupMember)
	{
		getEntityManager().persist(participantGroupMember);
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupMemberDao#delete(asapp.models.ParticipantGroupMember)
	 */
	@Override
	@Transactional
	public void delete(ParticipantGroupMember participantGroupMember)
	{
		getEntityManager().remove(participantGroupMember);
	}

	/**
	 * Get a participantGroupMember by Participant
	 * 
	 * @param Participant
	 * @return ParticipantGroupMember object
	 */
	@Override
	@Transactional
	public ParticipantGroupMember getByParticipant(Participant participant)
	{
		return getEntityManager().find(ParticipantGroupMember.class, participant);
	}

	/**
	 * Get a participantGroupMember by ParticipantGroup
	 * 
	 * @param ParticipantGroup
	 * @return ParticipantGroupMember object
	 */
	@Override
	@Transactional
	public ParticipantGroupMember getByParticipantGroup(ParticipantGroup participantGroup)
	{
		return getEntityManager().find(ParticipantGroupMember.class, participantGroup);
	}

	/**
	 * Get the initiator of a ParticipantGroupMember by ParticipantGroup
	 * 
	 * @param participantGroup
	 */
	@Override
	@Transactional
	public Participant getInitiatorByParticipantGroup(ParticipantGroup participantGroup)
	{
		logger.debug("Searching for ParticipantGroupMember initator for grp"
				+ participantGroup.getParticipantGroupId());

		try
		{
			ParticipantGroupMember participantGroupMember = getEntityManager()
					.createQuery(
							"SELECT p from ParticipantGroupMember p where participantGroup = :participantGroup and initiator = 1",
							ParticipantGroupMember.class)
					.setParameter("participantGroup", participantGroup).getSingleResult();
			return participantGroupMember.getParticipant();
		}
		catch (Exception e)
		{
			logger.debug("Error " + e);
			return null;
		}
	}

	/**
	 * Deleta a participantGroupMember by ParticipantGroup.
	 * 
	 * @param participantGroup
	 */
	@Override
	@Transactional
	public void deleteByParticipantGroup(ParticipantGroup participantGroup)
	{
		logger.debug("Deleting for ParticipantGroup" + participantGroup.getParticipantGroupId());

		try
		{
			getEntityManager()
					.createQuery(
							"DELETE FROM ParticipantGroupMember p where participantGroup = :participantGroup")
					.setParameter("participantGroup", participantGroup).executeUpdate();
		}
		catch (Exception e)
		{
			logger.debug("Error " + e);
		}
	}

	/**
	 * Get all participantGroupMembers created by Participant
	 * 
	 * @param Participant
	 * @return List with ParticipantGroupMember objects
	 */
	@Override
	@Transactional
	public List<ParticipantGroupMember> getBookedListByParticipant(Participant participant)
	{
		logger.debug("Searching for ParticipantGroupMembers for participant"
				+ participant.getParticipantId());

		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT p from ParticipantGroupMember p where participantGroup in ("
									+ "SELECT participantGroup FROM ParticipantGroupMember "
									+ "where participant = :participant and initiator = 1)",
							ParticipantGroupMember.class).setParameter("participant", participant)
					.getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupMemberDao#getByHash(java.lang.String)
	 */
	@Override
	public ParticipantGroupMember getByHash(String hash)
	{
		logger.debug("Searching for ParticipantGroupMember initator for hash" + hash);

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from ParticipantGroupMember p where hash = :hash",
							ParticipantGroupMember.class).setParameter("hash", hash)
					.getSingleResult();
		}
		catch (Exception e)
		{
			logger.debug("Error " + e);
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ParticipantGroupMemberDao#setParticipantConfirmedByHash
	 * (java.lang.String, int)
	 */
	@Override
	public void setParticipantConfirmedByHash(String hash, int participantConfirmed)
	{
		logger.debug("Updating confirmed value " + participantConfirmed + " for hash" + hash);

		try
		{
			getEntityManager()
					.createQuery(
							"UPDATE ParticipantGroupMember SET participantConfirmed = :participantConfirmed where hash = :hash")
					.setParameter("participantConfirmed", participantConfirmed)
					.setParameter("hash", hash).executeUpdate();
		}
		catch (Exception e)
		{
			logger.debug("Error " + e);
		}
	}
}
