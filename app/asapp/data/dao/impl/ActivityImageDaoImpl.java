/**
 * 
 * File created at 25 okt. 2013 12:38:45 by Vincent Vogelesang
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ActivityImageDao;
import asapp.models.types.ActivityImage;
import asapp.models.types.Activity;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ActivityImageDaoImpl extends DaoBase implements ActivityImageDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(ActivityImageDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityImageDao#create(asapp.models.ActivityImage)
	 */
	@Override
	@Transactional
	public void create(ActivityImage activityImage)
	{
		getEntityManager().persist(activityImage);
		logger.debug("Created activityImage with id " + activityImage.getActivityImageId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityImageDao#update(asapp.models.ActivityImage)
	 */
	@Override
	@Transactional
	public void update(ActivityImage activityImage)
	{
		getEntityManager().persist(activityImage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityImageDao#delete(asapp.models.ActivityImage)
	 */
	@Override
	@Transactional
	public void delete(ActivityImage activityImage)
	{
		getEntityManager().remove(activityImage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityImageDao#getById(int)
	 */
	@Override
	@Transactional
	public ActivityImage getById(int id)
	{
		return getEntityManager().find(ActivityImage.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityImageDao#getByActivityId(int)
	 */
	@Override
	@Transactional
	public List<ActivityImage> getByActivity(Activity activity)
	{
		logger.debug("Searching for activityImage with ActivityId " + activity.getActivityId());

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from ActivityImage p where activity = :activity", ActivityImage.class)
					.setParameter("activity", activity).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}

	}

}
