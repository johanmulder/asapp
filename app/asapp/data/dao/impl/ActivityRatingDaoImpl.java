/**
 * 
 * File created at 3 nov. 2013 14:36:42 by Vincent Vogelesang
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ActivityRatingDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityRating;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ActivityRatingDaoImpl extends DaoBase implements ActivityRatingDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(ActivityRatingDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityRatingDao#create(asapp.models.ActivityRating)
	 */
	@Override
	@Transactional
	public void create(ActivityRating activityRating)
	{
		getEntityManager().persist(activityRating);
		logger.debug("Created activityRating with id " + activityRating.getActivityRatingId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityRatingDao#update(asapp.models.ActivityRating)
	 */
	@Override
	@Transactional
	public void update(ActivityRating activityRating)
	{
		getEntityManager().persist(activityRating);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityRatingDao#delete(asapp.models.ActivityRating)
	 */
	@Override
	@Transactional
	public void delete(ActivityRating activityRating)
	{
		getEntityManager().remove(activityRating);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityRatingDao#getById(int)
	 */
	@Override
	@Transactional
	public ActivityRating getById(int id)
	{
		return getEntityManager().find(ActivityRating.class, id);
	}

	/*
	 * Get an average activityRating by Activity.
	 * 
	 * @param Activity
	 * 
	 * @return int rating (1-10)
	 */
	@Override
	@Transactional
	public int getAvgRatingByActivity(Activity activity)
	{
		logger.debug("Searching for activityRating for id" + activity.getActivityId());
		// int activityId = activity.getActivityId();

		Double result = getEntityManager()
				.createQuery(
						"SELECT AVG(r.rating) FROM ActivityRating r JOIN r.activityBooking ab "
								+ "JOIN ab.activityAvailability aa "
								+ "WHERE aa.activity = :activity", Double.class)
				.setParameter("activity", activity).getSingleResult();
		if (result == null)
			return 0;
		return (int) Math.ceil(result);
	}

	/*
	 * Get an activityRating list by Activity.
	 * 
	 * @param Activity
	 * 
	 * @return ActivityRating list
	 */
	@Override
	@Transactional
	public List<ActivityRating> getRatingsByActivity(Activity activity)
	{
		logger.debug("Searching for ActivityRating..");

		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT r FROM ActivityRating r JOIN r.activityBooking ab JOIN ab.activityAvailability aa "
									+ "WHERE aa.activity = :activity ORDER BY r DESC",
							ActivityRating.class).setParameter("activity", activity)
					.getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * Get top10 for ActivityCategory
	 * 
	 * @param ActivityCategory
	 * 
	 * @return rating list
	 */
	@Override
	@Transactional
	public List<String> getTop10ByCategory(ActivityCategory activityCategory)
	{
		String activityCategoryName = activityCategory.getCategory();
		logger.debug("Searching for ActivityRating for category " + activityCategoryName);

		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT CONCAT(round(AVG(r.rating)),' ', a.name) AS q FROM Activity a, ActivityRating r "
									+ "JOIN r.activityBooking ab JOIN ab.activityAvailability aa "
									+ "JOIN aa.activity a WHERE a.activityCategory = :activityCategory "
									+ "GROUP BY a ORDER BY q DESC LIMIT 0, 10", String.class)
					.setParameter("activityCategory", activityCategory).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}
}
