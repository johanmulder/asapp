/**
 * 
 * File created at 27 okt. 2013 11:38:56 by Vincent Vogelesang
 */
package asapp.data.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ParticipantGroupDao;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ParticipantGroupDaoImpl extends DaoBase implements ParticipantGroupDao
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(ParticipantGroupDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ParticipantGroupDao#create(asapp.models.ParticipantGroup)
	 */
	@Override
	@Transactional
	public void create(ParticipantGroup participantGroup)
	{
		logger.debug("Creating participantGroup with id " + participantGroup.getParticipantGroupId());
		getEntityManager().persist(participantGroup);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ParticipantGroupDao#update(asapp.models.ParticipantGroup)
	 */
	@Override
	@Transactional
	public void update(ParticipantGroup participantGroup)
	{
		getEntityManager().persist(participantGroup);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.ParticipantGroupDao#delete(asapp.models.ParticipantGroup)
	 */
	@Override
	@Transactional
	public void delete(ParticipantGroup participantGroup)
	{
		getEntityManager().remove(participantGroup);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupDao#getById(int)
	 */
	@Override
	@Transactional
	public ParticipantGroup getById(int id)
	{
		return getEntityManager().find(ParticipantGroup.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ParticipantGroupDao#getByStatus(asapp.models.types.
	 * ParticipantGroupStatus)
	 */
	@Override
	public List<ParticipantGroup> getByStatus(ParticipantGroupStatus status)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT pg from ParticipantGroup pg where status = :status",
							ParticipantGroup.class).setParameter("status", status).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}
}
