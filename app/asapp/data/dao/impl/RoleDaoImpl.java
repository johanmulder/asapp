/**
 * 
 * File created at 22 okt. 2013 23:18:14 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import javax.persistence.NoResultException;

import play.db.jpa.Transactional;
import asapp.data.dao.RoleDao;
import asapp.models.types.Role;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class RoleDaoImpl extends DaoBase implements RoleDao
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.RoleDao#getByName(java.lang.String)
	 */
	@Override
	@Transactional
	public Role getByName(String name)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT r FROM Role r WHERE name = :name", Role.class)
					.setParameter("name", name).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

}
