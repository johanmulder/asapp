/**
 * 
 * File created at 23 okt. 2013 21:06:35 by Vincent Vogelesang
 */
package asapp.data.dao.impl;

import java.util.List;
import java.util.Date;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.ActivityDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityCategory;
import asapp.models.types.Supplier;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ActivityDaoImpl extends DaoBase implements ActivityDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(ActivityDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#create(asapp.models.Activity)
	 */
	@Override
	@Transactional
	public void create(Activity activity)
	{
		getEntityManager().persist(activity);
		logger.debug("Created activity with id " + activity.getActivityId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#update(asapp.models.Activity)
	 */
	@Override
	@Transactional
	public void update(Activity activity)
	{
		getEntityManager().persist(activity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#delete(asapp.models.Activity)
	 */
	@Override
	@Transactional
	public void delete(Activity activity)
	{
		getEntityManager().remove(activity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getById(int)
	 */
	@Override
	@Transactional
	public Activity getById(int id)
	{
		return getEntityManager().find(Activity.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getById(int)
	 */
	@Override
	@Transactional
	public List<Activity> getBySupplier(Supplier supplier)
	{
		logger.debug("Searching for activity with Supplierid " + supplier.getSupplierId());

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from Activity p where supplier = :supplier",
							Activity.class).setParameter("supplier", supplier).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getBySupplierId(int)
	 */
	@Override
	@Transactional
	public List<Activity> getBySupplierId(int id)
	{
		logger.debug("Searching for activity with Supplierid " + id);

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from Activity p where supplier = :id", Activity.class)
					.setParameter("id", id).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * Get a booking count for Activity by given period
	 * 
	 * @param activity object
	 * 
	 * @param date start
	 * 
	 * @param date end
	 * 
	 * @return integer booking count
	 */
	@Override
	@Transactional
	public int getBookingCountByPeriod(Activity activity, Date startDate, Date endDate)
	{
		// logger.debug("Searching for activity in given period");

		try
		{
			Long bookingCount = (Long) getEntityManager()
					.createQuery(
							"SELECT COUNT(*) FROM ActivityBooking ab "
									+ "JOIN ab.activityAvailability aa JOIN aa.activity a JOIN a.supplier s "
									+ "WHERE a = :activity AND ab.datetime >= :startDate AND ab.datetime <= :endDate")
					.setParameter("activity", activity).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).getSingleResult();
			return bookingCount.intValue();
		}
		catch (javax.persistence.NoResultException e)
		{
			return 0;
		}

	}

	/*
	 * Get a tarrif total for Activity by given period
	 * 
	 * @param activity object
	 * 
	 * @param date start
	 * 
	 * @param date end
	 * 
	 * @return integer tariff total
	 */
	@Override
	@Transactional
	public int getTariffTotalByPeriod(Activity activity, Date startDate, Date endDate)
	{
		// logger.debug("Searching for activity tarrifs in given period");
		// int activityId = activity.getActivityId();

		try
		{
			BigDecimal tariffTotal = (BigDecimal) getEntityManager()
					.createQuery(
							"SELECT SUM(a.baseTariffParticipant) FROM ActivityBooking ab "
									+ "JOIN ab.activityAvailability aa JOIN aa.activity a JOIN a.supplier s "
									+ "WHERE a = :activity AND ab.datetime >= :startDate AND ab.datetime <= :endDate")
					.setParameter("activity", activity).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).getSingleResult();
			if (tariffTotal == null)
				tariffTotal = BigDecimal.ZERO;
			return tariffTotal.intValue();
		}
		catch (javax.persistence.NoResultException e)
		{
			return 0;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getByActivityCategory(asapp.models.types.
	 * ActivityCategory)
	 */
	@Override
	@Transactional
	public List<Activity> getByCategory(ActivityCategory activityCategory)
	{
		logger.debug("Searching for activity with category " + activityCategory);

		try
		{
			return getEntityManager()
					.createQuery(
							"SELECT p from Activity p where activityCategory like :activityCategory order by activityCategory",
							Activity.class).setParameter("activityCategory", activityCategory)
					.getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.ActivityDao#getByCategory(java.lang.String)
	 */
	@Override
	public List<Activity> getByCategory(String category)
	{
		logger.debug("Searching for activity with category " + category);

		try
		{
			String query = "SELECT a from Activity a JOIN a.activityCategory c "
					+ "WHERE c.category LIKE :category ORDER BY c.category";
			return getEntityManager().createQuery(query, Activity.class)
					.setParameter("category", category).getResultList();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}
	}
}
