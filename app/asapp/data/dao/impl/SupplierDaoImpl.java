/**
 * 
 * File created at 20 okt. 2013 22:43:28 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import javax.persistence.NoResultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.db.jpa.Transactional;
import asapp.data.dao.SupplierDao;
import asapp.models.types.Supplier;
import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class SupplierDaoImpl extends DaoBase implements SupplierDao
{
	// Logging class.
	private static Logger logger = LoggerFactory.getLogger(SupplierDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#create(asapp.models.Supplier)
	 */
	@Override
	@Transactional
	public void create(Supplier supplier)
	{
		getEntityManager().persist(supplier);
		logger.debug("Created supplier with id " + supplier.getSupplierId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#update(asapp.models.Supplier)
	 */
	@Override
	@Transactional
	public void update(Supplier supplier)
	{
		getEntityManager().persist(supplier);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#delete(asapp.models.Supplier)
	 */
	@Override
	@Transactional
	public void delete(Supplier supplier)
	{
		getEntityManager().remove(supplier);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#getById(int)
	 */
	@Override
	@Transactional
	public Supplier getById(int id)
	{
		return getEntityManager().find(Supplier.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#getByUser(user)
	 */
	@Override
	@Transactional
	public Supplier getByUser(User user)
	{
		try
		{
			return getEntityManager()
					.createQuery("SELECT u from Supplier u where user = :user", Supplier.class)
					.setParameter("user", user).getSingleResult();
		}
		catch (NoResultException e)
		{
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.SupplierDao#getByEmail(java.lang.String)
	 */
	@Override
	@Transactional
	public Supplier getByEmail(String email)
	{
		logger.debug("Searching for supplier with email address " + email);

		try
		{
			return getEntityManager()
					.createQuery("SELECT p from Supplier p where email = :email", Supplier.class)
					.setParameter("email", email).getSingleResult();
		}
		catch (javax.persistence.NoResultException e)
		{
			return null;
		}

	}
}
