/**
 * 
 * File created at 10 nov. 2013 00:04:40 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao.impl;

import java.math.BigDecimal;

import play.db.jpa.Transactional;
import asapp.data.dao.FacebookDataDao;
import asapp.models.types.FacebookData;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FacebookAuthTokenDaoImpl extends DaoBase implements FacebookDataDao
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.FacebookAuthTokenDao#create(asapp.models.types.
	 * FacebookAuthToken)
	 */
	@Override
	@Transactional
	public void create(FacebookData token)
	{
		getEntityManager().persist(token);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.FacebookAuthTokenDao#update(asapp.models.types.
	 * FacebookAuthToken)
	 */
	@Override
	@Transactional
	public void update(FacebookData token)
	{
		getEntityManager().persist(token);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see asapp.data.dao.FacebookAuthTokenDao#delete(asapp.models.types.
	 * FacebookAuthToken)
	 */
	@Override
	@Transactional
	public void delete(FacebookData token)
	{
		getEntityManager().remove(token);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * asapp.data.dao.FacebookAuthTokenDao#getByUserId(java.math.BigDecimal)
	 */
	@Override
	@Transactional
	public FacebookData getByUserId(BigDecimal uid)
	{
		return getEntityManager().find(FacebookData.class, uid);
	}

}
