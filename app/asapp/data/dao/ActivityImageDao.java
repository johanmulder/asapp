/**
 * 
 * File created at 25 okt. 2013 12:46:22 by Vincent Vogelesang
 */
package asapp.data.dao;

import java.util.List;

import asapp.models.types.ActivityImage;
import asapp.models.types.Activity;

/**
 * 
 * @author Vincent Vogelesang
 */
public interface ActivityImageDao
{
	/**
	 * Create a new activityImage entry.
	 * 
	 * @param activityImage
	 */
	public void create(ActivityImage activityImage);

	/**
	 * Update an existing activityImage entry.
	 * 
	 * @param activityImage
	 */
	public void update(ActivityImage activityImage);

	/**
	 * Delete an activityImage entry.
	 * 
	 * @param activityImage
	 */
	public void delete(ActivityImage activityImage);

	/**
	 * Get an activityImage entry by id.
	 * 
	 * @param id
	 * @return
	 */
	public ActivityImage getById(int id);

	/**
	 * Get a list of activityImages by ActivityId.
	 * 
	 * @param Id
	 * @return
	 */
	public List<ActivityImage> getByActivity(Activity activity);
}
