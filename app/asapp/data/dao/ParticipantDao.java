/**
 * 
 * File created at 20 okt. 2013 20:36:56 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import asapp.models.types.Participant;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface ParticipantDao
{
	/**
	 * Create a new participant.
	 * 
	 * @param participant
	 */
	public void create(Participant participant);

	/**
	 * Update an existing participant.
	 * 
	 * @param participant
	 */
	public void update(Participant participant);

	/**
	 * Delete a given participant.
	 * 
	 * @param participant
	 */
	public void delete(Participant participant);

	/**
	 * Get a participant by id.
	 * 
	 * @param id
	 * @return
	 */
	public Participant getById(int id);

	/**
	 * Get a participant by email. As the email is a unique key, only one result
	 * can be returned, or null if not found.
	 * 
	 * @param email
	 * @return
	 */
	public Participant getByEmail(String email);
}
