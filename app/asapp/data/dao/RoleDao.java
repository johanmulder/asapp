/**
 * 
 * File created at 22 okt. 2013 23:17:33 by Johan Mulder <johan@mulder.net>
 */
package asapp.data.dao;

import asapp.models.types.Role;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface RoleDao
{
	/**
	 * Get a role by name.
	 * 
	 * @param name
	 * @return
	 */
	public Role getByName(String name);
}
