package asapp.supplierportal.controllers;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.MultipartFormData.FilePart;
import play.cache.Cache;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.controllers.SecuredSupplier;
import asapp.models.ActivityModel;
import asapp.models.SupplierModel;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityImage;
import asapp.models.types.Supplier;
import asapp.supplierportal.views.html.registeractivity;
import asapp.supplierportal.views.html.registeredactivity;

/**
 * Controller handling the registration of activities.
 * 
 * @author Johan Mulder <johan@mulder.net>
 * @author Chakir Bouziane
 */
@Security.Authenticated(SecuredSupplier.class)
public class RegisterActivity extends Controller
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(RegisterActivity.class);
	// Form handling the activity input.
	private Form<Activity> activityForm = Form.form(Activity.class);
	@Inject
	private ActivityModel activityModel;
	@Inject
	private SupplierModel supplierModel;

	/**
	 * Get all possible categories.
	 * 
	 * @return A list of categories.
	 */
	@Transactional
	private List<ActivityCategory> getCategories()
	{
		List<ActivityCategory> categories = activityModel.getActivityCategories();
		return categories;
	}

	/**
	 * Method handling the main registration page for suppliers.
	 * 
	 * @return
	 */
	@Transactional
	public Result register()
	{
		List<ActivityImage> imageList = new ArrayList<ActivityImage>();
		List<String> dateList = new ArrayList<String>();
		return ok(registeractivity.render(activityForm, getCategories(), imageList, dateList));
	}

	/**
	 * Method handling the registration.
	 * 
	 * @return
	 */
	@Transactional
	public Result doRegister()
	{

		Form<Activity> form = activityForm.bindFromRequest();
		try
		{
			if (form.hasErrors())
			{
				logger.debug("Form activityRegistry has errors: " + form.errorsAsJson());
				Map<String, String> formMap = form.data();
				List<Date> dates = getDatesFromMap(formMap);
				List<String> dateList = new ArrayList<String>();
				if (!dates.isEmpty())
				{
					dateList = turnDatesIntoStrings(dates);
				}
				;
				return badRequest(registeractivity.render(form, getCategories(),
						new ArrayList<ActivityImage>(), dateList));
			}
			else
			{
				int supplierUserId = Integer.parseInt(session().get("userid"));
				Map<String, String> formMap = form.data();
				List<Date> dates = getDatesFromMap(formMap);
				Activity activity = form.get();
				Supplier supplier = supplierModel.getSupplierByUserId(supplierUserId);
				ActivityCategory activityCategory = new ActivityCategory();
				activityCategory.setCategory(formMap.get("activity_category"));
				activity.setActivityCategory(activityCategory);
				activity.setSupplier(supplier);
				List<ActivityAvailability> availability = getListActivityAvailability(dates,
						formMap, activity);
				HashMap<ActivityImage, File> imageFiles = getImagesFromCache();
				// Create the activity
				activityModel.createActivity(activity, imageFiles, availability);
				// Remove cached entries.
				Cache.remove(getCacheKey("image_files"));
				Cache.remove(getCacheKey("files"));
				// Redirect.
				return redirect(routes.RegisterActivity.registered());
			}
		}

		catch (IOException | NoSuchAlgorithmException | ParseException e)
		{
			logger.error("Exception caught", e);
			return badRequest(registeractivity.render(form, getCategories(),
					new ArrayList<ActivityImage>(), new ArrayList<String>()));
		}

	}

	/**
	 * 
	 * Makes a list of ActivityAvailability object out of the Date list and the
	 * formMap
	 * 
	 * @param dates list of available dates
	 * @param formMap Map with the data filled in the form
	 * @return list with the ActivityAvailability objects
	 */
	private List<ActivityAvailability> getListActivityAvailability(List<Date> dates,
			Map<String, String> formMap, Activity activity)
	{
		List<ActivityAvailability> actAvList = new ArrayList<>();
		for (int i = 0; i < dates.size(); i++)
		{
			ActivityAvailability activityAvailability = new ActivityAvailability();
			activityAvailability.setDateTime(dates.get(i));
			activityAvailability.setActivity(activity);
			activityAvailability
					.setMaxParticipants(Integer.parseInt(formMap.get("maxParticipant")));
			activityAvailability
					.setMinParticipants(Integer.parseInt(formMap.get("minParticipant")));
			actAvList.add(activityAvailability);

		}
		return actAvList;
	}

	/**
	 * Get a cache key for the given key name.
	 * 
	 * @return
	 */
	private String getCacheKey(String keyName)
	{
		String imageCacheKey = session(keyName);
		if (imageCacheKey == null)
		{
			imageCacheKey = UUID.randomUUID().toString();
			session(keyName, imageCacheKey);
		}
		return imageCacheKey;
	}

	/**
	 * Create a list of Dates from the form data
	 * 
	 * @param form map
	 * @return List with Date objects
	 */
	private List<Date> getDatesFromMap(Map<String, String> formMap) throws ParseException
	{
		// Get all map entries starting with dates
		List<Date> dateList = new ArrayList<>();
		for (String key : formMap.keySet())
		{
			if (key.contains("datetime"))
			{
				Date date = dateFromString(formMap.get(key));
				dateList.add(date);
			}
		}
		return dateList;
	}

	/**
	 * Changes the string into a Date
	 * 
	 * @param Datestring
	 * @return Date
	 */
	public static Date dateFromString(String dateString) throws ParseException
	{

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		Date date = formatter.parse(dateString);
		return date;

	}

	/**
	 * When a new activity has been created by doRegister(), a redirect will be
	 * sent to this method.
	 * 
	 * @return
	 */
	public Result registered()
	{
		// Using registered.scala.html instead of separate registered file
		return ok(registeredactivity.render());
	}

	/**
	 * Internal class used for a JSON status value used by fineuploader.
	 * 
	 * @author Johan Mulder <johan@mulder.net>
	 */
	private static class UploadStatus
	{
		@SuppressWarnings("unused")
		public boolean success;
		public String error;
	}

	/**
	 * Method handling the uploading of an image.
	 * 
	 * @return
	 */
	public Result uploadImage()
	{
		Http.RequestBody body = request().body();
		UploadStatus uploadStatus = new UploadStatus();
		if (body != null)
		{
			int count = 0;
			Map<String, String[]> testMap = body.asMultipartFormData().asFormUrlEncoded();
			String qquuid = null;
			for (String key : testMap.keySet())
			{
				if (key.equals("qquuid"))
				{
					String[] keyValues = testMap.get(key);
					if (keyValues.length >= 1)
						qquuid = keyValues[0];
				}
			}

			for (FilePart file : body.asMultipartFormData().getFiles())
			{
				if (checkInput(file.getContentType()))
				{
					HashMap<ActivityImage, File> imageFiles = getImagesFromCache();
					HashMap<String, File> files = getFilesFromCache();
					try
					{
						files.put(qquuid, file.getFile());
						Cache.set(getCacheKey("files"), files);
						ActivityImage activityImage = new ActivityImage();
						activityImage.setContentType(file.getContentType());
						imageFiles.put(activityImage, file.getFile());
						Cache.set(getCacheKey("image_files"), imageFiles);
						logger.debug("Got file with name " + file.getFilename() + "content type"
								+ file.getContentType() + " " + file.getFile().getCanonicalPath());
					}
					catch (IOException e)
					{
						logger.debug("Caught exception ", e);
					}
					count++;
				}
				else
				{
					uploadStatus.success = false;
					uploadStatus.error = "Not an acceptable contenttype";
					return badRequest(Json.toJson(uploadStatus));
				}
			}
			logger.debug("Got " + count + " files");
		}
		else
		{
			uploadStatus.success = false;
			uploadStatus.error = "Body was empty";
			return badRequest(Json.toJson(uploadStatus));
		}
		uploadStatus.success = true;
		return ok(Json.toJson(uploadStatus));
	}

	/**
	 * Method handling the deletion of an image.
	 * 
	 * @return
	 */
	public Result deleteImage()
	{

		Http.RequestBody body = request().body();
		UploadStatus deleteStatus = new UploadStatus();

		if (body != null)
		{

			Map<String, String[]> testMap = body.asFormUrlEncoded();
			String qquuid = null;
			for (String key : testMap.keySet())
			{
				if (key.equals("qquuid"))
				{
					String[] keyValues = testMap.get(key);
					if (keyValues.length >= 1)
						qquuid = keyValues[0];
				}
			}
			logger.debug(qquuid);
			int count = 0;
			HashMap<String, File> files = getFilesFromCache();
			File file = files.get(qquuid);
			HashMap<ActivityImage, File> imageFiles = getImagesFromCache();
			ActivityImage removalKey = null;
			try
			{
				for (Entry<ActivityImage, File> entry : imageFiles.entrySet())
				{
					if (file.equals(entry.getValue()))
					{
						removalKey = entry.getKey();
						file.delete();
					}
				}

				if (removalKey != null)
				{
					imageFiles.remove(removalKey);
				}
				Cache.set(getCacheKey("image_files"), imageFiles);
				logger.debug("Deleted file with name " + file.getName() + " "
						+ file.getCanonicalPath());
			}
			catch (IOException e)
			{
				logger.debug("Caught exception ", e);
			}
			count++;
			logger.debug("Deleted " + count + " files");
		}
		else
		{
			deleteStatus.success = false;
			deleteStatus.error = "Body was empty";
			logger.debug(deleteStatus.error);
			return badRequest(Json.toJson(deleteStatus));
		}
		deleteStatus.success = true;
		return ok(Json.toJson(deleteStatus));
	}

	/**
	 * 
	 * Checking if the input is allowed contenttype
	 * 
	 * @param contentType
	 * @return boolean true = acceptable, false= not acceptable
	 */
	private boolean checkInput(String contentType)
	{
		List<String> contentTypes = Arrays.asList("image/gif", "image/jpeg", "image/pjpeg",
				"image/png", "image/svg+xml", "image/tiff");
		if (contentTypes.contains(contentType))
		{
			return true;
		}
		return false;
	}

	/**
	 * Method handling the retrieval of the images hashmap from the cache.
	 * 
	 * @return HashMap<ActivityImage, File>
	 */
	@SuppressWarnings("unchecked")
	private HashMap<ActivityImage, File> getImagesFromCache()
	{
		HashMap<ActivityImage, File> imageFiles;
		if (Cache.get(getCacheKey("image_files")) == null)
		{
			imageFiles = new HashMap<ActivityImage, File>();
		}
		else
		{
			imageFiles = (HashMap<ActivityImage, File>) Cache.get(getCacheKey("image_files"));
			Cache.remove(getCacheKey("image_files"));
		}
		return imageFiles;
	}

	/**
	 * Method handling the retrieval of the files hashmap from the cache.
	 * 
	 * @return HashMap<ActivityImage, File>
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, File> getFilesFromCache()
	{
		HashMap<String, File> files;
		if (Cache.get(getCacheKey("image_files")) == null)
		{
			files = new HashMap<String, File>();
		}
		else
		{
			files = (HashMap<String, File>) Cache.get(getCacheKey("files"));
			Cache.remove(getCacheKey("files"));
		}
		return files;
	}

	/**
	 * Create a list of strings from the list of dates
	 * 
	 * @param datesList List with Date objects
	 * @return List with String objects
	 */

	private List<String> turnDatesIntoStrings(List<Date> datesList)
	{
		// Get all map entries starting with dates
		List<String> dateList = new ArrayList<>();
		for (Date date : datesList)
		{

			{
				try
				{
					String dateString = stringFromDate(date);
					dateList.add(dateString);
				}
				catch (ParseException e)
				{
					logger.error("Exception caught", e);
				}

			}
		}
		return dateList;
	}

	/**
	 * Changes the Date object into a string
	 * 
	 * @param Datestring
	 * @param Format of datestring
	 * @return Date
	 */
	public static String stringFromDate(Date date) throws ParseException
	{

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		String dateString = formatter.format(date);
		return dateString;

	}
}
