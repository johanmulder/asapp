package asapp.supplierportal.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.controllers.SecuredSupplier;
import asapp.models.ActivityImageModel;
import asapp.models.ActivityModel;
import asapp.models.SupplierModel;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityImage;
import asapp.models.types.Supplier;
import asapp.supplierportal.views.html.mutateactivity;
import asapp.supplierportal.views.html.mutateselectedactivity;

/**
 * Controller handling the mutations of activities.
 * 
 * @author JH
 * @author Chakir Bouziane
 */
@Security.Authenticated(SecuredSupplier.class)
public class MutateActivity extends Controller
{
	@Inject
	private ActivityModel activityModel;
	@Inject
	private ActivityImageModel activityImageModel;
	@Inject
	private SupplierModel supplierModel;
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(MutateActivity.class);

	/**
	 * Method handling the main mutation page for activities.
	 * 
	 * @return
	 */
	@Transactional
	public Result mutate()
	{
		List<Activity> activityList = getActivityList();
		return ok(mutateactivity.render(activityList));
	}

	/**
	 * 
	 * Method handling the deletion of activities.
	 * 
	 * @param activityId the Id of the activity to be deleted
	 * @return
	 */
	@Transactional
	public Result deleteActivity(int activityId)
	{
		if (checkAccess(activityId))
		{
			logger.debug("Deleting activity with ID" + activityId);
			activityModel.deleteActivity(activityId);
		}
		else
		{
			return unauthorized("Gebruiker heeft geen toegang tot deze activiteit");
		}
		return mutate();

	}

	/**
	 * Method returning the activity for change.
	 * 
	 * Method handling the change of activities.
	 */
	@Transactional
	public Result mutateActivity(int activityId)
	{
		if (checkAccess(activityId))
		{
			List<ActivityCategory> categories = activityModel.getActivityCategories();
			logger.debug("Return activity for change with ID" + activityId);
			Activity activity = activityModel.getByID(activityId);
			Form<Activity> activityForm = Form.form(Activity.class);
			Form<Activity> filledForm = activityForm.fill(activity);
			List<ActivityImage> imageList = activityImageModel.getActivityImageByActivity(activity);
			List<ActivityAvailability> activityAvailabilityList = activityModel
					.getActivityAvailabilities(activity);
			List<String> dateList = putDatesInList(activityAvailabilityList);
			return ok(mutateselectedactivity.render(filledForm, categories, imageList, dateList));
		}

		return unauthorized("Gebruiker heeft geen toegang tot deze activiteit");

	}

	/**
	 * 
	 * Checking if the user is allowed to change or delete the activity
	 * 
	 * @param activityId the ID of the activity
	 * @return boolean true = allowed, false= not allowed
	 */
	private boolean checkAccess(int activityId)
	{
		boolean access = false;

		int supplierUserId = Integer.parseInt(session().get("userid"));
		Supplier supplier = supplierModel.getSupplierByUserId(supplierUserId);
		Activity activity = activityModel.getByID(activityId);
		if (activity.getSupplier().equals(supplier))
		{
			access = true;
		}

		return access;
	}

	/**
	 * To get the list of activities of the current user
	 * 
	 * @return List <Activity> the list of activities of the current user
	 */
	private List<Activity> getActivityList()
	{
		int supplierUserId = Integer.parseInt(session().get("userid"));
		Supplier supplier = supplierModel.getSupplierByUserId(supplierUserId);
		List<Activity> activityList = activityModel.getBySupplier(supplier);
		return activityList;
	}

	/**
	 * Method handling the deletion of images.
	 * 
	 * @return
	 */
	@Transactional
	public Result deleteActivityImage(int activityImageId)
	{
		ActivityImage activityImage = activityImageModel.getById(activityImageId);
		int activityId = activityImage.getActivity().getActivityId();
		if (checkAccess(activityId))
		{
			logger.debug("Deleting activity with ID" + activityId);
			activityImageModel.deleteActivityImage(activityImage);
		}
		else
		{
			return unauthorized("Gebruiker heeft geen toegang tot deze activiteit");
		}
		return mutateActivity(activityId);

	}

	/**
	 * 
	 * Create a list of String from the form dates
	 * 
	 * @param availabilityList
	 * @return List of date Strings
	 */
	private List<String> putDatesInList(List<ActivityAvailability> availabilityList)
	{
		// Get all map entries starting with dates
		List<String> dateList = new ArrayList<>();
		for (ActivityAvailability availability : availabilityList)
		{

			Date date = availability.getDateTime();
			{
				try
				{
					String dateString = stringFromDate(date);
					dateList.add(dateString);
				}
				catch (ParseException e)
				{
					logger.error("Exception caught", e);
				}

			}
		}
		return dateList;
	}

	/**
	 * Changes the Date into a string
	 * 
	 * @param Date
	 * @return String with the date in the right format
	 */
	public static String stringFromDate(Date date) throws ParseException
	{

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		String dateString = formatter.format(date);
		return dateString;

	}

}
