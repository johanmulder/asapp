package asapp.supplierportal.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.joda.time.DateTime;

import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import asapp.models.ActivityModel;
import asapp.models.ActivityRatingModel;
import asapp.models.SupplierModel;
import asapp.models.types.Activity;
import asapp.models.types.Supplier;
import asapp.supplierportal.views.html.reports;
import asapp.frontend.controllers.SecuredSupplier;

/**
 * Controller handling the reporting of activities
 * 
 * @author Vincent Vogelesang
 */
@Security.Authenticated(SecuredSupplier.class)
public class Reports extends Controller
{
	// The form handling the registration.
	private static Form<Supplier> reportForm = Form.form(Supplier.class);
	@Inject
	private ActivityModel activityModel;
	@Inject
	private ActivityRatingModel activityRatingModel;
	@Inject
	private SupplierModel supplierModel;

	/**
	 * Method handling the main report page for suppliers.
	 * 
	 * @return
	 */
	public Result showReport()
	{
		List<Activity> activityList = new ArrayList<>();
		List<List<Integer>> allPeriodsList = new ArrayList<>();
		List<Integer> tariffTotalList = new ArrayList<>();
		List<Integer> ownGradeList = new ArrayList<>();
		List<String> allCategoryGradeList = new ArrayList<>();
		List<List<String>> allRemarkList = new ArrayList<>();

		return ok(reports.render(reportForm, activityList, allPeriodsList, tariffTotalList,
				ownGradeList, allCategoryGradeList, allRemarkList));
	}

	/**
	 * Method handling thereport result page for suppliers.
	 * 
	 * @return
	 */
	@Transactional
	public Result doShowReport()
	{
		String email = session().get("email");
		Supplier supplier = supplierModel.getByEmail(email);

		// Get the form parameters
		Form<Supplier> form = reportForm.bindFromRequest();
		Map<String, String> formMap = form.data();

		int periods = Integer.parseInt(formMap.get("periods"));
		int periodLength = Integer.parseInt(formMap.get("periodlength"));

		// Create all lists for template
		List<Activity> activityList = activityModel.getBySupplier(supplier);
		List<String> allCategoryGradeList = activityRatingModel.getTop10ByCategory(activityList
				.get(0).getActivityCategory());

		List<List<Integer>> allPeriodsList = new ArrayList<>();
		List<Integer> tariffTotalList = new ArrayList<>();
		List<Integer> ownGradeList = new ArrayList<>();
		List<List<String>> allRemarkList = new ArrayList<>();

		// Iterate activityList and populate lists
		for (Activity activity : activityList)
		{
			List<Integer> periodList = new ArrayList<Integer>();
			int tariffTotal = 0;
			int bookingTotal = 0;
			for (int i = 0; i < periods; i++)
			{
				Date startDate = new DateTime().minusDays((periods - i) * periodLength - 1)
						.toDate();
				Date endDate = new DateTime().minusDays((periods - i - 1) * periodLength).toDate();
				int bookingCount = activityModel.getBookingCountByPeriod(activity, startDate,
						endDate);
				tariffTotal = tariffTotal
						+ activityModel.getTariffTotalByPeriod(activity, startDate, endDate);
				periodList.add(bookingCount);
				bookingTotal += bookingCount;
			}
			periodList.add(bookingTotal);
			allPeriodsList.add(periodList);
			tariffTotalList.add(tariffTotal);
			ownGradeList.add(activityRatingModel.getAvgRatingByActivity(activity));
			allRemarkList.add(activityRatingModel.getRemarksByActivity(activity));
		}

		return ok(reports.render(form, activityList, allPeriodsList, tariffTotalList, ownGradeList,
				allCategoryGradeList, allRemarkList));
	}
}
