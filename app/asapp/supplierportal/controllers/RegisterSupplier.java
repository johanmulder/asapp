package asapp.supplierportal.controllers;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.data.validation.ValidationError;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.models.SupplierModel;
import asapp.models.exceptions.SupplierAlreadyExistsException;
import asapp.models.exceptions.UserAlreadyExistsException;
import asapp.models.types.Supplier;
import asapp.services.SupplierMailer;
import asapp.supplierportal.views.html.registered;
import asapp.supplierportal.views.html.registersupplier;
import asapp.util.Password;

/**
 * Controller handling the registration of activity suppliers.
 * 
 * @author Johan Mulder <johan@mulder.net>
 * @author Vincent Vogelesang
 */
public class RegisterSupplier extends Controller
{
	// Logging object.
	private static Logger logger = LoggerFactory.getLogger(RegisterSupplier.class);
	// The form handling the registration.
	private static Form<Supplier> registerSupplierForm = Form.form(Supplier.class);
	// Automatically injected supplierDao.
	@Inject
	private SupplierModel supplierModel;
	@Inject
	private SupplierMailer supplierMailer;

	/**
	 * Method handling the main registration page for suppliers.
	 * 
	 * @return
	 */
	public Result register()
	{
		return ok(registersupplier.render(registerSupplierForm));
	}

	/**
	 * Method handling the posted data for suppliers. This method validates the
	 * form and will create the new supplier. When created, a confirmation link
	 * will be sent to the supplier.
	 * 
	 * @return
	 */
	@Transactional
	public Result doRegister()
	{
		// Bind the form to the http request.
		Form<Supplier> supplierForm = registerSupplierForm.bindFromRequest();

		// Implement password validation.
		String password = supplierForm.data().get("password");

		// Check if the given passwords match.
		if (Password.isPasswordMismatch(supplierForm.data().get("passwordValidate"), password))
			supplierForm.reject(new ValidationError("passwordValidate", "error.password.mismatch"));

		if (supplierForm.hasErrors())
		{
			logger.debug("Form has errors: " + supplierForm.errorsAsJson());
			return badRequest(registersupplier.render(supplierForm));
		}
		else
		{
			try
			{
				Supplier supplier = supplierForm.get();
				supplierModel.create(supplier, password);
				// Mailing of confirmation.
				supplierMailer.mailSupplierRegistrationConfirmation(supplier);
				return redirect(routes.RegisterSupplier.registered());
			}
			catch (SupplierAlreadyExistsException | UserAlreadyExistsException
					| UnsupportedEncodingException | MessagingException e)
			{
				logger.error("Exception caught", e);
				return badRequest(registersupplier.render(supplierForm));
			}
		}
	}

	/**
	 * When a new supplier has been created by doRegister(), a redirect will be
	 * sent to this method.
	 * 
	 * @return
	 */
	public Result registered()
	{
		// Using registered.scala.html instead of separate registered file
		return ok(registered.render());
	}
}
