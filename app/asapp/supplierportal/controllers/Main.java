/**
 * 
 * File created at 23 okt. 2013 19:54:56 by Johan Mulder <johan@mulder.net>
 */
package asapp.supplierportal.controllers;

import play.mvc.Controller;
import play.mvc.Result;
import asapp.supplierportal.views.html.index;

/**
 * Main controller for the supplier portal.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class Main extends Controller
{
	public Result index()
	{
		return ok(index.render());
	}
}
