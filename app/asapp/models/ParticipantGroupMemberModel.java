/**
 * 
 * File created at 27 okt. 2013 11:36:56 by Vincent Vogelesang
 */
package asapp.models;

import javax.inject.Inject;

import java.util.List;

import asapp.models.types.Participant;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupMember;
import asapp.data.dao.ParticipantGroupMemberDao;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ParticipantGroupMemberModel
{
	@Inject
	private ParticipantGroupMemberDao participantGroupMemberDao;

	/**
	 * Create a new participantGroupMember.
	 * 
	 * @param participantGroupMember
	 */
	public void create(ParticipantGroupMember participantGroupMember)
	{
		participantGroupMemberDao.create(participantGroupMember);
	}
	
	/**
	 * Update a participantGroupMember.
	 * 
	 * @param participantGroupMember
	 */
	public void update(ParticipantGroupMember participantGroupMember)
	{
		participantGroupMemberDao.update(participantGroupMember);
	}
	
	/**
	 * Deleta a participantGroupMember by ParticipantGroup.
	 * 
	 * @param participantGroup
	 */
	public void deleteByParticipantGroup(ParticipantGroup participantGroup)
	{
		participantGroupMemberDao.deleteByParticipantGroup(participantGroup);
	}
	
	/**
	 * Get a participantGroupMember by Participant
	 * 
	 * @param Participant
	 * @return ParticipantGroupMember object
	 */	
	public ParticipantGroupMember getByParticipant(Participant participant)
	{
		return participantGroupMemberDao.getByParticipant(participant);
	}
	
	/**
	 * Get a participantGroupMember by ParticipantGroup
	 * 
	 * @param ParticipantGroup
	 * @return ParticipantGroupMember object
	 */	
	public ParticipantGroupMember getByParticipantGroup(ParticipantGroup participantGroup)
	{
		return participantGroupMemberDao.getByParticipantGroup(participantGroup);
	}
	
	/**
	 * Get all participantGroupMembers created by Participant
	 * 
	 * @param Participant
	 * @return List with ParticipantGroupMember objects
	 */	
	public List<ParticipantGroupMember> getBookedListByParticipant(Participant participant)
	{
		return participantGroupMemberDao.getBookedListByParticipant(participant);
	}
	
	/**
	 * Get the initiator of a ParticipantGroupMember by ParticipantGroup
	 * 
	 * @param participantGroup
	 * @return Participant
	 */
	public Participant getInitiatorByParticipantGroup(ParticipantGroup participantGroup)
	{
		return participantGroupMemberDao.getInitiatorByParticipantGroup(participantGroup);
	}
	
	/**
	 * Get the ParticipantGroupMember by hash
	 * 
	 * @param sha-1 40 digit hash String
	 * @return ParticipantGroupMember
	 */
	public ParticipantGroupMember getByHash(String hash)
	{
		return participantGroupMemberDao.getByHash(hash);
	}
	
	/**
	 * Set the ParticipantConfirmed by hash
	 * 
	 * @param sha-1 40 digit hash String
	 * @param int value 0 or 1
	 * 
	 */
	public void setParticipantConfirmedByHash(String hash, int participantConfirmed)
	{
		if (participantConfirmed < 2 && participantConfirmed >= 0 && hash.length() == 40)
		{
			participantGroupMemberDao.setParticipantConfirmedByHash(hash, participantConfirmed);
		}
	}	
}
