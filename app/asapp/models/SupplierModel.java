/**
 * 
 * File created at 23 okt. 2013 00:17:40 by Johan Mulder <johan@mulder.net>
 */
package asapp.models;

import javax.inject.Inject;

import asapp.data.dao.RoleDao;
import asapp.data.dao.SupplierDao;
import asapp.data.dao.UserDao;
import asapp.models.exceptions.SupplierAlreadyExistsException;
import asapp.models.exceptions.UserAlreadyExistsException;
import asapp.models.types.Supplier;
import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class SupplierModel
{
	@Inject
	private SupplierDao supplierDao;
	@Inject
	private RoleDao roleDao;
	@Inject
	private UserModel userModel;
	@Inject
	private UserDao userDao;

	public void create(Supplier supplier, String password) throws SupplierAlreadyExistsException,
			UserAlreadyExistsException
	{
		// Check if the supplier already exists.
		Supplier existingSupplier = supplierDao.getByEmail(supplier.getEmail());
		if (existingSupplier != null)
			throw new SupplierAlreadyExistsException(existingSupplier);

		// Create a new user object with the user role.
		User user = new User(supplier.getEmail(), password,
				roleDao.getByName(asapp.auth.Role.SUPPLIER.toString()));
		userModel.create(user);
		supplier.setUser(user);
		// Create the new supplier.
		supplierDao.create(supplier);
	}

	public Supplier getSupplierByUserId(int supplierUserId)
	{
		User user = userDao.getById(supplierUserId);
		Supplier supplier = supplierDao.getByUser(user);
		return supplier;
	}
	
	public Supplier getByEmail(String email)
	{
		return supplierDao.getByEmail(email);
	}

}
