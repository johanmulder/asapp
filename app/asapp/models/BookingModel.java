/**
 * 
 * File created at 29 okt. 2013 22:02:22 by Johan Mulder <johan@mulder.net>
 */
package asapp.models;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asapp.data.dao.ActivityBookingDao;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityBooking;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;

/**
 * Model handling booking actions.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class BookingModel
{
	// Logger.
	private static Logger logger = LoggerFactory.getLogger(BookingModel.class);
	// Dao objects.
	private ParticipantGroupModel participantGroupModel;
	private ActivityBookingDao activityBookingDao;

	/**
	 * Class constructor.
	 * 
	 * @param participantGroupModel
	 * @param activityBookingDao
	 */
	@Inject
	public BookingModel(ParticipantGroupModel participantGroupModel,
			ActivityBookingDao activityBookingDao)
	{
		this.participantGroupModel = participantGroupModel;
		this.activityBookingDao = activityBookingDao;
	}

	/**
	 * Book an availability for the given participant group.
	 * 
	 * @param participantGroup
	 * @param availability
	 * @return The newly created booking entry.
	 */
	public ActivityBooking book(ParticipantGroup participantGroup, ActivityAvailability availability)
	{
		// Create a new booking entry.
		ActivityBooking booking = new ActivityBooking();
		booking.setActivityAvailability(availability);
		booking.setDatetime(availability.getDateTime());
		booking.setParticipantGroup(participantGroup);
		activityBookingDao.create(booking);
		availability.addBooking(booking);
		logger.info("Created booking entry with id " + booking.getActivityBookingId());

		// Set the status of the participant group to booked.
		participantGroup.setStatus(ParticipantGroupStatus.BOOKED);
		participantGroupModel.update(participantGroup);

		return booking;
	}
		
	/**
	 * Delete an availability for the given participant group.
	 * 
	 * @param participantGroup
	 * @param availability
	 * @return The newly created booking entry.
	 */
	public void deleteBooking(ParticipantGroup participantGroup)
	{
		ActivityBooking booking = participantGroup.getActivityBooking();
		activityBookingDao.delete(booking);
	}

}
