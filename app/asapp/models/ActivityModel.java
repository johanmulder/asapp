/**
 * 
 * File created at 25 okt. 2013 16:46:00 by Chakir Bouziane
 */
package asapp.models;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Date;
import javax.inject.Inject;
import asapp.data.dao.ActivityAvailabilityDao;
import asapp.data.dao.ActivityCategoryDao;
import asapp.data.dao.ActivityDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityImage;
import asapp.models.types.Supplier;

/**
 * 
 * @author Chakir Bouziane
 * @author Vincent Vogelesang
 */
public class ActivityModel
{
	@Inject
	private ActivityDao activityDao;
	@Inject
	private ActivityCategoryDao activityCategoryDao;
	@Inject
	private ActivityAvailabilityDao activityAvailabilityDao;
	@Inject
	private ActivityImageModel activityImageModel;

	/**
	 * Returns a list of ActivityCategories
	 * 
	 * @return List with ActivityCategory objects
	 */
	public List<ActivityCategory> getActivityCategories()
	{
		List<ActivityCategory> categories = activityCategoryDao.getAll();
		return categories;
	}

	/**
	 * Returns a list of Activities for a given category
	 * 
	 * @param ActivityCategory
	 * @return List with Activity objects
	 */
	public List<Activity> getActivityByCategory(ActivityCategory category)
	{
		List<Activity> activities = activityDao.getByCategory(category);
		return activities;
	}

	/**
	 * Returns a list of Activities for a given category name.
	 * 
	 * @param category
	 * @return
	 */
	public List<Activity> getActivityByCategory(String category)
	{
		List<Activity> activities = activityDao.getByCategory(category);
		return activities;
	}

	/**
	 * Create a new activity.
	 * 
	 * @param activity The activity to create.
	 * @param imageFiles A map of ActivityImage/File objects. These files will
	 *            be moved into a separate directory for the activity.
	 * @throws IOException Thrown if some IO error occurs.
	 * @throws NoSuchAlgorithmException Thrown if the SHA1 algorithm is not
	 *             available.
	 */
	public void createActivity(Activity activity, Map<ActivityImage, File> imageFiles,
			List<ActivityAvailability> availability) throws IOException, NoSuchAlgorithmException
	{
		// First create the activity.
		activityDao.create(activity);
		// Create availability entries for all entries
		for (ActivityAvailability a : availability)
		{
			activityAvailabilityDao.create(a);
		}
		// Create image entries for all files.
		for (ActivityImage image : imageFiles.keySet())
			activityImageModel.createActivityImage(activity, image, imageFiles.get(image));
	}

	/**
	 * Get an activity list by supplier
	 * 
	 * @param supplier
	 * @return List with activities
	 */
	public List<Activity> getBySupplier(Supplier supplier)
	{
		return activityDao.getBySupplier(supplier);
	}

	/**
	 * Get an activity by ID
	 * 
	 * @param supplier
	 * @return List with activities
	 */
	public Activity getByID(int activityId)
	{
		return activityDao.getById(activityId);
	}

	/**
	 * Get a booking count for Activity by given period
	 * 
	 * @param activity object
	 * @param date start
	 * @param date end
	 * @return integer booking count
	 */
	public int getBookingCountByPeriod(Activity activity, Date startDate, Date endDate)
	{
		return activityDao.getBookingCountByPeriod(activity, startDate, endDate);
	}

	/**
	 * Get a tarrif total for Activity by given period
	 * 
	 * @param activity object
	 * @param date start
	 * @param date end
	 * @return integer tariff total
	 */
	public int getTariffTotalByPeriod(Activity activity, Date startDate, Date endDate)
	{
		return activityDao.getTariffTotalByPeriod(activity, startDate, endDate);
	}

	/**
	 * Deletes the activity by ID
	 * 
	 * @param activityId ID of the activity to be deleted
	 */
	public void deleteActivity(int activityId)
	{
		// TODO: create a way to set activities inactive

	}

	/**
	 * 
	 * Returns a list of ActiviyiAvailabilities of the given Activity
	 * 
	 * @param activity
	 * @return
	 */
	public List<ActivityAvailability> getActivityAvailabilities(Activity activity)
	{
		return activityAvailabilityDao.getByActivity(activity);
	}

}
