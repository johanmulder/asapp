/**
 * 
 * File created at 23 okt. 2013 00:18:27 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.exceptions;

import asapp.models.types.Supplier;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@SuppressWarnings("serial")
public class SupplierAlreadyExistsException extends Exception
{
	private final Supplier supplier;

	public SupplierAlreadyExistsException(Supplier supplier)
	{
		this.supplier = supplier;
	}

	/**
	 * Get the existing supplier.
	 * 
	 * @return
	 */
	public Supplier getSupplier()
	{
		return supplier;
	}
}
