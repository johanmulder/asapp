package asapp.models.exceptions;

import asapp.models.types.ActivityCategory;

/**
 * 
 * @author JH
 */
@SuppressWarnings("serial")
public class ActivityCategoryAlreadyExistsException extends Exception
{
	private final ActivityCategory activityCategory;

	public ActivityCategoryAlreadyExistsException(ActivityCategory activityCategory)
	{
		this.activityCategory = activityCategory;
	}

	/**
	 * Get the existing supplier.
	 * 
	 * @return
	 */
	public ActivityCategory getSupplier()
	{
		return activityCategory;
	}
}