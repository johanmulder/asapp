/**
 * 
 * File created at 22 okt. 2013 23:47:25 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.exceptions;

import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@SuppressWarnings("serial")
public class UserAlreadyExistsException extends Exception
{
	private final User user;

	/**
	 * The existing user entry.
	 * 
	 * @param user
	 */
	public UserAlreadyExistsException(User user)
	{
		this.user = user;
	}

	/**
	 * Get the existing user entry.
	 * 
	 * @return
	 */
	public User getUser()
	{
		return user;
	}
}
