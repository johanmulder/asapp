/**
 * 
 * File created at 22 okt. 2013 23:53:26 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.exceptions;

import asapp.models.types.Participant;

/**
 * Thrown if a participant already exists.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@SuppressWarnings("serial")
public class ParticipantAlreadyExistsException extends Exception
{
	private final Participant participant;

	/**
	 * Exception constructor.
	 * 
	 * @param participant The actual existing Participant instance.
	 */
	public ParticipantAlreadyExistsException(Participant participant)
	{
		this.participant = participant;
	}

	/**
	 * Get the existing participant instance.
	 * 
	 * @return
	 */
	public Participant getParticipant()
	{
		return participant;
	}

}
