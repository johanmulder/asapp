package asapp.models;

import java.util.List;

import javax.inject.Inject;

import asapp.data.dao.ActivityCategoryDao;
import asapp.models.exceptions.ActivityCategoryAlreadyExistsException;
import asapp.models.types.ActivityCategory;

/**
 * 
 * @author JH
 */
public class ActivityCategoryModel
{
	@Inject
	private ActivityCategoryDao activityCategoryDao;

	/**
	 * Create a new activity category.
	 * 
	 * @param activityCategory
	 * @throws ActivityCategoryAlreadyExistsException
	 */
	public void create(ActivityCategory activityCategory)
			throws ActivityCategoryAlreadyExistsException
	{
		ActivityCategory currentActivityCategory = activityCategoryDao
				.getByCategory(activityCategory.getCategory());
		if (currentActivityCategory != null)
			throw new ActivityCategoryAlreadyExistsException(currentActivityCategory);
		activityCategoryDao.create(activityCategory);

	}

	/**
	 * Get all ActivityCategories
	 * 
	 * @return List with ActivityCategory objects
	 */
	public List<ActivityCategory> getAll()
	{
		return activityCategoryDao.getAll();
	}

	/**
	 * Get the ActivityCategory
	 * 
	 * @param String category name
	 * @return ActivityCategory object
	 */
	public ActivityCategory getByCategory(String cat)
	{
		return activityCategoryDao.getByCategory(cat);
	}

	/**
	 * Update a category.
	 * 
	 * @param object
	 */
	public void update(ActivityCategory activityCategory)
	{
		activityCategoryDao.update(activityCategory);
	}

	/**
	 * Set description for a category.
	 * 
	 * @param String description
	 * @return ActivityCategory
	 */
	public ActivityCategory setDescription(String description)
	{
		ActivityCategory activityCategory = activityCategoryDao.getByCategory(description);
		// activityCategory.setDescription(description);
		return activityCategory;
	}

}
