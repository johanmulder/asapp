package asapp.models.types;

// Created Oct 27, 2013 13:37:02 PM by Vincent Vogelesang

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Vincent Vogelesang
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "participant_group_member")
public class ParticipantGroupMember implements java.io.Serializable
{
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "participant_group_id", nullable = false)
	private ParticipantGroup participantGroup;
	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "participant_id", nullable = false)
	private Participant participant;
	@Column(name = "participant_confirmed", nullable = false)
	private int participantConfirmed;
	@Column(name = "initiator", nullable = false)
	private int initiator;
	@Column(name = "hash", nullable = false)
	private String hash;

	public ParticipantGroupMember()
	{
	}

	public ParticipantGroupMember(ParticipantGroup participantGroup, Participant participant,
			int participantConfirmed, int initiator)
	{
		this.participantGroup = participantGroup;
		this.participant = participant;
		this.participantConfirmed = participantConfirmed;
		this.initiator = initiator;
	}

	public ParticipantGroup getParticipantGroup()
	{
		return this.participantGroup;
	}

	public void setParticipantGroup(ParticipantGroup participantGroup)
	{
		this.participantGroup = participantGroup;
	}

	public Participant getParticipant()
	{
		return this.participant;
	}

	public void setParticipant(Participant participant)
	{
		this.participant = participant;
	}

	public int getParticipantConfirmed()
	{
		return this.participantConfirmed;
	}

	public void setParticipantConfirmed(int participantConfirmed)
	{
		this.participantConfirmed = participantConfirmed;
	}

	public int getInitiator()
	{
		return this.initiator;
	}

	public void setInitiator(int initiator)
	{
		this.initiator = initiator;
	}

	public String getHash()
	{
		return this.hash;
	}

	public void setHash(String hash)
	{
		this.hash = hash;
	}
}
