/**
 * 
 * File created at 29 okt. 2013 22:24:22 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public enum ParticipantGroupStatus
{
	PENDING_SELECTION, PENDING_BOOKING, INVALID, BOOKED, PARTICIPANTS_INFORMED, SUPPLIER_INFORMED;
}
