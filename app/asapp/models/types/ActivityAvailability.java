/**
 * 
 * File created at 28 okt. 2013 14:38:58 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@Entity
@Table(name = "activity_availability")
public class ActivityAvailability
{
	@Id
	@GeneratedValue
	@Column(name = "activity_availability_id")
	private int activityAvailabilityId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activity_id", nullable = false)
	private Activity activity;
	@Column(name = "datetime", nullable = false)
	private Date dateTime;
	@Column(name = "min_participants")
	private int minParticipants = 0;
	@Column(name = "max_participants")
	private int maxParticipants = 0;
	@Column(name = "tariff_participant")
	private BigDecimal tariffParticipant;
	@Column(name = "participant_discount")
	private BigDecimal participantDiscount;
	@Column(name = "max_discount")
	private BigDecimal maxDiscount;
	@ManyToMany(mappedBy = "activityAvailabilities", fetch = FetchType.LAZY)
	private Set<ParticipantGroup> participantGroups = new HashSet<>();
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "activityAvailability")
	private Set<ActivityBooking> bookings = new HashSet<>();

	public int getActivityAvailabilityId()
	{
		return activityAvailabilityId;
	}

	public void setActivityAvailabilityId(int activityAvailabilityId)
	{
		this.activityAvailabilityId = activityAvailabilityId;
	}

	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public Date getDateTime()
	{
		return dateTime;
	}

	public void setDateTime(Date dateTime)
	{
		this.dateTime = dateTime;
	}

	public int getMinParticipants()
	{
		return minParticipants;
	}

	public void setMinParticipants(int minParticipants)
	{
		this.minParticipants = minParticipants;
	}

	public int getMaxParticipants()
	{
		return maxParticipants;
	}

	public void setMaxParticipants(int maxParticipants)
	{
		this.maxParticipants = maxParticipants;
	}

	public BigDecimal getTariffParticipant()
	{
		return tariffParticipant;
	}

	public void setTariffParticipant(BigDecimal tariffParticipant)
	{
		this.tariffParticipant = tariffParticipant;
	}

	public BigDecimal getParticipantDiscount()
	{
		return participantDiscount;
	}

	public void setParticipantDiscount(BigDecimal participantDiscount)
	{
		this.participantDiscount = participantDiscount;
	}

	public BigDecimal getMaxDiscount()
	{
		return maxDiscount;
	}

	public void setMaxDiscount(BigDecimal maxDiscount)
	{
		this.maxDiscount = maxDiscount;
	}

	public Set<ParticipantGroup> getParticipantGroups()
	{
		return participantGroups;
	}

	public void addParticipantGroup(ParticipantGroup participantGroup)
	{
		participantGroups.add(participantGroup);
	}

	public Set<ActivityBooking> getBookings()
	{
		return bookings;
	}

	public void addBooking(ActivityBooking booking)
	{
		bookings.add(booking);
	}
}
