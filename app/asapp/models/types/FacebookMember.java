package asapp.models.types;

// Generated Oct 16, 2013 11:03:02 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * FacebookMember generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "facebook_member", uniqueConstraints = @UniqueConstraint(columnNames = "uid"))
public class FacebookMember implements java.io.Serializable
{
	@Id
	@Column(name = "facebook_member_id", unique = true, nullable = false)
	@GeneratedValue
	private int facebookMemberId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "facebook_friendlist_id", nullable = false)
	private FacebookFriendlist facebookFriendlist;
	@Column(name = "uid", unique = true, nullable = false)
	private long uid;
	@Column(name = "first_name", length = 30)
	private String firstName;
	@Column(name = "middle_name", length = 15)
	private String middleName;
	@Column(name = "last_name", length = 30)
	private String lastName;

	public FacebookMember()
	{
	}

	public FacebookMember(int facebookMemberId, FacebookFriendlist facebookFriendlist, long uid)
	{
		this.facebookMemberId = facebookMemberId;
		this.facebookFriendlist = facebookFriendlist;
		this.uid = uid;
	}

	public FacebookMember(int facebookMemberId, FacebookFriendlist facebookFriendlist, long uid,
			String firstName, String middleName, String lastName)
	{
		this.facebookMemberId = facebookMemberId;
		this.facebookFriendlist = facebookFriendlist;
		this.uid = uid;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}

	public int getFacebookMemberId()
	{
		return this.facebookMemberId;
	}

	public void setFacebookMemberId(int facebookMemberId)
	{
		this.facebookMemberId = facebookMemberId;
	}

	public FacebookFriendlist getFacebookFriendlist()
	{
		return this.facebookFriendlist;
	}

	public void setFacebookFriendlist(FacebookFriendlist facebookFriendlist)
	{
		this.facebookFriendlist = facebookFriendlist;
	}

	public long getUid()
	{
		return this.uid;
	}

	public void setUid(long uid)
	{
		this.uid = uid;
	}

	public String getFirstName()
	{
		return this.firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return this.middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

}
