/**
 * 
 * File created at 23 okt. 2013 22:47:18 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@Entity
@Table(name = "activity_image")
public class ActivityImage
{
	@Id
	@GeneratedValue
	@Column(name = "activity_image_id", nullable = false)
	private int activityImageId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activity_id", nullable = false)
	private Activity activity;
	@Column(name = "description", nullable = false, length = 45)
	private String description;
	@Column(name = "content_type", nullable = false, length = 45)
	private String contentType;
	@Column(name = "hash", nullable = false, length = 40)
	private String hash;
	@Column(name = "size", nullable = false)
	private long size;

	public int getActivityImageId()
	{
		return activityImageId;
	}

	public void setActivityImageId(int activityImageId)
	{
		this.activityImageId = activityImageId;
	}

	public Activity getActivity()
	{
		return activity;
	}

	public void setActivity(Activity activity)
	{
		this.activity = activity;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getHash()
	{
		return hash;
	}

	public void setHash(String hash)
	{
		this.hash = hash;
	}

	public long getSize()
	{
		return size;
	}

	public void setSize(long size)
	{
		this.size = size;
	}

}
