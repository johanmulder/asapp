/**
 * 
 * File created at 9 nov. 2013 21:23:45 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

/**
 * Facebook Auth token pojo.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@Entity
@Table(name = "facebook_data")
public class FacebookData
{
	@Id
	@Column(name = "uid")
	private BigDecimal userId;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "participant_id")
	private Participant participant;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplier_id")
	private Supplier supplier;
	@Column(name = "expires", nullable = false)
	private Date expireTime;
	@Column(name = "token", nullable = false)
	private String token;
	@Column(name = "name")
	private String name;
	@Column(name = "email")
	private String email;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "middle_name")
	private String middleName = "";
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "gender")
	private String gender;

	public BigDecimal getUserId()
	{
		return userId;
	}

	public void setUserId(BigDecimal userId)
	{
		this.userId = userId;
	}

	public Participant getParticipant()
	{
		return participant;
	}

	public void setParticipant(Participant participant)
	{
		this.participant = participant;
	}

	public Supplier getSupplier()
	{
		return supplier;
	}

	public void setSupplier(Supplier supplier)
	{
		this.supplier = supplier;
	}

	public Date getExpireTime()
	{
		return expireTime;
	}

	public void setExpireTime(Date expireTime)
	{
		this.expireTime = expireTime;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}
