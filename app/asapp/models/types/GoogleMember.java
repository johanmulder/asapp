package asapp.models.types;

// Generated Oct 16, 2013 11:03:02 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * GoogleMember generated by hbm2java
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "google_member", uniqueConstraints = @UniqueConstraint(columnNames = "uid"))
public class GoogleMember implements java.io.Serializable
{
	@Id
	@Column(name = "google_member_id", unique = true, nullable = false)
	private int googleMemberId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "google_friendlist_id", nullable = false)
	private GoogleFriendlist googleFriendlist;
	@Column(name = "uid", unique = true, nullable = false)
	private long uid;
	@Column(name = "first_name", length = 30)
	private String firstName;
	@Column(name = "middle_name", length = 15)
	private String middleName;
	@Column(name = "last_name", length = 30)
	private String lastName;

	public GoogleMember()
	{
	}

	public GoogleMember(int googleMemberId, GoogleFriendlist googleFriendlist, long uid)
	{
		this.googleMemberId = googleMemberId;
		this.googleFriendlist = googleFriendlist;
		this.uid = uid;
	}

	public GoogleMember(int googleMemberId, GoogleFriendlist googleFriendlist, long uid,
			String firstName, String middleName, String lastName)
	{
		this.googleMemberId = googleMemberId;
		this.googleFriendlist = googleFriendlist;
		this.uid = uid;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}

	public int getGoogleMemberId()
	{
		return this.googleMemberId;
	}

	public void setGoogleMemberId(int googleMemberId)
	{
		this.googleMemberId = googleMemberId;
	}

	public GoogleFriendlist getGoogleFriendlist()
	{
		return this.googleFriendlist;
	}

	public void setGoogleFriendlist(GoogleFriendlist googleFriendlist)
	{
		this.googleFriendlist = googleFriendlist;
	}

	public long getUid()
	{
		return this.uid;
	}

	public void setUid(long uid)
	{
		this.uid = uid;
	}

	public String getFirstName()
	{
		return this.firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getMiddleName()
	{
		return this.middleName;
	}

	public void setMiddleName(String middleName)
	{
		this.middleName = middleName;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

}
