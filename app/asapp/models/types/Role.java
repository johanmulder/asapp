/**
 * 
 * File created at 22 okt. 2013 23:02:53 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@Entity
@Table(name = "role")
public class Role
{
	@Id
	@GeneratedValue
	@Column(name = "role_id", nullable = false)
	private int roleId;
	@Column(name = "name", nullable = false, length = 30)
	private String name;
	@Column(name = "description", nullable = true, length = 45)
	private String description;

	public int getRoleId()
	{
		return roleId;
	}

	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

}
