/**
 * 
 * File created at 22 okt. 2013 23:02:42 by Johan Mulder <johan@mulder.net>
 */
package asapp.models.types;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@Entity
@Table(name = "user")
public class User
{
	@Id
	@GeneratedValue
	@Column(name = "user_id", nullable = false)
	private int userId;
	@Column(name = "username", nullable = true, length = 45)
	private String username;
	@Column(name = "password", nullable = true, length = 100)
	private String password;
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "role_id")
	private Role role;

	public User()
	{

	}

	/**
	 * Construct a new user object with all vars filled in.
	 * 
	 * @param username
	 * @param password
	 * @param role
	 */
	public User(String username, String password, Role role)
	{
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public Role getRole()
	{
		return role;
	}

	public void setRole(Role role)
	{
		this.role = role;
	}

}
