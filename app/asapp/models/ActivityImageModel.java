/**
 * 
 * File created at 26 okt. 2013 12:08:10 by Vincent Vogelesang
 */
package asapp.models;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asapp.data.dao.ActivityImageDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityImage;
import asapp.supplierportal.controllers.RegisterActivity;
import asapp.util.AppConfig;
import asapp.util.CryptUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ActivityImageModel
{
	@Inject
	private ActivityImageDao activityImageDao;
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(RegisterActivity.class);

	/**
	 * Returns a list of ActivityImages for a given activity
	 * 
	 * @param Activity
	 * @return List with ActivityImage objects
	 */
	public List<ActivityImage> getActivityImageByActivity(Activity activity)
	{
		List<ActivityImage> activityImageList = activityImageDao.getByActivity(activity);
		return activityImageList;
	}

	/**
	 * Get an activity image by id.
	 * 
	 * @param imageId
	 * @return
	 */
	public ActivityImage getById(int imageId)
	{
		return activityImageDao.getById(imageId);
	}

	/**
	 * Delete an ActivityImage
	 * 
	 * @param activityImage the Image to be deleted
	 */
	public void deleteActivityImage(ActivityImage activityImage)
	{
		String hash = activityImage.getHash();
		Activity activity = activityImage.getActivity();
		String destination = getImagePath(activity, hash);
		File deleteFile = new File(destination);
		deleteFile.delete();
		activityImageDao.delete(activityImage);

	}

	/**
	 * Create an activity image for the given image.
	 * 
	 * @param activity The activity to create the image for.
	 * @param image The ActivityImage object filled with content type and such.
	 * @param imageFile The source file to read from.
	 * @throws FileNotFoundException Thrown if imageFile was not found.
	 * @throws IOException Thrown if some IO error occurs.
	 * @throws NoSuchAlgorithmException Thrown if the SHA1 algorithm is not
	 *             available.
	 */
	public void createActivityImage(Activity activity, ActivityImage image, File imageFile)
			throws FileNotFoundException, IOException, NoSuchAlgorithmException
	{
		if (!imageFile.exists())
			throw new FileNotFoundException("Source file " + imageFile.getCanonicalPath()
					+ " not found");
		// Generate a random hash.
		String hash = getRandomHash();
		String destination = getImagePath(activity, hash);
		File destinationFile = new File(destination);
		// Create the destination directory if it doesn't exist.
		destinationFile.mkdirs();
		// TODO: Files.move doesn't work cross filesystem. Should be fixed.
		Files.move(imageFile.toPath(), destinationFile.toPath(),
				StandardCopyOption.REPLACE_EXISTING);
		logger.debug("Moved " + imageFile.toPath() + " to " + destinationFile.toPath());
		// Create a new ActivityImage entry.
		image.setActivity(activity);
		image.setSize(imageFile.length());
		image.setHash(hash);
		activityImageDao.create(image);
	}

	/**
	 * Get the image path for the given Activity.
	 * 
	 * @param activity the activity where the image belongs to
	 * @param fileName the name of the file
	 * @return
	 */
	private String getImagePath(Activity activity, String fileName)
	{
		return AppConfig.getActivityImagePath(new String[]
			{ Integer.toString(activity.getActivityId()), fileName });
	}

	/**
	 * Generate a random hash from 128 bytes of random data.
	 * 
	 * @return
	 * @throws NoSuchAlgorithmException Thrown if the SHA1 algorithm is not
	 *             available.
	 */
	private String getRandomHash() throws NoSuchAlgorithmException
	{
		Random r = new Random();
		byte[] randomBytes = new byte[128];
		r.nextBytes(randomBytes);
		return CryptUtils.getSha1Hash(randomBytes);
	}

}
