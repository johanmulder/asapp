/**
 * 
 * File created at 22 okt. 2013 23:45:33 by Johan Mulder <johan@mulder.net>
 */
package asapp.models;

import java.util.List;

import javax.inject.Inject;

import asapp.data.dao.UserDao;
import asapp.models.exceptions.UserAlreadyExistsException;
import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class UserModel
{
	@Inject
	private UserDao userDao;

	/**
	 * Create a new user.
	 * 
	 * @param user
	 * @throws UserAlreadyExistsException
	 */
	public void create(User user) throws UserAlreadyExistsException
	{
		User currentUser = userDao.getByUsername(user.getUsername());
		if (currentUser != null)
			throw new UserAlreadyExistsException(currentUser);
		userDao.create(user);
	}

	/**
	 * Get all Users
	 * 
	 * @return List with User objects
	 */
	public List<User> getAllUsers()
	{
		return userDao.getAllUsers();
	}

	/**
	 * Delete a user.
	 * 
	 * @param userid
	 */
	public void delete(int userId)
	{
		User user = userDao.getById(userId);
		userDao.delete(user);
	}

}
