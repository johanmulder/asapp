/**
 * 
 * File created at 22 okt. 2013 23:50:42 by Johan Mulder <johan@mulder.net>
 */
package asapp.models;

import javax.inject.Inject;

import asapp.data.dao.ParticipantDao;
import asapp.data.dao.RoleDao;
import asapp.models.exceptions.ParticipantAlreadyExistsException;
import asapp.models.exceptions.UserAlreadyExistsException;
import asapp.models.types.Participant;
import asapp.models.types.User;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class ParticipantModel
{
	@Inject
	private ParticipantDao participantDao;
	@Inject
	private RoleDao roleDao;
	@Inject
	private UserModel userModel;

	/**
	 * Create a new participant.
	 * 
	 * @param participant
	 * @throws ParticipantAlreadyExistsException
	 */
	public void create(Participant participant) throws ParticipantAlreadyExistsException
	{
		Participant currentParticipant = participantDao.getByEmail(participant.getEmail());
		if (currentParticipant != null)
			throw new ParticipantAlreadyExistsException(currentParticipant);
		participantDao.create(participant);
	}

	/**
	 * Create a new participant with the given password.
	 * 
	 * @param newParticipant
	 * @param password
	 * @return
	 * @throws UserAlreadyExistsException
	 * @throws ParticipantAlreadyExistsException
	 */
	public void create(Participant newParticipant, String password)
			throws UserAlreadyExistsException, ParticipantAlreadyExistsException
	{
		// Create a new user object with the user role.
		User user = new User(newParticipant.getEmail(), password,
				roleDao.getByName(asapp.auth.Role.USER.toString()));
		userModel.create(user);
		newParticipant.setUser(user);
		create(newParticipant);
	}
	
	/**
	 * Get a participant by email
	 * 
	 * @param Email
	 * @return Pariticpant object
	 */	
	public Participant getByEmail(String email)
	{
		return participantDao.getByEmail(email);
	}
}
