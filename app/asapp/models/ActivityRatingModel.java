/**
 * 
 * File created at 3 nov. 2013 15:06:00 by Vincent Vogelesang
 */
package asapp.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import asapp.data.dao.ActivityRatingDao;
import asapp.models.types.Activity;
import asapp.models.types.ActivityCategory;
import asapp.models.types.ActivityRating;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ActivityRatingModel
{
	@Inject
	private ActivityRatingDao activityRatingDao;

	/**
	 * Create an activityRating
	 * 
	 * @see asapp.data.dao.ActivityRatingDao#create(asapp.models.ActivityRating)
	 */
	public void create(ActivityRating activityRating)
	{
		activityRatingDao.create(activityRating);
	}

	/**
	 * Get an average activityRating by Activity.
	 * 
	 * @param Activity
	 * @return int rating (1-10)
	 */
	public int getAvgRatingByActivity(Activity activity)
	{
		return activityRatingDao.getAvgRatingByActivity(activity);
	}

	/**
	 * Get an activityRating list by Activity
	 * 
	 * @param Activity
	 * @return ActivityRating list
	 */
	public List<ActivityRating> getRatingsByActivity(Activity activity)
	{
		return activityRatingDao.getRatingsByActivity(activity);
	}

	/**
	 * Get an activityRating remark list by Activity
	 * 
	 * @param Activity
	 * @return Remark list
	 */
	public List<String> getRemarksByActivity(Activity activity)
	{
		List<String> remarkList = new ArrayList<>();
		List<ActivityRating> ratingsList = activityRatingDao.getRatingsByActivity(activity);

		for (ActivityRating activityRating : ratingsList)
		{
			remarkList.add(activityRating.getReview());
		}
		return remarkList;
	}

	/**
	 * Get top10 for ActivityCategory
	 * 
	 * @param ActivityCategory
	 * @return rating list
	 */
	public List<String> getTop10ByCategory(ActivityCategory activityCategory)
	{
		return activityRatingDao.getTop10ByCategory(activityCategory);
	}
}
