/**
 * 
 * File created at 27 okt. 2013 11:36:56 by Vincent Vogelesang
 */
package asapp.models;

import java.util.List;

import javax.inject.Inject;

import asapp.data.dao.ParticipantGroupDao;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;

/**
 * 
 * @author Vincent Vogelesang
 */
public class ParticipantGroupModel
{
	@Inject
	private ParticipantGroupDao participantGroupDao;

	/**
	 * Create a new participantGroup.
	 * 
	 * @param participantGroup
	 */
	public void create(ParticipantGroup participantGroup)
	{
		participantGroupDao.create(participantGroup);
	}

	/**
	 * Delete a participantGroup.
	 * 
	 * @param participantGroup
	 */
	public void delete(ParticipantGroup participantGroup)
	{
		participantGroupDao.delete(participantGroup);
	}

	/**
	 * Get a participantGroup by id
	 * 
	 * @param id
	 * @return Pariticpant object
	 */
	public ParticipantGroup getById(int id)
	{
		return participantGroupDao.getById(id);
	}

	/**
	 * Update a participant group entry.
	 * 
	 * @param participantGroup
	 */
	public void update(ParticipantGroup participantGroup)
	{
		participantGroupDao.update(participantGroup);
	}

	/**
	 * Get the groups for which a booking needs to be made.
	 * 
	 * @return
	 */
	public List<ParticipantGroup> getGroupsPendingBooking()
	{
		return participantGroupDao.getByStatus(ParticipantGroupStatus.PENDING_BOOKING);
	}

	/**
	 * Get the groups needing activity selection.
	 * 
	 * @return
	 */
	public List<ParticipantGroup> getGroupsPendingSelection()
	{
		return participantGroupDao.getByStatus(ParticipantGroupStatus.PENDING_SELECTION);
	}

	/**
	 * Get the participant groups which have been booked, but of which the
	 * participants haven't been informed yet.
	 * 
	 * @return
	 */
	public List<ParticipantGroup> getGroupsPendingMailing()
	{
		return participantGroupDao.getByStatus(ParticipantGroupStatus.BOOKED);
	}
}
