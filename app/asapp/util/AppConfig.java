/**
 * 
 * File created at 5 nov. 2013 21:43:54 by Johan Mulder <johan@mulder.net>
 */
package asapp.util;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class AppConfig
{
	/**
	 * Get the global activity image path.
	 * 
	 * @return
	 */
	public static String getActivityImagePath()
	{
		// TODO: This should actually be moved into asapp.AsAppConfig
		return play.Play.application().configuration().getString("activity.image.path");
	}

	/**
	 * Get the activity image path + any subpath. This can be useful to generate
	 * an absolte filename.
	 * 
	 * @param subPath
	 * @return
	 */
	public static String getActivityImagePath(String[] subPath)
	{
		// TODO: This should actually be moved into asapp.AsAppConfig
		StringBuilder sb = new StringBuilder(getActivityImagePath());
		for (String s : subPath)
			sb.append(System.getProperty("file.separator")).append(s);
		return sb.toString();
	}
}
