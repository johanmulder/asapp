/**
 * 
 * File created at 28 okt. 2013 14:07:58 by Johan Mulder <johan@mulder.net>
 */
package asapp.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

/**
 * Utility class for crypt methods.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class CryptUtils
{
	/**
	 * Get a SHA1 hash of a string.
	 * 
	 * @param input
	 * @return
	 */
	public static String getSha1Hash(String input) throws NoSuchAlgorithmException
	{
		return getSha1Hash(input.getBytes());
	}

	/**
	 * Get a SHA1 hash of input bytes.
	 * 
	 * @param input
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String getSha1Hash(byte[] input) throws NoSuchAlgorithmException
	{
		MessageDigest hash;
		hash = MessageDigest.getInstance("SHA-1");
		hash.update(input);
		return new String(Hex.encodeHex(hash.digest()));
	}

}
