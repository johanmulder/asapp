package asapp.util;

/*
 * Password checker
 */

public class Password {
	
	/**
	 * Check if two given password mismatch.
	 * 
	 * @param password
	 * @param validate
	 * @return
	 */
	public static boolean isPasswordMismatch(String password, String validate)
	{
		return password == null || validate == null || !password.equals(validate);
	}	
}
