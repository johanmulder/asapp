package asapp.util;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.MimeMessage.RecipientType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that makes a Mail handler
 * 
 * @author JH
 */
public class Mailer
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(Mailer.class);
	private static Session session = null;

	/**
	 * Return the mail session
	 * 
	 * @return session
	 */
	public Session getSession()
	{
		// Initialize only once.
		if (session != null)
			return session;

		// Initialize properties.
		Properties props = new Properties();

		// Set SMTP authentication if required.
		final String username, password;
		boolean smtpAuth = play.Play.application().configuration().getBoolean("mail.smtp.auth");
		if (smtpAuth)
		{
			props.put("mail.smtp.auth", "true");
			username = play.Play.application().configuration().getString("mail.smtp.username");
			password = play.Play.application().configuration().getString("mail.smtp.password");
		}
		else
		{
			props.put("mail.smtp.auth", "false");
			username = password = null;
		}

		// Other options.
		props.put("mail.smtp.host",
				play.Play.application().configuration().getString("mail.smtp.host"));
		props.put("mail.smtp.port",
				play.Play.application().configuration().getString("mail.smtp.port"));
		props.put("mail.smtp.socketFactory.port", play.Play.application().configuration()
				.getString("mail.smtp.socketFactory.port"));
		props.put("mail.smtp.socketFactory.class", play.Play.application().configuration()
				.getString("mail.smtp.socketFactory.class"));

		if (smtpAuth)
			// Initialize with an authenticator.
			session = Session.getInstance(props, new javax.mail.Authenticator()
			{
				@Override
				protected PasswordAuthentication getPasswordAuthentication()
				{
					return new PasswordAuthentication(username, password);
				}
			});
		else
			session = Session.getDefaultInstance(props);

		return session;
	}

	/**
	 * Send the message
	 */
	public void sendMail(Message message) throws MessagingException
	{
		// Debug log the recipients.
		Address[] recipients = message.getRecipients(RecipientType.TO);
		for (Address address : recipients)
			logger.debug("Sending mail with subject \"" + message.getSubject() + "\" to "
					+ address.toString());
		// Send the message.
		Transport.send(message);
	}
}