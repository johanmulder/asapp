package asapp.util;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/*
 * Date Formatter
 * @author Vincent Vogelesang
 */
public class DateFormatter {
	
	/**
	 * Generate Date from string
	 * 
	 * @param Datestring
	 * @param Format of datestring
	 * @return Date 
	 */
	public static Date dateFromString(String dateString, String format)
	{
		try 
		{
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			Date date = formatter.parse(dateString);	
			return date;
		} 
		catch (ParseException ex)
		{ 
			ex.printStackTrace();
			return null;
		}
	}	
}
