/**
 * 
 * File created at 10 nov. 2013 14:57:00 by Johan Mulder <johan@mulder.net>
 */
package asapp.util;

import play.libs.F.Function0;
import play.libs.F.Promise;
import play.mvc.Result;

/**
 * Utility methods for promises.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class PromiseUtil
{
	/**
	 * Get a promise for the given result.
	 * 
	 * @param result
	 * @return
	 */
	public static Promise<Result> getResultPromise(final Result result)
	{
		return Promise.promise(new Function0<Result>()
		{
			@Override
			public Result apply() throws Throwable
			{
				return result;
			}
		});

	}

}
