package asapp.adminportal.controllers;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.adminportal.views.html.index;
import asapp.frontend.controllers.SecuredAdmin;

/**
 * Main controller for the admin portal
 * 
 * @author JH
 */
@Security.Authenticated(SecuredAdmin.class)
public class Main extends Controller
{
	public Result index()
	{
		return ok(index.render());
	}
}
