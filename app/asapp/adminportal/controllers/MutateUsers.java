package asapp.adminportal.controllers;

import java.util.List;

import javax.inject.Inject;

import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.controllers.SecuredAdmin;
import asapp.adminportal.views.html.mutateusers;
import asapp.adminportal.views.html.mutateactivity;
import asapp.models.UserModel;
import asapp.models.types.User;

/**
 * Controller handling the mutations of users.
 * 
 * @author JH
 */
@Security.Authenticated(SecuredAdmin.class)
public class MutateUsers extends Controller
{
	@Inject
	private UserModel userModel;

	/**
	 * Method handling the main mutation page for users.
	 * 
	 * @return
	 */
	@Transactional
	public Result usersOverview()
	{
		List<User> userList = userModel.getAllUsers();
		return ok(mutateusers.render(userList));
	}

	/**
	 * Method handling the deletion of a user.
	 * 
	 * @return
	 */
	@Transactional
	public Result delete(int userId)
	{
		// userModel.delete(userId);
		return ok(mutateactivity.render()); // JH: Needs a own page, no time to
											// implement
	}
}
