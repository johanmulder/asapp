package asapp.adminportal.controllers;

import static play.data.Form.form;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.auth.Role;
import asapp.data.dao.UserDao;
import asapp.models.types.User;
import asapp.adminportal.views.html.login;

/**
 * Controller handling the admin login.
 * 
 * @author JH
 */
public class Login extends Controller
{
	// Logging object.
	private static Logger logger = LoggerFactory.getLogger(Login.class);
	@Inject
	private UserDao userDao;

	/**
	 * Method handling the login page for unauthenticated users.
	 * 
	 * @return Result
	 */
	public Result login()
	{
		return ok(login.render(form(Logins.class)));
	}

	/**
	 * Class containing the email and password of a user.
	 * 
	 * @author JH
	 */
	public static class Logins
	{
		public String email;
		public String password;

	}

	/**
	 * Authenticate the user. If successful sets session variable and returns to
	 * index.
	 * 
	 * @return result
	 */
	@Transactional
	public Result authenticate()
	{
		Form<Logins> loginForm = form(Logins.class).bindFromRequest();
		Logins loginData = loginForm.get();
		User user = userDao.getUserByNameAndRole(loginData.email, Role.ADMIN.toString());

		// Check if the given email address exists
		if (user == null)
		{
			logger.info("Auth failed: username " + loginData.email + " does not exist");
			return badRequest(login.render(loginForm));
		}
		else
		{
			if (user.getPassword().equals(loginData.password))
			{
				session().clear();
				session("email", loginData.email);
				session("role", user.getRole().getName());
				// Log it.
				logger.info("Logged in as " + loginData.email + " from "
						+ request().remoteAddress());
				// Redirect location after logged in
				return redirect(routes.Main.index());
			}
		}
		// Return loginform if not authenticated.
		return badRequest(login.render(loginForm));
	}

	/**
	 * Controller function handling the log out.
	 * 
	 * @return Result
	 */
	public Result logout()
	{
		session().clear();
		flash("success", "You've been logged out");
		return redirect(routes.Main.index());
	}
}
