package asapp.adminportal.controllers;

import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.controllers.SecuredAdmin;
import asapp.models.ActivityCategoryModel;
import asapp.models.exceptions.ActivityCategoryAlreadyExistsException;
import asapp.models.types.*;
import asapp.adminportal.views.html.mutatecategory;
import asapp.adminportal.views.html.successnewcategory;
import asapp.adminportal.views.html.successmutatecategory;
import asapp.adminportal.views.html.transformcategory;
import asapp.adminportal.views.html.transformcategoryerror;

/**
 * Controller handling the mutations of categories.
 * 
 * @author JH
 */
@Security.Authenticated(SecuredAdmin.class)
public class MutateCategory extends Controller
{
	// Logger object.
	private static Logger logger = LoggerFactory.getLogger(MutateCategory.class);
	@Inject
	private ActivityCategoryModel activityCategoryModel;
	// The form handling the registration.
	private static Form<ActivityCategory> categoryForm = Form.form(ActivityCategory.class);

	/**
	 * Method handling the main mutation page for categories.
	 * 
	 * @return
	 */
	@Transactional
	public Result categoryOverview()
	{
		List<ActivityCategory> activityCategoryList = activityCategoryModel.getAll();
		return ok(mutatecategory.render(categoryForm, activityCategoryList));
	}

	/**
	 * Method handling the posted data for activity category. This method
	 * validates the form and will create the new activity category. When
	 * created, a confirmation link will be sent.
	 * 
	 * @return
	 */
	@Transactional
	public Result makeActivityCategory()
	{
		// Bind the form to the http request.
		Form<ActivityCategory> catForm = categoryForm.bindFromRequest();
		List<ActivityCategory> activityCategoryList = activityCategoryModel.getAll();

		// Check if the given category and description are valid.
		String category = catForm.data().get("category");
		String description = catForm.data().get("description");
		if (category == "" || description == "")
		{
			logger.debug("Category and description form input not valid");
			return badRequest(mutatecategory.render(catForm, activityCategoryList));
		}
		if (catForm.hasErrors())
		{
			logger.debug("Category form has errors: " + catForm.errorsAsJson());
			return badRequest(mutatecategory.render(catForm, activityCategoryList));
		}
		else
		{
			try
			{
				// Get the Category object from the posted form.
				ActivityCategory newCategory = catForm.get();
				// Create the new Category.
				activityCategoryModel.create(newCategory);
				return ok(successnewcategory.render());
			}
			catch (ActivityCategoryAlreadyExistsException e)
			{
				logger.error("Exception caught", e);
				catForm.reject("Exception caught: " + e.getMessage());
				return badRequest(mutatecategory.render(catForm, activityCategoryList));
			}
		}
	}

	/**
	 * Method handling the successfull new categories entries.
	 * 
	 * @return
	 */
	@Transactional
	public Result madeNewCategory()
	{
		return ok(successnewcategory.render());
	}

	/**
	 * Method handling the mutation of a categories.
	 * 
	 * @return
	 */
	@Transactional
	public Result mutateCat(String cat)
	{
		logger.debug("Trying to open mutation form for category : " + cat);

		try
		{
			// Get the Category object from the posted form.
			ActivityCategory activityCategory = activityCategoryModel.getByCategory(cat);
			return ok(transformcategory.render(activityCategory, categoryForm));
		}
		catch (NullPointerException e)
		{
			logger.error("Exception caught", e);
			return badRequest(transformcategoryerror.render());
		}
	}

	/**
	 * Method handling the posted data for updating a activity category. This
	 * method validates the form and will update the activity category.
	 * 
	 * @return
	 */
	@Transactional
	public Result updateActivityCategory()
	{
		// Bind the form to the http request.
		Form<ActivityCategory> catForm = categoryForm.bindFromRequest();

		// Check if the given category and description are valid.
		String category = catForm.data().get("category");
		String description = catForm.data().get("description");
		ActivityCategory currentCategory = activityCategoryModel.getByCategory(catForm.data().get(
				"category"));
		if (category == "" || description == "")
		{
			logger.debug("Category and description form input not valid for updating");
			return badRequest(transformcategory.render(currentCategory, catForm));
		}
		if (catForm.hasErrors())
		{
			logger.debug("Category form has errors: " + catForm.errorsAsJson());
			return badRequest(transformcategory.render(currentCategory, catForm));
		}
		else
		{
			try
			{
				// Get the Category object from the posted form.
				ActivityCategory updateCat = activityCategoryModel.getByCategory(catForm.get()
						.getCategory());
				// Update the Category with new description.
				updateCat.setDescription(description);
				activityCategoryModel.update(updateCat);
				return ok(successmutatecategory.render());
			}
			catch (Exception e)
			{
				logger.error("Update Exception caught", e);
				catForm.reject("Exception caught: " + e.getMessage());
				return badRequest(transformcategory.render(currentCategory, catForm));
			}
		}
	}
}
