package asapp.adminportal.controllers;

import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import asapp.frontend.controllers.SecuredAdmin;
import asapp.adminportal.views.html.mutateactivity;

/**
 * Controller handling the mutations of activities.
 * 
 * @author JH
 */
@Security.Authenticated(SecuredAdmin.class)
public class MutateActivity extends Controller
{
	/**
	 * Method handling the main mutation page for activities.
	 * 
	 * @return Result
	 */
	@Transactional
	public Result mutate()
	{
		return ok(mutateactivity.render());
	}

}
