/**
 * 
 * File created at 9 nov. 2013 21:08:00 by Johan Mulder <johan@mulder.net>
 */
package asapp.oauth.controllers;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@SuppressWarnings("serial")
public class InvalidAuthCallbackRequestException extends Exception
{
	public InvalidAuthCallbackRequestException(String message)
	{
		super(message);
	}
}
