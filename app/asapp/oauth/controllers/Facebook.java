/**
 * 
 * File created at 9 nov. 2013 20:57:11 by Johan Mulder <johan@mulder.net>
 */
package asapp.oauth.controllers;

import java.util.Date;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.F.Function;
import play.libs.F.Function0;
import play.libs.F.Promise;
import play.mvc.Call;
import play.mvc.Controller;
import play.mvc.Result;
import asapp.data.dao.FacebookDataDao;
import asapp.frontend.controllers.routes;
import asapp.models.types.FacebookData;
import asapp.models.types.Participant;
import asapp.oauth.providers.FacebookProvider;
import asapp.oauth.providers.FacebookToken;
import asapp.util.PromiseUtil;

/**
 * Facebook callback controller.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class Facebook extends Controller
{
	// Logger object.
	private Logger logger = LoggerFactory.getLogger(Facebook.class);
	@Inject
	private FacebookProvider facebookProvider;
	@Inject
	private FacebookDataDao facebookDataDao;

	/**
	 * Class containing facebook authorization callback data.
	 * 
	 * @author Johan Mulder <johan@mulder.net>
	 */
	static class FacebookAuthCallbackData
	{
		public String code;
		public String errorMessage;
		public String errorCode;
	}

	/**
	 * Controller action called by facebook.
	 * 
	 * @return A Promise containing the Result, as there are asynchronous
	 *         actions in this controller method.
	 */
	public Promise<Result> callback()
	{
		final FacebookAuthCallbackData fbData;
		try
		{
			// Get the callback data. These are regular parameters via a GET
			// request.
			fbData = getDataFromRequest(request().queryString());
		}
		catch (InvalidAuthCallbackRequestException e)
		{
			logger.error("Invalid callback data: " + e.getMessage(), e);
			return PromiseUtil.getResultPromise(badRequest("Invalid calback data: "
					+ e.getMessage()));
		}

		// Convert the code from the callback to an access token.
		// A flatMap needs to be used as there are multiple asynchronous
		// actions.
		return facebookProvider.codeToToken(fbData.code).flatMap(
				new Function<FacebookToken, Promise<Result>>()
				{
					@Override
					public Promise<Result> apply(final FacebookToken token) throws Throwable
					{
						// FacebookProvider returns a promise with a big
						// decimal, as the facebook data is getting retrieved
						// asynchronously.
						return facebookProvider.getBaseData(token.getAccessToken()).map(
								new Function<FacebookData, Result>()
								{
									@Override
									public Result apply(final FacebookData data) throws Throwable
									{
										// This needs to be wrapper in a
										// JPA.withTransaction, because the
										// @Transactional statement does nothing
										// when asynchronous actions are
										// involved.
										return play.db.jpa.JPA
												.withTransaction(new Function0<Result>()
												{
													@Override
													public Result apply() throws Throwable
													{
														return handleAuthCallback(data, token);
													}
												});
									}
								});
					}
				});

	}

	/**
	 * Handle the authentication callback.
	 * 
	 * @param fbUid
	 * @param tokenInfo
	 * @return
	 */
	private Result handleAuthCallback(FacebookData data, FacebookToken tokenInfo)
	{
		// Set essential session data.
		session("oauthProvider", "facebook");
		session("facebookUid", data.getUserId().toPlainString());
		session("registrationType", "participant");

		// Check if the user id has already been entered.
		FacebookData fbData = getDataForUid(data, tokenInfo);

		// Now check if the user finished registration before.
		if (isNewUser(fbData))
		{
			// This user has never been seen before. That means
			// the user is registering for either a supplier or
			// a participant, or that the user previously
			// attempted to register but failed and is retrying.

			// Get the route to the registration page.
			final Call redirectTo = getRegistrationPageRoute();
			if (redirectTo == null)
				return badRequest("Invalid session data");

			// Redirect to the registration page to complete the registration.
			return redirect(redirectTo);
		}
		else
		{
			// Update the token.
			fbData.setExpireTime(getExpiryDate(tokenInfo.getExpires()));
			fbData.setToken(tokenInfo.getAccessToken());
			facebookDataDao.update(fbData);

			// Check if the user is attached to a participant or
			// supplier.
			if (fbData.getParticipant() != null)
			{
				// A participant logged in. Redirect to their portal.
				return doFacebookLogin(fbData);
			}
			else if (fbData.getSupplier() != null)
			{
				// A supplier logged in. Redirect to their portal.
				// TODO: Implement this.
				return internalServerError("Facebook supplier connection not implemented");
			}
			else
				// We shouldn't be getting here.
				return internalServerError("The impossible happened. Please contact the site administrator");
		}
	}

	/**
	 * Do facebook login.
	 * 
	 * @param data
	 * @return
	 */
	private Result doFacebookLogin(FacebookData data)
	{
		Participant participant = data.getParticipant();
		session().clear();
		// XXX: This is duplicated from the Login controller.
		// It should be moved into a separate class to have DRYness.
		session("email", participant.getEmail());
		session("role", participant.getUser().getRole().getName());
		session("userid", Integer.toString(participant.getUser().getUserId()));
		session("facebookUid", data.getUserId().toPlainString());
		session("oauthProvider", "session");
		// Redirect to the main page.
		return redirect(routes.Main.index());
	}

	/**
	 * Determine if the user is a new.
	 * 
	 * @param token
	 * @return
	 */
	private boolean isNewUser(FacebookData token)
	{
		return token.getSupplier() == null && token.getParticipant() == null;
	}

	/**
	 * Get the FacebookData entry for the given uid. Create it if it doesn't
	 * exist.
	 * 
	 * @param data
	 * @param tokenInfo
	 * @return
	 */
	private FacebookData getDataForUid(FacebookData data, FacebookToken tokenInfo)
	{
		FacebookData currentData = facebookDataDao.getByUserId(data.getUserId());
		if (currentData == null)
		{
			// Create a new entry as it doesn't appear to exist.
			// The facebook api returns an expiry time in
			// seconds. Calculate the expiry date/time by adding
			// it to the current time.
			data.setExpireTime(getExpiryDate(tokenInfo.getExpires()));
			data.setToken(tokenInfo.getAccessToken());
			facebookDataDao.create(data);
			return data;
		}
		return currentData;
	}

	/**
	 * Get the expiry date/time.
	 * 
	 * @param expiryTime The amount of seconds to add to the current time.
	 * @return
	 */
	private Date getExpiryDate(long expiryTime)
	{
		return new Date(System.currentTimeMillis() + (expiryTime * 1000));
	}

	/**
	 * Get the route to the right registration page. This method gets the
	 * registrationType from the session and then determines the route for the
	 * registration page.
	 * 
	 * @return
	 */
	private Call getRegistrationPageRoute()
	{
		String registrationType = session("registrationType");
		if (registrationType == null)
			logger.error("Registration type not set in the session");
		else
			// Get the redirect route for the registration type.
			switch (registrationType)
			{
				case "participant":
					return asapp.frontend.controllers.routes.Register.register();
				case "supplier":
					return asapp.supplierportal.controllers.routes.RegisterSupplier.register();
				default:
					logger.error("Invalid registration type set in session: " + registrationType);
			}

		return null;
	}

	private FacebookAuthCallbackData getDataFromRequest(Map<String, String[]> requestData)
			throws InvalidAuthCallbackRequestException
	{
		FacebookAuthCallbackData data = new FacebookAuthCallbackData();
		data.code = getMapValue(requestData, "code", 0);
		// TODO: Implement error message retrieval.
		return data;
	}

	private String getMapValue(Map<String, String[]> map, String key, int index)
			throws InvalidAuthCallbackRequestException
	{
		String[] values = map.get(key);
		if (values == null || values.length < (index + 1))
			throw new InvalidAuthCallbackRequestException("Key \"" + key
					+ "\" not set in callback request");
		return values[index];
	}
}
