/**
 * 
 * File created at 10 nov. 2013 00:37:33 by Johan Mulder <johan@mulder.net>
 */
package asapp.oauth.providers;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FacebookToken
{
	private String accessToken;
	private long expires;

	public FacebookToken()
	{

	}

	public FacebookToken(String accessToken, long expires)
	{
		this.accessToken = accessToken;
		this.expires = expires;
	}

	public String getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
	}

	public long getExpires()
	{
		return expires;
	}

	public void setExpires(long expires)
	{
		this.expires = expires;
	}

}
