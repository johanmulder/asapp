/**
 * 
 * File created at 9 nov. 2013 21:16:34 by Johan Mulder <johan@mulder.net>
 */
package asapp.oauth.providers;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.F.Function;
import play.libs.F.Promise;
import play.libs.WS;
import asapp.AsAppConfig;
import asapp.models.types.FacebookData;

/**
 * Class handling Facebook OAuth provider functions.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class FacebookProvider
{
	private Logger logger = LoggerFactory.getLogger(FacebookProvider.class);
	private AsAppConfig config;

	/**
	 * Class constructor.
	 * 
	 * @param config Necessary for retrieving various configuration settings.
	 */
	@Inject
	public FacebookProvider(AsAppConfig config)
	{
		this.config = config;
	}

	/**
	 * Convert a code to a token.
	 * 
	 * @param code
	 * @return A promise containing the filled in facebook token, because the
	 *         http request is done asynchronously.
	 */
	public Promise<FacebookToken> codeToToken(final String code)
	{
		return WS.url(config.getString("oauth.facebook.auth.token_uri"))
				.setQueryParameter("client_id", config.getString("oauth.facebook.app_id"))
				.setQueryParameter("redirect_uri", config.getString("oauth.facebook.redirect_uri"))
				.setQueryParameter("client_secret", config.getString("oauth.facebook.app_secret"))
				.setQueryParameter("code", code).get()
				.map(new Function<WS.Response, FacebookToken>()
				{
					@Override
					public FacebookToken apply(WS.Response response)
							throws UnsupportedEncodingException
					{
						// The result from the request is a query string alike
						Map<String, String> parameters = splitQuery(response.getBody());
						FacebookToken token = new FacebookToken(parameters.get("access_token"),
								Long.parseLong(parameters.get("expires")));
						return token;
					}
				});
	}

	/**
	 * Get base user data with the given access token.
	 * 
	 * @param accessToken
	 * @return A promise containing the base data, because the http request is
	 *         done asynchronously.
	 */
	public Promise<FacebookData> getBaseData(String accessToken)
	{
		logger.debug("Getting user id for access token " + accessToken + " from "
				+ config.getString("oauth.facebook.auth.graph_uri"));
		// Request these fields from the graph.
		String fields = StringUtils.join(new String[]
			{ "id", "name", "first_name", "middle_name", "last_name", "gender" }, ",");
		return WS.url(config.getString("oauth.facebook.auth.graph_uri"))
				.setQueryParameter("fields", fields).setQueryParameter("access_token", accessToken)
				.get().map(new Function<WS.Response, FacebookData>()
				{
					@Override
					public FacebookData apply(WS.Response response)
					{
						logger.debug(response.getBody());
						// Get the data and store it in a new FacebookData
						// object.
						JsonNode fbData = response.asJson();
						FacebookData data = new FacebookData();
						data.setUserId(new BigDecimal(getJsonPath(fbData, "id")));
						data.setName(getJsonPath(fbData, "name"));
						data.setFirstName(getJsonPath(fbData, "first_name"));
						data.setMiddleName(getJsonPath(fbData, "middle_name"));
						data.setLastName(getJsonPath(fbData, "last_name"));
						data.setEmail(getJsonPath(fbData, "email"));
						data.setGender(getJsonPath(fbData, "gender"));
						return data;
					}
				});
	}

	/**
	 * Get a value from a json node with the given path.
	 * 
	 * @param node The json node to look up the info in.
	 * @param path The path to check.
	 * @return null or the string value.
	 */
	private String getJsonPath(JsonNode node, String path)
	{
		JsonNode dataNode = node.findPath(path);
		return dataNode == null ? null : dataNode.asText();
	}

	/**
	 * Split up a query string and decode the parts of it. Returns a map of
	 * key/value pairs.
	 * 
	 * @param queryString
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static Map<String, String> splitQuery(String queryString)
			throws UnsupportedEncodingException
	{
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		String[] pairs = queryString.split("&");
		for (String pair : pairs)
		{
			int idx = pair.indexOf("=");
			query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
					URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}
		return query_pairs;
	}
}
