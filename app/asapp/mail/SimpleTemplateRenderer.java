package asapp.mail;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.api.libs.Files;

public class SimpleTemplateRenderer implements MailTemplateRenderer
{
	private static Logger logger = LoggerFactory.getLogger(SimpleTemplateRenderer.class);

	@Override
	public String render(URL template, Map<String, String> templateValues)
	{
		try
		{
			File file = new File(template.toURI());
			String content = Files.readFile(file);
			for (String templateKey : templateValues.keySet())
				content = content.replace("%" + templateKey + "%", templateValues.get(templateKey));
			return content;
		}
		catch (URISyntaxException e)
		{
			logger.error("Exception caught: " + e.getMessage(), e);
			return null;
		}
	}

	@Override
	public String render(File template, Map<String, String> templateValues)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String render(String template, Map<String, String> templateValues)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
