/**
 * 
 * File created at 3 nov. 2013 22:24:21 by Johan Mulder <johan@mulder.net>
 */
package asapp.mail;

import java.io.File;
import java.net.URL;
import java.util.Map;

/**
 * Interface defining the actions which a mail template render engine should
 * implement.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public interface MailTemplateRenderer
{
	/**
	 * Render a mail template from the given template url. The keys/values from
	 * templateValues will be assigned replaced in the template data.
	 * 
	 * @param template The template url.
	 * @param templateValues
	 * @return
	 */
	public String render(URL template, Map<String, String> templateValues);

	/**
	 * Render a mail template from the given template file. The keys/values from
	 * templateValues will be assigned replaced in the template data.
	 * 
	 * @param template The template file.
	 * @param templateValues
	 * @return
	 */
	public String render(File template, Map<String, String> templateValues);

	/**
	 * Render a mail template from the given template string. The keys/values
	 * from templateValues will be assigned replaced in the template data.
	 * 
	 * @param template The template string.
	 * @param templateValues
	 * @return
	 */
	public String render(String template, Map<String, String> templateValues);
}
