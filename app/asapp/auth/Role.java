/**
 * 
 * File created at 23 okt. 2013 20:24:29 by Johan Mulder <johan@mulder.net>
 */
package asapp.auth;

/**
 * Enum holding predefined role name constants.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public enum Role
{
	USER("user"), SUPPLIER("supplier"), ADMIN("admin"), UNKNOWN("UNKNOWN");
	private String value;

	private Role(String s)
	{
		value = s;
	}

	@Override
	public String toString()
	{
		return value;
	}

	/**
	 * Get the enumerate by enum value
	 * 
	 * @param s
	 * @return
	 */
	public static Role fromString(String s)
	{
		if (s != null)
			for (Role e : Role.values())
				if (e.toString().equals(s))
					return e;
		return Role.UNKNOWN;
	}
}
