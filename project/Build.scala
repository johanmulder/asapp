import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "AsApp"
  val appVersion      = "1.0"

  val appDependencies = Seq(
      "org.hibernate" % "hibernate-entitymanager" % "3.6.9.Final",
      "com.google.inject" % "guice" % "3.0",
      "mysql" % "mysql-connector-java" % "5.1.26",
      "javax.mail" % "mail" % "1.4.7",
      "org.apache.commons" % "commons-io" % "1.3.2",
      "org.mockito" % "mockito-all" % "1.9.5" % "test"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}