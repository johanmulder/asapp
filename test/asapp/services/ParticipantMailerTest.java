/**
 * 
 * File created at 3 nov. 2013 22:53:24 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import asapp.mail.MailTemplateRenderer;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;
import asapp.util.Mailer;

/**
 * Participant mailer test.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@RunWith(MockitoJUnitRunner.class)
public class ParticipantMailerTest
{
	@Mock
	private ParticipantGroupModel participantGroupModel;
	@Mock
	private Mailer mailer;
	@Mock
	private MailTemplateRenderer mailTemplateRenderer;
	@Mock
	private ServiceUtils serviceUtils;
	// Data generator.
	private TestDataGenerator dataGenerator = new TestDataGenerator();
	// The class under test.
	@InjectMocks
	private ParticipantMailer participantMailer;

	/**
	 * Test mailBookedParticipants
	 * 
	 * @throws MessagingException
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testMailBookedParticipants() throws MessagingException
	{
		// Test data.
		List<ParticipantGroup> groups = new ArrayList<>();
		ParticipantGroup testGroup = getTestGroup();
		groups.add(testGroup);

		// Configure mocks.
		when(participantGroupModel.getGroupsPendingMailing()).thenReturn(groups);
		when(serviceUtils.getBounceAddress(anyString(), anyInt())).thenReturn("bounce@bounce.com");
		when(serviceUtils.getMailTemplateUrl(anyString())).thenReturn(null);

		// Call the mail method of the participant mailer.
		participantMailer.mailBookedParticipants();
		// The status should change to this after sending the mail.
		assertEquals(ParticipantGroupStatus.PARTICIPANTS_INFORMED, testGroup.getStatus());
		// Verify that all calls have been made.
		verify(mailer, times(testGroup.getParticipants().size())).sendMail(any(MimeMessage.class));
		// Verify template rendering for all participants.
		verify(mailTemplateRenderer, times(testGroup.getParticipants().size())).render(
				any(URL.class), any(Map.class));
		// Verify updating of the participant group.
		verify(participantGroupModel, times(1)).update(eq(testGroup));
	}

	/**
	 * Generate a test group.
	 * 
	 * @return
	 */
	private ParticipantGroup getTestGroup()
	{
		// Generate a new group.
		ParticipantGroup testGroup = dataGenerator.getParticipantGroup(
				ParticipantGroupStatus.BOOKED, 3, 1, 9000, new Date(), "test");
		// Generate an activity.
		Activity activity = dataGenerator.getActivity("test", "1234AA", null);
		ActivityAvailability availability = dataGenerator.getAvailability(activity, new Date(), 1,
				1, null);
		// Generate a booking (this will automatically set the booking in the
		// test group).
		dataGenerator.getActivityBooking(availability, testGroup);

		return testGroup;
	}

}
