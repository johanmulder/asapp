/**
 * 
 * File created at 2 nov. 2013 17:00:13 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import asapp.models.types.*;
import asapp.models.types.ActivityBooking;

/**
 * Class which is useful for generating test data.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
class TestDataGenerator
{
	// Dummy id values.
	private int userId = 0;
	private int groupId = 0;
	private int activityId = 0;
	private int activityBookingId = 0;

	/**
	 * Get an activity availability.
	 * 
	 * @param activity
	 * @param date
	 * @param minUsers
	 * @param maxUsers
	 * @param tariffParticipant
	 * @return
	 */
	ActivityAvailability getAvailability(Activity activity, Date date, int minUsers, int maxUsers,
			BigDecimal tariffParticipant)
	{
		ActivityAvailability availability = new ActivityAvailability();
		availability.setActivity(activity);
		availability.setDateTime(date);
		availability.setMinParticipants(minUsers);
		availability.setMaxParticipants(maxUsers);
		availability.setTariffParticipant(tariffParticipant);
		return availability;
	}

	/**
	 * Get a new activity.
	 * 
	 * @param category
	 * @param zipcode
	 * @param baseTariff
	 * @return
	 */
	Activity getActivity(String category, String zipcode, BigDecimal baseTariff)
	{
		Activity activity = new Activity();
		activity.setActivityId(++activityId);
		activity.setActivityCategory(new ActivityCategory(category));
		activity.setZipcode(zipcode);
		activity.setBaseTariffParticipant(baseTariff);
		// Supplier
		activity.setSupplier(new Supplier(1, "Test Supplier", "Test Supplier Description", null,
				"supplier@asapp.localhost.nl"));
		return activity;
	}

	/**
	 * Get a new participant group.
	 * 
	 * @param status
	 * @param numParticipants
	 * @param participantTariff
	 * @param postcodeArea
	 * @param preferredTime
	 * @param category
	 * @return
	 */
	ParticipantGroup getParticipantGroup(ParticipantGroupStatus status, int numParticipants,
			int participantTariff, int postcodeArea, Date preferredTime, String category)
	{
		return getParticipantGroup(status, numParticipants, participantTariff, postcodeArea,
				preferredTime, category, null);
	}

	/**
	 * Get a new participantgroup with a predefined list of availabilities.
	 * 
	 * @param status
	 * @param numParticipants
	 * @param participantTariff
	 * @param postcodeArea
	 * @param preferredTime
	 * @param category
	 * @param availabilities
	 * @return
	 */
	ParticipantGroup getParticipantGroup(ParticipantGroupStatus status, int numParticipants,
			int participantTariff, int postcodeArea, Date preferredTime, String category,
			List<ActivityAvailability> availabilities)
	{
		return getParticipantGroup(status, numParticipants, participantTariff, postcodeArea,
				preferredTime, category, availabilities, 0);
	}

	/**
	 * Get a new participantgroup with a predefined list of availabilities and a
	 * maximum distance.
	 * 
	 * @param status
	 * @param numParticipants
	 * @param participantTariff
	 * @param postcodeArea
	 * @param preferredTime
	 * @param category
	 * @param availabilities
	 * @param maxDistance
	 * @return
	 */
	ParticipantGroup getParticipantGroup(ParticipantGroupStatus status, int numParticipants,
			int participantTariff, int postcodeArea, Date preferredTime, String category,
			List<ActivityAvailability> availabilities, int maxDistance)
	{
		ParticipantGroup group = new ParticipantGroup();
		group.setParticipantGroupId(++groupId);
		if (maxDistance > 0)
			group.setMaxDistance(10);
		group.setPostcodeArea(postcodeArea);
		group.setPreferredTime(preferredTime);
		group.setTariffParticipant(participantTariff);
		group.setStatus(status);
		group.setActivityCategory(new ActivityCategory(category));
		// Add participants.
		for (int i = 0; i < numParticipants; i++)
		{
			Participant p = getParticipant();
			group.addParticipant(p);
			// Create a new group member entry.
			ParticipantGroupMember member = new ParticipantGroupMember();
			member.setInitiator(i == 0 ? 1 : 0);
			member.setParticipantConfirmed(1);
			member.setParticipantGroup(group);
			member.setParticipant(p);
			group.getGroupMembers().add(member);
		}
		// Add availability entries if given.
		if (availabilities != null)
			for (ActivityAvailability a : availabilities)
				group.addActivityAvailability(a);
		return group;
	}

	/**
	 * Get a dummy participant.
	 * 
	 * @return
	 */
	Participant getParticipant()
	{
		Participant participant = new Participant();
		userId++;
		participant.setParticipantId(userId);
		participant.setEmail("user" + userId + "@domain.com");
		participant.setFirstName("First " + userId);
		participant.setMiddleName("Middle " + userId);
		participant.setLastName("Last " + userId);
		return participant;
	}

	/**
	 * Get an activity booking.
	 * 
	 * @param availability
	 * @param group
	 * @return
	 */
	ActivityBooking getActivityBooking(ActivityAvailability availability, ParticipantGroup group)
	{
		ActivityBooking booking = new ActivityBooking();
		booking.setActivityAvailability(availability);
		booking.setActivityBookingId(++activityBookingId);
		booking.setDatetime(new Date());
		booking.setParticipantGroup(group);
		group.setActivityBooking(booking);

		return booking;
	}

}
