/**
 * 
 * File created at 1 nov. 2013 22:53:27 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import asapp.data.dao.ActivityAvailabilityDao;
import asapp.models.BookingModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.*;
import asapp.services.zipcode.ZipcodeService;

/**
 * Activity selection unit test.
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@RunWith(MockitoJUnitRunner.class)
public class ActivitySelectionTest
{
	// The class under test.
	@InjectMocks
	private ActivitySelection activitySelection;
	// Mocking objects.
	@Mock
	private BookingModel bookingModel;
	@Mock
	private ParticipantGroupModel participantGroupModel;
	@Mock
	private ActivityAvailabilityDao activityAvailabilityDao;
	@Mock
	private ZipcodeService zipcodeService;
	// Data generator.
	private TestDataGenerator dataGenerator = new TestDataGenerator();

	/**
	 * Test method for
	 * {@link asapp.services.ActivitySelection#selectActivitiesForPendingGroups()}
	 * .
	 */
	@Test
	public void testSelectActivitiesForPendingGroups()
	{
		Date testDate = new Date();
		// Get test activity and availabilities.
		Activity testActivity = dataGenerator.getActivity("test", "9000AA", null);
		List<ActivityAvailability> availabilities = new ArrayList<>();
		// This activity should not be selected by the process.
		ActivityAvailability notSelectable = dataGenerator.getAvailability(testActivity, testDate,
				100, 100, new BigDecimal("14.50"));
		availabilities.add(notSelectable);
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));
		// Get another test activity with distance too far away.
		Activity distantActivity = dataGenerator.getActivity("test", "8000AA", null);
		ActivityAvailability farAvailability = dataGenerator.getAvailability(distantActivity,
				testDate, 100, 100, new BigDecimal("14.50"));
		availabilities.add(farAvailability);

		// Create a list with a test group.
		List<ParticipantGroup> groups = new ArrayList<>();
		// Group contains 5 participants in zipcode area 9000 with a preferred
		// tariff of €10 and maximum distance of 10 kilometers.
		ParticipantGroup testGroup = dataGenerator.getParticipantGroup(
				ParticipantGroupStatus.PENDING_SELECTION, 5, 10, 9000, testDate, "test", null, 10);
		groups.add(testGroup);

		// Mocks.
		when(participantGroupModel.getGroupsPendingSelection()).thenReturn(groups);
		when(activityAvailabilityDao.getByCategory(any(ActivityCategory.class))).thenReturn(
				availabilities);
		when(
				zipcodeService.calculateDistance(testActivity.getZipcode(),
						Integer.toString(testGroup.getPostcodeArea()))).thenReturn(0.0);
		// The distant activity is 100 kilometers from the participant group.
		when(
				zipcodeService.calculateDistance(distantActivity.getZipcode(),
						Integer.toString(testGroup.getPostcodeArea()))).thenReturn(
				testGroup.getMaxDistance() + 1.0);

		// Run the selection.
		activitySelection.selectActivitiesForPendingGroups();
		// The new status of the group should be this.
		assertEquals(ParticipantGroupStatus.PENDING_BOOKING, testGroup.getStatus());
		// The amount of availability entries attached to the group should be 3.
		assertEquals(3, testGroup.getActivityAvailabilities().size());
		// This availability should not be listed in the selected entries.
		assertFalse(testGroup.getActivityAvailabilities().contains(notSelectable));
		// Verify the calls to the zipcode service. There should be as many
		// calls as there are availabilities minus one, because there is one
		// distant activity.
		verify(zipcodeService, times(availabilities.size() - 1)).calculateDistance(
				eq(testActivity.getZipcode()), eq(Integer.toString(testGroup.getPostcodeArea())));
		// The distant activity should also be checked.
		verify(zipcodeService, times(1)).calculateDistance(eq(distantActivity.getZipcode()),
				eq(Integer.toString(testGroup.getPostcodeArea())));
		// The distant activity could never be selected, as the preferred
		// distance of the group is less than the (fake) distance between
		// the preferred zipcode area and the zipcode of the far availability
		assertFalse(testGroup.getActivityAvailabilities().contains(farAvailability));
	}
}
