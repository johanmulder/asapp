/**
 * 
 * File created at 3 nov. 2013 20:55:36 by Johan Mulder <johan@mulder.net>
 */
package asapp.services;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import asapp.models.BookingModel;
import asapp.models.ParticipantGroupModel;
import asapp.models.types.Activity;
import asapp.models.types.ActivityAvailability;
import asapp.models.types.ParticipantGroup;
import asapp.models.types.ParticipantGroupStatus;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
@RunWith(MockitoJUnitRunner.class)
public class ActivityBookingTest
{
	private TestDataGenerator dataGenerator = new TestDataGenerator();
	@InjectMocks
	private ActivityBooking activityBooking;
	// Mocking objects.
	@Mock
	private BookingModel bookingModel;
	@Mock
	private ParticipantGroupModel participantGroupModel;
	@Mock
	private SupplierMailer supplierMailer;

	/**
	 * Test method for
	 * {@link asapp.services.ActivityBooking#bookPendingSelections()}.
	 * 
	 * @throws MessagingException
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void testBookPendingSelections() throws UnsupportedEncodingException, MessagingException
	{
		// Initialize test data.
		Date testDate = new Date();
		Activity testActivity = dataGenerator.getActivity("test", "9000AA", null);

		// Activity availability entries.
		List<ActivityAvailability> availabilities = new ArrayList<>();
		// This activity should not be selected by the process.
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));
		availabilities.add(dataGenerator.getAvailability(testActivity, testDate, 3, 15,
				new BigDecimal("14.50")));

		// Create a list with a test group.
		List<ParticipantGroup> groups = new ArrayList<>();
		// Group contains 5 participants in area 9000 with a preferred tariff of
		// €10.
		ParticipantGroup testGroup = dataGenerator.getParticipantGroup(
				ParticipantGroupStatus.PENDING_BOOKING, 5, 10, 9000, testDate, "test",
				availabilities);
		groups.add(testGroup);

		// Fake booking
		asapp.models.types.ActivityBooking booking = new asapp.models.types.ActivityBooking();
		booking.setActivityAvailability(availabilities.get(0));
		booking.setParticipantGroup(testGroup);

		// Mocks.
		when(participantGroupModel.getGroupsPendingBooking()).thenReturn(groups);
		when(bookingModel.book(any(ParticipantGroup.class), any(ActivityAvailability.class)))
				.thenReturn(booking);

		// Run the booking.
		activityBooking.bookPendingSelections();

		// The bookingModel.book method should have been called exactly 1 time
		// with the test group as argument and any of the ActivityAvailability
		// entries.
		verify(bookingModel, times(1)).book(eq(testGroup), any(ActivityAvailability.class));
		// A mail should be sent.
		verify(supplierMailer, times(1)).mailBookingInformation(eq(testActivity.getSupplier()),
				eq(booking));
	}

}
